#whenever channel full, high number full channel first
#else whenever channel not empty, high number not empty channel first
import simpy
import random
import csv
from numpy import random as rm

RANDOM_SEED = 42
CHANNEL_NUMBER = 24
CHANNEL_FIFO_DEPTH = 4
READOUT_FIFO_DEPTH = 16
HIT_SEND_OUT_TIME = 20
IDLE_SEND_OUT_TIME = 5
FIFO_WRITE_TIME = 2
T_MEAN = 800
#800  400Khz

T_MINUS = 0
SIM_TIME = 10000000
#1 SIM_TIME = 3.125 ns
MONITOR_TIME = 1000000
FILENAME = '../data/b2_400k_nodeadtime_binomial_sameclkread.csv'
P0 = 0.00125  #0.00125 400KHz


def hit(channel, hit_time, env, channel_fifo, channel_count_list, channel_fifo_occupancy_list):
    if len(channel_fifo.items) >= CHANNEL_FIFO_DEPTH:
        channel_count_list[channel][1] = channel_count_list[channel][1] + 1 #hit lost ++
        print('At %d hit lost in channel %d .' % (env.now, channel))
    else:
        channel_fifo_occupancy_list[channel][len(channel_fifo.items)] = channel_fifo_occupancy_list[channel][len(channel_fifo.items)] + 1
        yield env.timeout(FIFO_WRITE_TIME)    #wait 2 cycle before fifo write
        channel_count_list[channel][0] = channel_count_list[channel][0] + 1  #hit acquire ++
        yield channel_fifo.put(hit_time)


def hit_generator(env, channel_fifo, channel, channel_count_list, channel_fifo_occupancy_list):
    while True:
        i=0;

        while random.random() > P0 :
            i=i+1
        # hit_interval = rm.exponential(T_MEAN)
        # hit_interval = random.expovariate(1.0/(T_MEAN - T_MINUS))+T_MINUS
        hit_interval = i
        yield env.timeout(int(hit_interval)+1)
        hit_time = env.now - 1 + hit_interval - int(hit_interval)
        env.process(hit(channel, hit_time, env, channel_fifo, channel_count_list, channel_fifo_occupancy_list))


def channel_mux_sample_empty_full(env, channel_fifo_list, readout_fifo, readout_fifo_occupancy_list_atwrite):
    while True:
        channel_fifo_depth_list = [len(i.items) for i in channel_fifo_list]       #list for items count in every channel's Store
        not_empty_index = [index for index, value in enumerate(channel_fifo_depth_list) if value > 0]
        not_empty_index.sort(reverse=True)   #bigger channel number has higher priority
        full_index = [index for index, value in enumerate(channel_fifo_depth_list) if value == 4]
        full_index.sort(reverse=True)
        if len(full_index) > 0:
            for index in full_index:
                hit_arrive = yield channel_fifo_list[index].get()
                # hit_arrive = channel_fifo_list[index].get().value
                yield env.timeout(FIFO_WRITE_TIME)
                readout_fifo_occupancy_list_atwrite[len(readout_fifo.items)] = readout_fifo_occupancy_list_atwrite[len(readout_fifo.items)] + 1
                #occupancy = 0, record in [0]
                yield readout_fifo.put([index, hit_arrive])
        elif len(not_empty_index) > 0:
            for index in not_empty_index:
                hit_arrive = yield channel_fifo_list[index].get()
                # hit_arrive =channel_fifo_list[index].get().value
                yield env.timeout(FIFO_WRITE_TIME)
                readout_fifo_occupancy_list_atwrite[len(readout_fifo.items)] = readout_fifo_occupancy_list_atwrite[len(readout_fifo.items)] + 1
                # occupancy = 0, record in [0]
                yield readout_fifo.put([index, hit_arrive])
        else:
            yield env.timeout(1)

#
def channel_mux_sample_empty_full_nofifo(env, channel_fifo_list, channel_latency_list):
    while True:
        channel_fifo_depth_list = [len(i.items) for i in channel_fifo_list]       #list for items count in every channel's Store
        not_empty_index = [index for index, value in enumerate(channel_fifo_depth_list) if value > 0]
        not_empty_index.sort(reverse=True)   #bigger channel number has higher priority
        full_index = [index for index, value in enumerate(channel_fifo_depth_list) if value == 4]
        full_index.sort(reverse=True)
        if len(full_index) > 0:
            for index in full_index:
                hit_arrive = yield channel_fifo_list[index].get()
                channel_latency_list[index].append(env.now - hit_arrive)
                yield env.timeout(HIT_SEND_OUT_TIME)
        elif len(not_empty_index) > 0:
            for index in not_empty_index:
                hit_arrive = yield channel_fifo_list[index].get()
                channel_latency_list[index].append(env.now - hit_arrive)
                yield env.timeout(HIT_SEND_OUT_TIME)
        else:
            yield env.timeout(IDLE_SEND_OUT_TIME)



def fifo_readout(env, readout_fifo, channel_latency_list, occupancy_list):
    while True:
        if len(readout_fifo.items) > 0:
            ([chl_index, hit_arrive]) = yield readout_fifo.get()
            #this yield above ensures that fifo_occupancy remains when read and write in the same time
            occupancy_list[len(readout_fifo.items)] = occupancy_list[len(readout_fifo.items)] + 1
            channel_latency_list[chl_index].append(env.now - hit_arrive)
            yield env.timeout(HIT_SEND_OUT_TIME)
        else:
            yield env.timeout(0) #if fifo_readout intepreted first, yield to wait for the fifo write
            yield env.timeout(IDLE_SEND_OUT_TIME)



def result_csvsave(channel_latency_list, channel_count_list, readout_fifo_occupancy_list, readout_fifo_occupancy_list_atwrite, channel_fifo_occupancy_list):
    total_hit = 0
    total_lost = 0
    hit_rate = 0
    decrease_rate = [0 for i in range(READOUT_FIFO_DEPTH)]
    decrease_rate_atwrite = [0 for i in range(READOUT_FIFO_DEPTH + 1)]
    for i in range(CHANNEL_NUMBER):
        total_hit = total_hit + channel_count_list[i][0]
        total_lost = total_lost + channel_count_list[i][1]
    print('total hit = %d, total lost = %d' % (total_hit, total_lost))
    hit_rate = (total_hit + total_lost) / (float(SIM_TIME) * 3.125*1e-9) / 24
    print('hit rate = %d Hz' % hit_rate)
    print('readout fifo occupancy from 1 to %d :' % READOUT_FIFO_DEPTH)
    print(readout_fifo_occupancy_list)
    for i in range(READOUT_FIFO_DEPTH - 1):
        if (readout_fifo_occupancy_list[i+1]==0):
            # print(i)
            decrease_rate[i] = 0
        else:
            # print(i)
            decrease_rate[i] = float(readout_fifo_occupancy_list[i]) / readout_fifo_occupancy_list[i + 1]
    print(decrease_rate)

    # rate = [float(item) / total_hit for item in readout_fifo_occupancy_list]
    # print(rate)
    print('readout fifo occupancy at write in from 0 to %d :' % (READOUT_FIFO_DEPTH))
    print(readout_fifo_occupancy_list_atwrite)
    for i in range(READOUT_FIFO_DEPTH - 1):
        if (readout_fifo_occupancy_list_atwrite[i+1]==0):
            # print(i)
            decrease_rate_atwrite[i] = 0
        else:
            # print(i)
            decrease_rate_atwrite[i] = float(readout_fifo_occupancy_list_atwrite[i]) / readout_fifo_occupancy_list_atwrite[i + 1]
    print(decrease_rate_atwrite)
    # rate = [float(item) / total_hit for item in readout_fifo_occupancy_list_atwrite]
    # print(rate)
    # for i in range(CHANNEL_NUMBER):
    #     print('channel %d occupancy from 1 to %d' % (i, CHANNEL_FIFO_DEPTH))
    #     print(channel_fifo_occupancy_list[i])
    filename = FILENAME
    with open(filename, 'wb') as csvfile:
        writer = csv.writer(csvfile)
        for i in range(CHANNEL_NUMBER):
            writer.writerow(channel_latency_list[i])
        writer.writerow(readout_fifo_occupancy_list)
        writer.writerow(decrease_rate)
        writer.writerow(readout_fifo_occupancy_list_atwrite)
        writer.writerow(decrease_rate_atwrite)
        # for i in range(CHANNEL_NUMBER):
        #     writer.writerow(channel_count_list[i])
        # for i in range(CHANNEL_NUMBER):
        #     writer.writerow(channel_fifo_occupancy_list[i])






def monitor(env, channel_latency_list):
    percentage=0
    while True:
        yield env.timeout(MONITOR_TIME)
        percentage += float(MONITOR_TIME)/SIM_TIME *100
        print('%.1f%% in progress, hit count displayed below' % percentage)
        print([len(channel_latency_list[i]) for i in range(CHANNEL_NUMBER)])


print('readout sim starting')
random.seed(RANDOM_SEED)
env = simpy.Environment()
channel_latency_list = [[] for i in range(CHANNEL_NUMBER)]  #initialize a 2-d array
channel_count_list = [[0, 0] for i in range(CHANNEL_NUMBER)]  #initialize a 2-d array
channel_fifo_list = [simpy.Store(env, capacity=CHANNEL_FIFO_DEPTH) for i in range(0, CHANNEL_NUMBER)]
readout_fifo_occupancy_list = [0 for i in range(READOUT_FIFO_DEPTH)]
readout_fifo_occupancy_list_atwrite = [0 for i in range(READOUT_FIFO_DEPTH+1)]
channel_fifo_occupancy_list = [[0 for i in range (CHANNEL_FIFO_DEPTH)] for j in range(CHANNEL_NUMBER)]
# channel_hit_list = [[] for i in range(CHANNEL_NUMBER)]


for i in range(0, CHANNEL_NUMBER):
    env.process(hit_generator(env, channel_fifo_list[i], i, channel_count_list, channel_fifo_occupancy_list))

if READOUT_FIFO_DEPTH > 0:
    readout_fifo = simpy.Store(env, capacity=READOUT_FIFO_DEPTH)
    env.process(channel_mux_sample_empty_full(env, channel_fifo_list, readout_fifo, readout_fifo_occupancy_list_atwrite))
    env.process(fifo_readout(env, readout_fifo, channel_latency_list, readout_fifo_occupancy_list))
elif READOUT_FIFO_DEPTH == 0:
    env.process(channel_mux_sample_empty_full_nofifo(env, channel_fifo_list, channel_latency_list))

env.process(monitor(env, channel_latency_list))
env.run(until=SIM_TIME)
result_csvsave(channel_latency_list, channel_count_list, readout_fifo_occupancy_list, readout_fifo_occupancy_list_atwrite, channel_fifo_occupancy_list)