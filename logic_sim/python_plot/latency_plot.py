import numpy as np
from matplotlib import pyplot as plt
import csv
import os
root_dir = "./400KHz_latency_data"

OFFSET = 3.65
BIN_LEFTEDGE = 140
BIN_RIGHTEDGE = 500
BIN_STEP = 5
BIN_COUNT = int(((BIN_RIGHTEDGE - BIN_LEFTEDGE) / BIN_STEP - 1))
bins = [BIN_LEFTEDGE + i*BIN_STEP for i in range(BIN_COUNT)]
latency_all = []
files = [root_dir+'/'+filename for filename in os.listdir(root_dir) if filename[-1:-5:-1]=='vsc.']

for i in range(24):
    with open(files[i],'r') as csvfile:
        reader = csv.reader(csvfile)
        latency = [float(row[0]) for row in reader]
    print('chl=%d,min=%f, max=%f, count=%d' % (i, np.amin(latency), np.amax(latency), len(latency)))

    hist, bintmp = np.histogram(latency, bins)
    # # for single channel hist plot
    # print(hist)
    # plt.hist(latency, bins)
    # plt.title("histogram")
    # plt.show()
    latency_all = latency_all + latency
latency_all = [x + OFFSET for x in latency_all]

#for all data hist plot
print('all_data min=%f, max=%f, count=%d' % (np.amin(latency_all), np.amax(latency_all), len(latency_all)))
hist, bintmp = np.histogram(latency_all, bins)
print(hist)
plt.hist(latency_all, bins)
# plt.title("histogram")
plt.xlabel("Latency (ns)")
plt.ylabel("Count")
plt.show()
print('len_bin=%d, len_hist=%d' %(len(bins),len(hist)))
binplot=bins[0:-1]
hist_percent = [x*100.0/len(latency_all) for x in hist]
print('bar1 percent =%.3f' %hist_percent[2])
plt.bar(binplot, hist_percent, width = 4)
# plt.title("histogram")
plt.xlabel("Latency (ns)")
plt.ylabel("Percentage (%)")
plt.show()


hist_integ = hist
for i in range(BIN_COUNT - 2):
    hist_integ[i+1] = hist_integ[i] + hist[i+1]
print(hist_integ)
hist_integ_rate = [float(x)*100/len(latency_all) for x in hist_integ]
print(hist_integ_rate)
binplot=bins[0:-1]
plt.plot(binplot, hist_integ_rate)
# plt.title("Integrated Percentage")
plt.xlabel("Latency (ns)")
plt.ylabel("Integrated Percentage (%)")
plt.show()
