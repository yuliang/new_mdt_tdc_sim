
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : TDC_sim_build_vivado.tcl
# Author     : Yu Liang
# Revision   : 
#              First created on 2019-02-17 
# Note       : 
     


set_param general.maxThreads 8


# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/../../source_files"
set firmware_dir "$origin_dir/../../../../MDT_TDC_ver1_sim_firmware"

create_project MDT_TDC_ver1_sim_firmware $firmware_dir -part xc7k325tffg900-2  -force
set_property board_part xilinx.com:kc705:part0:1.3 [current_project]


set_property include_dirs $source_dir/include [current_fileset]
add_files $source_dir/tdc_logic

set_property SOURCE_SET sources_1 [get_filesets sim_1]
set_property include_dirs $source_dir/include [get_filesets sim_1]
add_files -fileset sim_1  $source_dir/top_sim

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1


