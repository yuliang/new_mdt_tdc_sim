/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : common_definition.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`timescale 1 ns / 1 fs
`define ff_delay 0.1
`define logic_delay 0.1

`define ring_buffer1_adder_decoder_binary
`define roll_over_difference_structure
//`define roll_over_difference_structure_timing_tight
`define FIFO_param_behave1


`define BCID_POSITION_RING_BUFFER 16:5
`define VALID_POSITION_RING_BUFFER 36 //dual edge
//`define VALID_POSITION_RING_BUFFER 17 //single edge
`define DEBUG_PACKET_WIDTH 47:0



`define FULL_EDGE_TIME_SIZE 17
//`define CHANNEL_FIFO_WIDTH 36 //2*17+2bit_matching
`define CHANNEL_FIFO_WIDTH 40 //normal(2*17+2bit_matching+1bit rising_is_leading+ 3bit_flag) debug(1bit flag + 1bit edge_mode + 2bit hit_num + 15bit cnt_drop + 4bit q + 17bit data)
`define FULL_DATA_SIZE 40 //(1bit flag + 1bit edge_mode + 2bit hit_num + 15bit cnt_drop + 4bit q + 17bit data)
`define COMBINE_TIME_IN_SIZE 36
`define CHANNEL_BUFFER_WIDTH 37 //2*17+2bit_matching+1bit_fake

`define COMBINE_TIME_OUT_SIZE 10

`define READ_OUT_FIFO_SIZE 43

`define TRIGGER_MATCHING_HIT_COUNT_SIZE 10

//FSM trigger_matching
//`define TRIGGER_MATCHING_SIZE 4:0
//`define CHNL_TRIGGER_MATCHING_IDLE 5'b00001
//`define CHNL_TRIGGER_MATCHING_IDLE_bit 0
//`define CHNL_TRIGGER_MATCHING_UPDATE_DATA 5'b00010
//`define CHNL_TRIGGER_MATCHING_UPDATE_DATA_bit 1
//`define CHNL_TRIGGER_MATCHING_WAIT_FOR_INTEREST_DATA 5'b00100
//`define CHNL_TRIGGER_MATCHING_WAIT_FOR_INTEREST_DATA_bit 2
//`define CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO 5'b01000
//`define CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO_bit 3
//`define CHNL_TRIGGER_MATCHING_WAIT_FIFO 5'b10000
//`define CHNL_TRIGGER_MATCHING_WAIT_FIFO_bit 4


//FSM trigger_event_builder
`define TRIGGER_EVENT_BUILDER_SIZE 6:0

`define TRIGGER_EVENT_BUILDER_IDLE 7'b0000001
`define TRIGGER_EVENT_BUILDER_IDLE_bit 0
`define TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO 7'b0000010
`define TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit 1
`define TRIGGER_EVENT_BUILDER_WAIT_CHNL_DATA 7'b0000100
`define TRIGGER_EVENT_BUILDER_WAIT_CHNL_DATA_bit 2
`define TRIGGER_EVENT_BUILDER_SEND_HEAD 7'b0001000
`define TRIGGER_EVENT_BUILDER_SEND_HEAD_bit 3
`define TRIGGER_EVENT_BUILDER_GET_CHNL_DATA 7'b0010000
`define TRIGGER_EVENT_BUILDER_GET_CHNL_DATA_bit 4
`define TRIGGER_EVENT_BUILDER_SEND_CHNL_DATA 7'b0100000
`define TRIGGER_EVENT_BUILDER_SEND_CHNL_DATA_bit 5
`define TRIGGER_EVENT_BUILDER_SEND_TRAIL 7'b1000000
`define TRIGGER_EVENT_BUILDER_SEND_TRAIL_bit 6



`define TRIGGER_EVENT_BUILDER_TIME_OUT_COUNT_SIZE 12


`define TRIGGER_LEADING_IDLE_packet  	8'b10111100
`define TRIGGER_PAIRING_IDLE_packet  	8'b10111100
`define TRIGGERLESS_LEADING_IDLE_packet 8'b00111100
`define TRIGGERLESS_PAIRING_IDLE_packet 8'b10111100
`define TDC_ID_IDLE_packet				8'b00111100
`define TRIGGERLESS_ERROR_packet		8'b11011100	
`define CONFIG_ERROR_packet				8'b00111100

`define MAX_PACKET_NUM_SIZE 12



`define CLOG2(x)   (x<=2)?1:(x <= 4)?2:(x<=8)?3:(x<=16)?4:(x<=32)?5:(x<=64)?6:7