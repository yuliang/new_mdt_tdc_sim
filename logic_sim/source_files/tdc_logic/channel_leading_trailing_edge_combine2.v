/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_leading_trailing_edge_combine.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 15st, 2018
//  Note       : combine leading edge sub channel and trailing edge sub channel to edges data or combine data 
// timing is a little tight
`include "common_definition.v"
module channel_leading_trailing_edge_combine2
(
    input clk,
    input rst,
    input data_ready_l,data_ready_t,
    input [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_l,effictive_data_t,
    input [`FULL_DATA_SIZE-`FULL_EDGE_TIME_SIZE-1:0] debug_data,
    //config 
    //input enable_pair,
    input enable_leading,
    //input [11:0] roll_over,
    //input [2:0] width_select,    
    input [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config, //config for combine leading and trailing edge
    input channel_data_debug,
    input rising_is_leading,


    output combine_data_ready,
    output [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data,
    output [1:0] combine_flag,

    output chnl_fifo_write_triggerless,
    output [`CHANNEL_FIFO_WIDTH-1:0]fifo_data_in_triggerless
);

  wire  combine_data_ready_match;
  wire [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data_match;
  wire [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data_r;
  wire miss_trailing_edge;
  wire miss_trailing_edge_time_out;
  wire miss_trailing_edge_new_edge;
  wire [16:0] full_width;
  wire [7:0] width,width_int;
  
  
  assign  combine_data_ready = (enable_leading) ? data_ready_l :  combine_data_ready_match;
  assign  effictive_combine_data = (enable_leading) ? {effictive_data_t,effictive_data_l} :effictive_combine_data_r ;
  
  assign effictive_combine_data_r = effictive_combine_data_match;
  //always @(posedge clk  ) begin
  //  if (rst) begin
  //    // reset
  //    effictive_combine_data_r <= 34'b0;
  //  //end else if (enable_pair) begin
  //  //  effictive_combine_data_r <= { 9'b0,width,effictive_combine_data_match[16:0]};
  //  end else begin
  //    effictive_combine_data_r <= effictive_combine_data_match;
  //  end
  //end 
  assign  combine_flag = {miss_trailing_edge_time_out,miss_trailing_edge_new_edge};  
  assign chnl_fifo_write_triggerless = channel_data_debug ? data_ready_l : combine_data_ready;
  assign fifo_data_in_triggerless = channel_data_debug ? {debug_data,effictive_data_l} : {3'b000,rising_is_leading,combine_flag,effictive_combine_data};
  
  
  
  //matching leading and trailing edge
  channel_leading_trailing_edge_match3 #(`FULL_EDGE_TIME_SIZE)
    channel_leading_trailing_edge_match_inst(
      .clk(clk),
      .rst(rst),
      .enable(~enable_leading),

      .data_ready_l(data_ready_l),.data_ready_t(data_ready_t),
      .effictive_data_l(effictive_data_l),.effictive_data_t(effictive_data_t),
    
      //config for combine leading and trailing edge
      .combine_time_out_config(combine_time_out_config),  
    
      .combine_data_ready(combine_data_ready_match),
      .effictive_combine_data(effictive_combine_data_match),
      .miss_trailing_edge(miss_trailing_edge),
      .miss_trailing_edge_time_out(miss_trailing_edge_time_out),
      .miss_trailing_edge_new_edge(miss_trailing_edge_new_edge)
    );
 /*     
  //calculate width 
  roll_over_difference 
    roll_over_difference_inst(
     .enable(enable_pair),
     .roll_over(roll_over),
     .a(effictive_combine_data_match[33:17]),
     .b(effictive_combine_data_match[16:0]),
     .a_b(full_width)
    );
  
  assign over_flow = miss_trailing_edge | 
      (width_select == 7) & ( | full_width[16:15] ) |
      (width_select == 6) & ( | full_width[16:14] ) |
      (width_select == 5) & ( | full_width[16:13] ) |
      (width_select == 4) & ( | full_width[16:12] ) |
      (width_select == 3) & ( | full_width[16:11] ) |
      (width_select == 2) & ( | full_width[16:10] ) |
      (width_select == 1) & ( | full_width[16:9] ) |
      (width_select == 0) & ( | full_width[16:8] ) ;
  assign width_int = width_select[2] ? 
      ( width_select[1] ? (width_select[0] ? full_width[14:7] : full_width[13:6]) : 
              (width_select[0] ? full_width[12:5] : full_width[11:4]) ) :
      ( width_select[1] ? (width_select[0] ? full_width[10:3] : full_width[9:2]) : 
              (width_select[0] ? full_width[8:1] : full_width[7:0]) ) ;
  assign  width = over_flow ? 8'hff : width_int;
  */
endmodule