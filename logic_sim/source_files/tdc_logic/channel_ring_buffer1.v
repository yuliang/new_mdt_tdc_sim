/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_ring_buffer1.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Jan 16st, 2017
//  Note       : 
// 
`include "common_definition.v"
module channel_ring_buffer1 #(
	parameter DATA_WIDTH=37,
	parameter DATA_DEEP	=16
	)
(
    input clk,
    input rst,
    input enable,
    input data_ready,
    input [DATA_WIDTH-2:0] effictive_data,
    input fake_hit,
    output [(DATA_WIDTH*DATA_DEEP-1):0] data_out
    );
wire write_ringbuffer;
wire [DATA_WIDTH-1:0] ring_buffer_data;

ring_buffer_control1 #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP)
	) chnl_ring_buffer_control
(
    .clk(clk),
    .rst(rst),
    .data_ready(data_ready&enable),
    .effictive_data(effictive_data),
    .fake_hit(fake_hit&enable),
    .ring_buffer_data(ring_buffer_data),
    .write_ringbuffer(write_ringbuffer)
    );

ring_buffer1 #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP)
	)chnl_ring_buffer_inst
(
    .clk(clk),
    .rst(rst),
    .write(write_ringbuffer),
    .data_in(ring_buffer_data),
    .data_out(data_out)
    );
endmodule