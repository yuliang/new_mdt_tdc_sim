/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : chnl_trigger_matching2.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 22st, 2017
//  Note       :  not using FSM
// 
`include "common_definition.v"
module chnl_trigger_matching2 #(
	parameter DATA_WIDTH=37,
	parameter DATA_DEEP	=16
	)
(
    input clk,
    input rst,
    input enable,
    //ring buffer interface
	input [(DATA_WIDTH*DATA_DEEP-1):0] data_in,
    //trigger interface
    input trigger,
    input [11:0] limit_up,
    input [11:0] limit_down,
    output reg trigger_processing,
    //channel fifo interface
    input chnl_fifo_full,
    output reg chnl_fifo_write,
    output [DATA_WIDTH-2:0] data_out    
    );

reg [1:0] trigger_r;
wire trigger_inner;
reg [11:0] limit_up_r,limit_down_r;
wire [DATA_WIDTH-1:0] data [DATA_DEEP-1:0];

wire [DATA_DEEP-1:0] data_interest,data_interest_next;
reg	 [DATA_DEEP-1:0] data_interest_r;

//wire still_exist_interst;
//reg trigger_processing_r;

wire [DATA_DEEP-1:0] data_interest_one_hot;
wire [DATA_WIDTH-1:0] data_need_to_write;

reg [DATA_WIDTH-2:0] data_out_r;


 
//syn trriger
always @(posedge clk  ) begin
	if (rst) begin
		trigger_r <= 2'b0;
	end
	else begin
		trigger_r <= {trigger_r[0],trigger};	
	end
end

assign  trigger_inner = trigger_r[0]&(~trigger_r[1])&enable;

//if timing is tight
always @(posedge clk  ) begin
	if (rst) begin
		limit_up_r 	<= 12'hfff;
		limit_down_r <= 12'b0;
	end
	else if(trigger_inner) begin 
		limit_up_r 	<= limit_up;
		limit_down_r <= limit_down;
	end
end
//if timing is not tight
//always @(*) begin
//	limit_up_r 	= limit_up;
//	limit_down_r = limit_down;
//
//end


reg trigger_inner_r;
always @(posedge clk  ) begin
	if (rst) begin
		trigger_inner_r <= 1'b0;
	end
	else begin
		trigger_inner_r <= trigger_inner;	
	end
end

//lock data

generate
	genvar i;
	for (i = 0; i < DATA_DEEP; i = i + 1)
	begin:data_update_generate
		assign	data[i] = data_in[(i+1)*DATA_WIDTH-1:i*DATA_WIDTH];
	end
endgenerate

//select interest data
generate
	genvar i0;
	for (i0 = 0; i0 < DATA_DEEP; i0 = i0 + 1)
	begin:data_interest_selector_generate
		data_interest_selector #(.DATA_WIDTH(DATA_WIDTH))
		data_interest_selector_inst(
			.limit_up(limit_up_r),
			.limit_down(limit_down_r),
			.data_in(data[i0]),
			.interest(data_interest[i0])
			);
	end
endgenerate


always @(posedge clk  ) begin
  if (rst) begin
    // reset
    data_interest_r <= 'b0;
  end
  else if (trigger_inner_r) begin
    data_interest_r <= data_interest ;
  end
  else begin
    data_interest_r <= data_interest_next;
  end
end
assign data_interest_next = chnl_fifo_full ? data_interest_r : (data_interest_r & (~data_interest_one_hot));



multi_hot_one_hot #(.WIDTH(DATA_DEEP)) 
multi_hot_one_hot_inst(
	.multi_hot_code(data_interest_r),
	.one_hot_code(data_interest_one_hot)
	);

one_hot_select #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP))
one_hot_select_inst(
	.one_hot_code(data_interest_one_hot),
	.data_in(data_in),
	.data_out(data_need_to_write)
	);


always @(posedge clk  ) begin
    if (rst) begin
        chnl_fifo_write <= 'b0;        
    end
    else if(~chnl_fifo_full) begin
        chnl_fifo_write <= |data_interest_r;
    end
end

always @(posedge clk  ) begin
	if (rst) begin
		data_out_r <= 'b0;		
	end
	else if(~chnl_fifo_full) begin
		data_out_r <= data_need_to_write[DATA_WIDTH-2:0];
	end
end
assign data_out = data_out_r;


wire trigger_processing_inner;
assign trigger_processing_inner = chnl_fifo_write|trigger_inner_r|trigger_inner;
reg [1:0] trigger_processing_r_delay;
always @(posedge clk  ) begin
	if (rst) begin
		trigger_processing_r_delay <= 2'b0;
	end
	else  if(~chnl_fifo_full)  begin
		trigger_processing_r_delay <= {trigger_processing_r_delay[0],trigger_processing_inner};
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		trigger_processing <= 1'b0;		
	end
	else begin
		trigger_processing <= trigger_processing_inner||(|trigger_processing_r_delay);
	end
end

//stattistic number write to chnl fifo
//reg [3:0] hit_number;
//always @(posedge clk  ) begin
//	if (rst) begin
		// reset
//		hit_number <= 3'b0;
//	end	else if (trigger_inner) begin
//		hit_number <= 3'b0;
//	end else if(chnl_fifo_write&(~chnl_fifo_full)) begin
//		hit_number <= hit_number + 3'b1;
//	end
//end
endmodule