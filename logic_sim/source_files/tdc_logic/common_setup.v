/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : common_setup.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 3st, 2017
//  Note       : 
// 
`include "common_definition.v"
module common_setup
(
	input  tck,
	input  trst,
	input  tdi,
	output  reg  tdo_b,
	input  tms,

	input tdi_to_dr,

	input setup0_shift,
	output setup0_data,

	input setup1_shift,
	output setup1_data,

	input setup2_shift,
	output setup2_data,


	input control0_shift,
	input control0_update,
	input control0_enable,
	output control0_data,

	input control1_shift,
	input control1_update,
	input control1_enable,
	output control1_data,

	input status0_shift,
	output status0_data,

	input status1_shift,
	output status1_data,


	output [4  :0] phase_clk160,
	output [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2,
	output         rst_ePLL,
	
	output [3  :0] ePllResA, ePllResB, ePllResC,
	output [3  :0] ePllIcpA, ePllIcpB, ePllIcpC,
	output [1  :0] ePllCapA, ePllCapB, ePllCapC,
	
	input          ePll_lock,
	
	
	output        config_error,
	
	
	output [3:0]  debug_port_select,
	output        chnl_fifo_overflow_clear,
	input  [23:0] chnl_fifo_overflow,
	input  		  instruction_error,
	//for TTC interface
	output        reset_jtag_in,
	output        event_teset_jtag_in,
	
	output 		  enable_new_ttc,
	output        enable_master_reset_code,
	output        enable_direct_bunch_reset,
	output        enable_direct_event_reset,
	output        enable_direct_trigger,
	
	//for bcr
	output [11:0] roll_over,
	output [11:0] coarse_count_offset,
	output        auto_roll_over,
	output        bypass_bcr_distribution,
	
	//channel model
	output        enable_trigger,
	output        channel_data_debug,
	output        enable_leading,
	output        enable_pair,
	output        enbale_fake_hit,
	output [11:0] fake_hit_time_interval,
	output [23:0] rising_is_leading,
	output [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config,
	output [23:0] channel_enable_r,
	output [23:0] channel_enable_f,
	
	//for trigger para
	output [11:0] bunch_offset,
	output [11:0] event_offset,
	output [11:0] match_window,
	
	
	//for readout interface
	output        enable_trigger_timeout,
	output        enable_high_speed, 
	output        enable_legacy,
	output 		  full_width_res,
	output [2:0]  width_select,
	output        enable_8b10b,
	output        enable_insert,
	output [`MAX_PACKET_NUM_SIZE-1:0] syn_packet_number,
	output        enable_error_packet,
	output        enable_TDC_ID,
	output [18:0] TDC_ID,
	output        enable_error_notify,
	
	
	output [3:0]  fine_sel,
	output [1:0]  lut0,lut1,lut2,lut3,lut4,lut5,lut6,lut7,
	output [1:0]  lut8,lut9,luta,lutb,lutc,lutd,lute,lutf
);


assign config_error = 1'b0;
//====================================================setup_chain_0 =============================================================
//TTC_setup 			5
//bcr_distribute 		2
//tdc_mode 				77
//TDC_ID 				19
//readout 				12
//total 				115
wire TTC_setup_data_in,TTC_setup_data_out;
wire TTC_setup_data_in_b,TTC_setup_data_out_b;
assign TTC_setup_data_in = tdi_to_dr;
assign TTC_setup_data_in_b = tdi;
wire [4:0] TTC_setup_out;
wire [4:0] TTC_setup_out_b;
assign {
enable_new_ttc,
enable_master_reset_code,
enable_direct_bunch_reset,
enable_direct_event_reset,
enable_direct_trigger
} = trst ? TTC_setup_out : TTC_setup_out_b;
setup_reg #(.WIDTH(5),.initial_value({5'b00000}))
TTC_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup0_shift),
	.serial_data_in(TTC_setup_data_in),
	.serial_data_out(TTC_setup_data_out),
	.parallel_data_out(TTC_setup_out)
);
setup_reg #(.WIDTH(5),.initial_value({5'b00000}))
TTC_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(TTC_setup_data_in_b),
	.serial_data_out(TTC_setup_data_out_b),
	.parallel_data_out(TTC_setup_out_b)
);



wire bcr_distribute_serial_in,bcr_distribute_serial_out;
wire bcr_distribute_serial_in_b,bcr_distribute_serial_out_b;
assign bcr_distribute_serial_in = TTC_setup_data_out, bcr_distribute_serial_in_b=TTC_setup_data_out_b;
wire [1:0] bcr_distribute_out;
wire [1:0] bcr_distribute_out_b;
assign {
auto_roll_over,
bypass_bcr_distribution} =trst ? bcr_distribute_out: bcr_distribute_out_b;
setup_reg #(.WIDTH(2),.initial_value({2'b10}))
bcr_distribute_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup0_shift),
	.serial_data_in(bcr_distribute_serial_in),
	.serial_data_out(bcr_distribute_serial_out),
	.parallel_data_out(bcr_distribute_out)
);
setup_reg #(.WIDTH(2),.initial_value({2'b10}))
bcr_distribute_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(bcr_distribute_serial_in_b),
	.serial_data_out(bcr_distribute_serial_out_b),
	.parallel_data_out(bcr_distribute_out_b)
);



wire tdc_mode_serial_in,tdc_mode_serial_out;
wire tdc_mode_serial_in_b,tdc_mode_serial_out_b;
assign tdc_mode_serial_in = bcr_distribute_serial_out, tdc_mode_serial_in_b = bcr_distribute_serial_out_b;
wire [76:0] tdc_mode_out;
wire [76:0] tdc_mode_out_b;
assign {
enable_trigger,
channel_data_debug,
enable_leading,
enable_pair,
enbale_fake_hit,
rising_is_leading,
channel_enable_r,
channel_enable_f
}= trst ? tdc_mode_out: tdc_mode_out_b;
setup_reg #(.WIDTH(77),.initial_value({1'b0,1'b0,1'b0,1'b1,1'b0,24'hff_ffff,24'hff_ffff,24'hff_ffff}))
tdc_mode_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup0_shift),
	.serial_data_in(tdc_mode_serial_in),
	.serial_data_out(tdc_mode_serial_out),
	.parallel_data_out(tdc_mode_out)
);
setup_reg #(.WIDTH(77),	.initial_value({1'b0,1'b0,1'b0,1'b1,1'b0,24'hff_ffff,24'hff_ffff,24'hff_ffff}))
tdc_mode_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(tdc_mode_serial_in_b),
	.serial_data_out(tdc_mode_serial_out_b),
	.parallel_data_out(tdc_mode_out_b)
);



wire TDC_ID_serial_in,TDC_ID_serial_out;
wire TDC_ID_serial_in_b,TDC_ID_serial_out_b;
assign  TDC_ID_serial_in = tdc_mode_serial_out, TDC_ID_serial_in_b=tdc_mode_serial_out_b;
wire [18:0] TDC_ID_out;
wire [18:0] TDC_ID_out_b;
assign {
TDC_ID
}= trst ? TDC_ID_out: TDC_ID_out_b;
setup_reg #(.WIDTH(19),.initial_value({19'h7aaaa}))
TDC_ID_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup0_shift),
	.serial_data_in(TDC_ID_serial_in),
	.serial_data_out(TDC_ID_serial_out),
	.parallel_data_out(TDC_ID_out)
);
setup_reg #(.WIDTH(19),.initial_value({19'h05555}))
TDC_ID_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(TDC_ID_serial_in_b),
	.serial_data_out(TDC_ID_serial_out_b),
	.parallel_data_out(TDC_ID_out_b)
);



wire readout_serial_in,readout_serial_out;
wire readout_serial_in_b,readout_serial_out_b;
assign readout_serial_in = TDC_ID_serial_out, readout_serial_in_b = TDC_ID_serial_out_b;
wire [11:0] readout_out;
wire [11:0] readout_out_b;
assign {
enable_trigger_timeout,
enable_high_speed,
enable_legacy,
full_width_res,
width_select,
enable_8b10b,
enable_insert,
enable_error_packet,
enable_TDC_ID,
enable_error_notify
}= trst ? readout_out: readout_out_b;
setup_reg #(.WIDTH(12),.initial_value({1'b0,1'b1,1'b0,1'b0,3'b000,1'b1,1'b0,1'b0,1'b0,1'b0}))
readout_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup0_shift),
	.serial_data_in(readout_serial_in),
	.serial_data_out(readout_serial_out),
	.parallel_data_out(readout_out)
);
setup_reg #(.WIDTH(12),.initial_value({1'b0,1'b1,1'b0,1'b0,3'b000,1'b1,1'b0,1'b0,1'b0,1'b0}))
readout_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(readout_serial_in_b),
	.serial_data_out(readout_serial_out_b),
	.parallel_data_out(readout_out_b)
);

assign setup0_data = readout_serial_out;

wire [114:0] setup_chain_0;
assign setup_chain_0 = {TTC_setup_out,bcr_distribute_out,tdc_mode_out,TDC_ID_out,readout_out};
//====================================================setup_chain_1 =============================================================
//timer 				12+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE
//coarse 		 		60
//total 				72+`COMBINE_TIME_OUT_SIZE(10)+`MAX_PACKET_NUM_SIZE(12)=94
wire timer_serial_in,timer_serial_out;
wire timer_serial_in_b,channel_timer_serial_out_b;
assign timer_serial_in = tdi_to_dr;
assign timer_serial_in_b = readout_serial_out_b;
wire [12+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE-1:0] timer_out;
wire [12+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE-1:0] timer_out_b;
assign {
combine_time_out_config,
fake_hit_time_interval,
syn_packet_number
}= trst ? timer_out: timer_out_b;
setup_reg #(.WIDTH(12+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE),.initial_value({10'd40,12'd256,12'hfff}))
timer_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup1_shift),
	.serial_data_in(timer_serial_in),
	.serial_data_out(timer_serial_out),
	.parallel_data_out(timer_out)
);
setup_reg #(.WIDTH(12+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE),.initial_value({10'd40,12'd256,12'hfff}))
timer_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(timer_serial_in_b),
	.serial_data_out(channel_timer_serial_out_b),
	.parallel_data_out(timer_out_b)
);



wire coarse_serial_in,coarse_serial_out;
wire coarse_serial_in_b,coarse_serial_out_b;
assign coarse_serial_in = timer_serial_out, coarse_serial_in_b = channel_timer_serial_out_b;
wire [59:0] coarse_out;
wire [59:0] coarse_out_b;
assign {
roll_over,
coarse_count_offset,
bunch_offset,
event_offset,
match_window
}= trst ? coarse_out: coarse_out_b;
setup_reg #(.WIDTH(60),.initial_value({12'hfff,12'h000,12'hF9C,12'h000,12'h01f}))
coarse_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup1_shift),
	.serial_data_in(coarse_serial_in),
	.serial_data_out(coarse_serial_out),
	.parallel_data_out(coarse_out)
);
setup_reg #(.WIDTH(60),.initial_value({12'hfff,12'h000,12'hF9C,12'h000,12'h01f}))
coarse_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(coarse_serial_in_b),
	.serial_data_out(coarse_serial_out_b),
	.parallel_data_out(coarse_out_b)
);

assign setup1_data = coarse_serial_out;

wire [72+`COMBINE_TIME_OUT_SIZE+`MAX_PACKET_NUM_SIZE-1:0] setup_chain_1;
assign setup_chain_1 = {timer_out,coarse_out};


//====================================================setup_chain_2 =============================================================
//chnl_decode 			36
//total 				36
wire chnl_decode_serial_in,chnl_decode_serial_out;
wire chnl_decode_serial_in_b,chnl_decode_serial_out_b;
assign  chnl_decode_serial_in = tdi_to_dr;
assign  chnl_decode_serial_in_b = coarse_serial_out_b;
wire [35:0] chnl_decode_out;
wire [35:0] chnl_decode_out_b;
assign {
fine_sel,
lut0,lut1,lut2,lut3,lut4,lut5,lut6,lut7,
lut8,lut9,luta,lutb,lutc,lutd,lute,lutf
}= trst ? chnl_decode_out: chnl_decode_out_b;
setup_reg #(.WIDTH(36),.initial_value({4'b0011,2'b00,2'b01,2'b10,2'b01,2'b11,2'b00,2'b10,2'b10,2'b00,2'b00,2'b00,2'b01,2'b11,2'b00,2'b11,2'b00}))
chnl_decode_setup(
	.clk(tck),
	.trst(trst),
	.shift(setup2_shift),
	.serial_data_in(chnl_decode_serial_in),
	.serial_data_out(chnl_decode_serial_out),
	.parallel_data_out(chnl_decode_out)
);
  				
setup_reg #(.WIDTH(36),.initial_value({4'b0011,2'b00,2'b01,2'b10,2'b01,2'b11,2'b00,2'b10,2'b10,2'b00,2'b00,2'b00,2'b01,2'b11,2'b00,2'b11,2'b00}))
chnl_decodee_setup_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.serial_data_in(chnl_decode_serial_in_b),
	.serial_data_out(chnl_decode_serial_out_b),
	.parallel_data_out(chnl_decode_out_b)
);

assign setup2_data = chnl_decode_serial_out;

wire [35:0] setup_chain_2;
assign setup_chain_2 = {chnl_decode_out};

wire [244:0] total_setup;
assign  total_setup= {setup_chain_2,setup_chain_1,setup_chain_0};
//====================================================control_chain_0 =============================================================
//reset_clear  			8
//total 				8
wire reset_control_serial_in,reset_controlserial_out;
wire reset_control_serial_in_b,reset_control_serial_out_b;
assign reset_control_serial_in = tdi_to_dr;
assign reset_control_serial_in_b = chnl_decode_serial_out_b;
wire [7:0] reset_control_out;
wire [7:0] reset_control_out_b;
assign {
rst_ePLL,
reset_jtag_in,
event_teset_jtag_in,
chnl_fifo_overflow_clear,
debug_port_select
}= trst ? reset_control_out: reset_control_out_b;
control_reg #(.WIDTH(8),.initial_value(8'b0000_0000))
reset_control(
	.clk(tck),
	.trst(trst),
	.shift(control0_shift),
	.enable(control0_enable),
	.update(control0_update),
	.serial_data_in(reset_control_serial_in),
	.serial_data_out(reset_controlserial_out),
	.parallel_data_out(reset_control_out)
);
  				
control_reg #(.WIDTH(8),.initial_value(8'b0000_0000))
reset_control_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.enable(tms),
	.update(~tms),
	.serial_data_in(reset_control_serial_in_b),
	.serial_data_out(reset_control_serial_out_b),
	.parallel_data_out(reset_control_out_b)
);
assign control0_data = reset_controlserial_out;		
//====================================================control_chain_1 =============================================================
//ePLL_control 			47
//total 				47
wire ePLL_control_serial_in,ePLL_control_serial_out;
wire ePLL_control_serial_in_b,ePLL_control_serial_out_b;
assign ePLL_control_serial_in = tdi_to_dr;
assign ePLL_control_serial_in_b = reset_control_serial_out_b;
wire [46:0] ePLL_control_out;
wire [46:0] ePLL_control_out_b;
assign {
phase_clk160,
phase_clk320_0,phase_clk320_1, phase_clk320_2,
ePllResA,ePllIcpA,ePllCapA,
ePllResB,ePllIcpB,ePllCapB,
ePllResC,ePllIcpC,ePllCapC
}= trst ? ePLL_control_out: ePLL_control_out_b;
control_reg #(.WIDTH(47),.initial_value({5'b00000,4'b0100, 4'b0000,4'b0010,4'b0010,4'b0100,2'b10,4'b0010,4'b0100,2'b10,4'b0010,4'b0100,2'b10}))
ePLL_control(
	.clk(tck),
	.trst(trst),
	.shift(control1_shift),
	.enable(control1_enable),
	.update(control1_update),
	.serial_data_in(ePLL_control_serial_in),
	.serial_data_out(ePLL_control_serial_out),
	.parallel_data_out(ePLL_control_out)
);
  				
control_reg #(.WIDTH(47),.initial_value({5'b00000,4'b0100, 4'b0000,4'b0010,4'b0010,4'b0100,2'b10,4'b0010,4'b0100,2'b10,4'b0010,4'b0100,2'b10}))
ePLL_control_b(
	.clk(tck),
	.trst(~trst),
	.shift(tms),
	.enable(tms),
	.update(~tms),
	.serial_data_in(ePLL_control_serial_in_b),
	.serial_data_out(ePLL_control_serial_out_b),
	.parallel_data_out(ePLL_control_out_b)
);

assign control1_data = ePLL_control_serial_out;

always @(negedge tck) begin
	tdo_b <= ePLL_control_serial_out_b;
end
//assign tdo_b = ePLL_control_serial_out_b;
//====================================================crc=============================================================
wire [54:0] total_control;
assign  total_control= {reset_control_out,ePLL_control_out};

wire [299:0] total_reg;
assign  total_reg = {total_control,total_setup};


wire [31:0] CRC;
crc8bit
crc8bit_0(
  .data_in(total_reg[80:0]),
  .crc_out(CRC[7:0])
);

crc8bit
crc8bit_1(
  .data_in(total_reg[161:81]),
  .crc_out(CRC[15:8])
);

crc8bit
crc8bit_2(
  .data_in(total_reg[242:162]),
  .crc_out(CRC[23:16])
);

crc8bit
crc8bit_3(
  .data_in({24'b0,total_reg[299:243]}),
  .crc_out(CRC[31:24])
);

//crc8bit
//crc8bit_4(
//  .data_in({62'b0,total_reg[342:324]}),
//  .crc_out(CRC[39:32])
//);

  			
//====================================================status_chain_0 =============================================================
//CRC_status 			33
//total 				33
wire CRC_serial_in,CRC_serial_out;

status_reg #(.WIDTH(33))
CRC_status(
	.clk(tck),
	.shift(status0_shift),
	.serial_data_in(CRC_serial_in),
	.serial_data_out(CRC_serial_out),
	.parallel_data_in({instruction_error,CRC})
);		
assign CRC_serial_in = tdi_to_dr;
assign  status0_data= CRC_serial_out;
//====================================================status_chain_1 =============================================================
//overflow_lock			25
//total 				25
wire overflow_lock_serial_in,overflow_lock_serial_out;

status_reg #(.WIDTH(25))
overflow_lock_status(
	.clk(tck),
	.shift(status1_shift),
	.serial_data_in(overflow_lock_serial_in),
	.serial_data_out(overflow_lock_serial_out),
	.parallel_data_in({ePll_lock,chnl_fifo_overflow})
);		
assign overflow_lock_serial_in = tdi_to_dr;
assign  status1_data= overflow_lock_serial_out;
endmodule