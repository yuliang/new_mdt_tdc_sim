/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : control_decoder.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    

`include "common_definition.v"
module control_decoder
( 
	input clk160, 
	input enable,
	input reset_in, 

	input encoded_control_in,

	input enable_master_reset_code,
	input enable_trigger,

	output reg trigger,
	output reg bunch_reset,
	output reg event_reset,
	output reg master_reset
);

reg[2:0] encoded_control_syn_r;
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		// reset
		encoded_control_syn_r <= 3'b0;
	end	else begin
		encoded_control_syn_r <= {encoded_control_syn_r[1] ,encoded_control_syn_r[0] & enable,encoded_control_in };
	end
end


wire syn_40M;
reg [1:0] syn_40M_reg;
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		syn_40M_reg <= 2'b00;
	end else if(encoded_control_syn_r[1]&(~encoded_control_syn_r[2])) begin
		syn_40M_reg <= 2'b10;
	end else begin
		syn_40M_reg <= syn_40M_reg + 2'b01;
	end
end
assign syn_40M = &syn_40M_reg;

reg input_shift_reg;
reg [2:0] decoder_shift_reg;
//shift reg to decode commands
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		input_shift_reg <= 1'b0;
		decoder_shift_reg <= 3'b0;
	end else if (syn_40M) begin
		input_shift_reg      <= encoded_control_syn_r[2];
		decoder_shift_reg[0] <= input_shift_reg;
		decoder_shift_reg[1] <= decoder_shift_reg[0];
		decoder_shift_reg[2] <= decoder_shift_reg[1];
	end
end
reg [1:0] code_size_count;
//bit count from start bit
always @(posedge clk160 or posedge reset_in)
	begin
	if (reset_in)
		code_size_count <=2'd0;
	else if (syn_40M) begin
		if ( ((code_size_count == 2'd0) | (code_size_count == 2'd3)) & input_shift_reg )
			code_size_count <= 2'd1;
		else if ( (code_size_count == 2'd1) | (code_size_count == 2'd2) )
			code_size_count <=  code_size_count + 2'd1;
		else
			code_size_count <= 2'd0;
		end
	end

wire code_detect,trigger_detect,bunch_reset_detect,event_reset_detect,master_reset_detect;

assign 	code_detect = (code_size_count == 3),
		trigger_detect =     code_detect & decoder_shift_reg[2] & ~decoder_shift_reg[1] & ~decoder_shift_reg[0], 
		bunch_reset_detect = code_detect & decoder_shift_reg[2] &  decoder_shift_reg[1] & ~decoder_shift_reg[0], 
		event_reset_detect = code_detect & decoder_shift_reg[2] &  decoder_shift_reg[1] & decoder_shift_reg[0], 
		master_reset_detect= code_detect & decoder_shift_reg[2] & ~decoder_shift_reg[1] & decoder_shift_reg[0];

//-----------------------------------------------------------
//registered versions of resets and trigger
always @(posedge clk160)
	begin
	trigger <= enable & enable_trigger & trigger_detect;
	end

always @(posedge clk160)
	begin
	bunch_reset <= enable & bunch_reset_detect;
	end

always @(posedge clk160)
	begin
	event_reset <= enable & event_reset_detect;
	end

always @(posedge clk160)
	begin
	master_reset = enable & enable_master_reset_code & master_reset_detect;
	end

endmodule