/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : control_reg.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 

`include "common_definition.v"
module control_reg
#(parameter
	WIDTH = 1,
	initial_value = 0
)
(
	input clk,
	input trst,
	input shift,
	input update,
	input enable,
	input serial_data_in,
	output serial_data_out,
	output reg [WIDTH-1:0] parallel_data_out
);

	reg [WIDTH-1:0] serial_reg;
	assign  serial_data_out = serial_reg[0];
	always @(posedge clk or negedge trst) begin
		if(~trst)begin
			serial_reg <= initial_value;
		end else if(enable)begin
			if (shift) begin
				serial_reg <= {serial_data_in,serial_reg[WIDTH-1:1]};
			end else begin
				serial_reg <= parallel_data_out;
			end	
		end	
	end

	always @(negedge clk or negedge trst) begin
		if (~trst) begin
			parallel_data_out <= initial_value;
		end else if (update) begin
			parallel_data_out <= serial_reg;
		end
	end
endmodule

