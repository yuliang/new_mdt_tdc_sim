//-----------------------------------------------------------------------------
// Copyright (C) 2009             OutputLogic.com 
// This source file may be used and distributed without restriction 
// provided that this copyright statement is not removed from the file 
// and that any derivative work contains the original copyright notice 
// and the associated disclaimer. 
// 
// THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS 
// OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED	
// WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
//-----------------------------------------------------------------------------
// CRC module for data[80:0] ,   crc[7:0]=1+x^1+x^2+x^3+x^5+x^8;
//-----------------------------------------------------------------------------
`include "common_definition.v"
module crc8bit(
  input [80:0] data_in,
  output [7:0] crc_out
);

  reg [7:0] lfsr_c;
  wire [7 :0] lfsr_q;

  assign crc_out = lfsr_c;
  assign lfsr_q  = {8{1'b0}}; //----> Initial value set to zero

  always @(*) begin
    lfsr_c[0] = lfsr_q[4] ^ lfsr_q[6] ^ data_in[0] ^ data_in[3] ^ data_in[5] ^ data_in[7] ^ data_in[8] 
                ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[12] ^ data_in[15] ^ data_in[21] 
                ^ data_in[22] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[27] ^ data_in[30] 
                ^ data_in[31] ^ data_in[32] ^ data_in[33] ^ data_in[35] ^ data_in[36] ^ data_in[37] 
                ^ data_in[38] ^ data_in[42] ^ data_in[44] ^ data_in[45] ^ data_in[46] ^ data_in[49] 
                ^ data_in[50] ^ data_in[51] ^ data_in[53] ^ data_in[54] ^ data_in[58] ^ data_in[59] 
                ^ data_in[61] ^ data_in[63] ^ data_in[64] ^ data_in[67] ^ data_in[69] ^ data_in[70] 
                ^ data_in[72] ^ data_in[77] ^ data_in[79];

    lfsr_c[1] = lfsr_q[0] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[6] ^ lfsr_q[7] ^ data_in[0] ^ data_in[1] 
                ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7] ^ data_in[13] ^ data_in[15] 
                ^ data_in[16] ^ data_in[21] ^ data_in[26] ^ data_in[27] ^ data_in[28] ^ data_in[30] 
                ^ data_in[34] ^ data_in[35] ^ data_in[39] ^ data_in[42] ^ data_in[43] ^ data_in[44] 
                ^ data_in[47] ^ data_in[49] ^ data_in[52] ^ data_in[53] ^ data_in[55] ^ data_in[58] 
                ^ data_in[60] ^ data_in[61] ^ data_in[62] ^ data_in[63] ^ data_in[65] ^ data_in[67] 
                ^ data_in[68] ^ data_in[69] ^ data_in[71] ^ data_in[72] ^ data_in[73] ^ data_in[77] 
                ^ data_in[78] ^ data_in[79] ^ data_in[80];

    lfsr_c[2] = lfsr_q[0] ^ lfsr_q[1] ^ lfsr_q[4] ^ lfsr_q[5] ^ lfsr_q[7] ^ data_in[0] ^ data_in[1] 
                ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6] ^ data_in[9] ^ data_in[10] 
                ^ data_in[11] ^ data_in[12] ^ data_in[14] ^ data_in[15] ^ data_in[16] ^ data_in[17] 
                ^ data_in[21] ^ data_in[23] ^ data_in[24] ^ data_in[25] ^ data_in[28] ^ data_in[29] 
                ^ data_in[30] ^ data_in[32] ^ data_in[33] ^ data_in[37] ^ data_in[38] ^ data_in[40] 
                ^ data_in[42] ^ data_in[43] ^ data_in[46] ^ data_in[48] ^ data_in[49] ^ data_in[51] 
                ^ data_in[56] ^ data_in[58] ^ data_in[62] ^ data_in[66] ^ data_in[67] ^ data_in[68] 
                ^ data_in[73] ^ data_in[74] ^ data_in[77] ^ data_in[78] ^ data_in[80];

    lfsr_c[3] = lfsr_q[1] ^ lfsr_q[2] ^ lfsr_q[4] ^ lfsr_q[5] ^ data_in[0] ^ data_in[1] ^ data_in[2] 
                ^ data_in[4] ^ data_in[8] ^ data_in[9] ^ data_in[13] ^ data_in[16] ^ data_in[17] 
                ^ data_in[18] ^ data_in[21] ^ data_in[23] ^ data_in[26] ^ data_in[27] ^ data_in[29] 
                ^ data_in[32] ^ data_in[34] ^ data_in[35] ^ data_in[36] ^ data_in[37] ^ data_in[39] 
                ^ data_in[41] ^ data_in[42] ^ data_in[43] ^ data_in[45] ^ data_in[46] ^ data_in[47] 
                ^ data_in[51] ^ data_in[52] ^ data_in[53] ^ data_in[54] ^ data_in[57] ^ data_in[58] 
                ^ data_in[61] ^ data_in[64] ^ data_in[68] ^ data_in[70] ^ data_in[72] ^ data_in[74] 
                ^ data_in[75] ^ data_in[77] ^ data_in[78];

    lfsr_c[4] = lfsr_q[0] ^ lfsr_q[2] ^ lfsr_q[3] ^ lfsr_q[5] ^ lfsr_q[6] ^ data_in[1] ^ data_in[2] 
                ^ data_in[3] ^ data_in[5] ^ data_in[9] ^ data_in[10] ^ data_in[14] ^ data_in[17]
                ^ data_in[18] ^ data_in[19] ^ data_in[22] ^ data_in[24] ^ data_in[27] ^ data_in[28] 
                ^ data_in[30] ^ data_in[33] ^ data_in[35] ^ data_in[36] ^ data_in[37] ^ data_in[38] 
                ^ data_in[40] ^ data_in[42] ^ data_in[43] ^ data_in[44] ^ data_in[46] ^ data_in[47] 
                ^ data_in[48] ^ data_in[52] ^ data_in[53] ^ data_in[54] ^ data_in[55] ^ data_in[58] 
                ^ data_in[59] ^ data_in[62] ^ data_in[65] ^ data_in[69] ^ data_in[71] ^ data_in[73] 
                ^ data_in[75] ^ data_in[76] ^ data_in[78] ^ data_in[79];

    lfsr_c[5] = lfsr_q[1] ^ lfsr_q[3] ^ lfsr_q[7] ^ data_in[0] ^ data_in[2] ^ data_in[4] ^ data_in[5] 
                ^ data_in[6] ^ data_in[7] ^ data_in[8] ^ data_in[9] ^ data_in[12] ^ data_in[18] ^ data_in[19] 
                ^ data_in[20] ^ data_in[21] ^ data_in[22] ^ data_in[24] ^ data_in[27] ^ data_in[28] 
                ^ data_in[29] ^ data_in[30] ^ data_in[32] ^ data_in[33] ^ data_in[34] ^ data_in[35] 
                ^ data_in[39] ^ data_in[41] ^ data_in[42] ^ data_in[43] ^ data_in[46] ^ data_in[47]
                ^ data_in[48] ^ data_in[50] ^ data_in[51] ^ data_in[55] ^ data_in[56] ^ data_in[58] 
                ^ data_in[60] ^ data_in[61] ^ data_in[64] ^ data_in[66] ^ data_in[67] ^ data_in[69] 
                ^ data_in[74] ^ data_in[76] ^ data_in[80];

    lfsr_c[6] = lfsr_q[2] ^ lfsr_q[4] ^ data_in[1] ^ data_in[3] ^ data_in[5] ^ data_in[6] ^ data_in[7] 
                ^ data_in[8] ^ data_in[9] ^ data_in[10] ^ data_in[13] ^ data_in[19] ^ data_in[20] 
                ^ data_in[21] ^ data_in[22] ^ data_in[23] ^ data_in[25] ^ data_in[28] ^ data_in[29] 
                ^ data_in[30] ^ data_in[31] ^ data_in[33] ^ data_in[34] ^ data_in[35] ^ data_in[36] 
                ^ data_in[40] ^ data_in[42] ^ data_in[43] ^ data_in[44] ^ data_in[47] ^ data_in[48] 
                ^ data_in[49] ^ data_in[51] ^ data_in[52] ^ data_in[56] ^ data_in[57] ^ data_in[59] 
                ^ data_in[61] ^ data_in[62] ^ data_in[65] ^ data_in[67] ^ data_in[68] ^ data_in[70] 
                ^ data_in[75] ^ data_in[77];

    lfsr_c[7] = lfsr_q[3] ^ lfsr_q[5] ^ data_in[2] ^ data_in[4] ^ data_in[6] ^ data_in[7] ^ data_in[8] 
                ^ data_in[9] ^ data_in[10] ^ data_in[11] ^ data_in[14] ^ data_in[20] ^ data_in[21] 
                ^ data_in[22] ^ data_in[23] ^ data_in[24] ^ data_in[26] ^ data_in[29] ^ data_in[30] 
                ^ data_in[31] ^ data_in[32] ^ data_in[34] ^ data_in[35] ^ data_in[36] ^ data_in[37] 
                ^ data_in[41] ^ data_in[43] ^ data_in[44] ^ data_in[45] ^ data_in[48] ^ data_in[49] 
                ^ data_in[50] ^ data_in[52] ^ data_in[53] ^ data_in[57] ^ data_in[58] ^ data_in[60] 
                ^ data_in[62] ^ data_in[63] ^ data_in[66] ^ data_in[68] ^ data_in[69] ^ data_in[71] 
                ^ data_in[76] ^ data_in[78];

  end // always

endmodule // crc