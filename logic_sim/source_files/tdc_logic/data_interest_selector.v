/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_interest_selector.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module data_interest_selector #(
	parameter DATA_WIDTH=18
	)
(
    input [11:0] limit_up,
    input [11:0] limit_down,
    input [DATA_WIDTH-1:0] data_in,
    output interest
    );
	wire data_inrange;
	comparator_up_down #(.DATA_WIDTH(12))comparator_inst(
		.limit_up(limit_up),
		.limit_down(limit_down),
		.data_in(data_in[`BCID_POSITION_RING_BUFFER]),
		.inrange(data_inrange)
		);

	wire valid_data;
	assign valid_data = data_in[`VALID_POSITION_RING_BUFFER];

	assign interest = data_inrange & valid_data;
endmodule