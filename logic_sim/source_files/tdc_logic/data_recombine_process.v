/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_recombine_process.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 16st, 2018
//  Note       : 
//               
`include "common_definition.v"
module data_recombine_process
(
	input clk,
	input rst,

	input channel_data_debug,

	input data_ready,
	input [(`CHANNEL_FIFO_WIDTH+5-1):0] data_in,
	input read_out_fifo_full,
	output read_out_fifo_write,
	output [`READ_OUT_FIFO_SIZE-1:0] read_out_fifo_data,

	input enable_trigger,
	input enable_leading,
	input enable_pair,
	input [11:0] roll_over,
	input full_width_res,
	input [2:0] width_select,  

	input trigger_head,
	input trigger_trail
);
	reg [`READ_OUT_FIFO_SIZE-1:0] data_r_1;
	reg data_ready_1;
	reg trailing_edge_MSB;
	reg matching_error_r;
	wire matching_error;
	assign  matching_error = |data_in[35:34];
	always @(posedge clk  ) begin
		if (rst) begin
			// reset
			data_r_1 <= 'b0;
			trailing_edge_MSB <= 'b0;
			matching_error_r <= 'b0;
		end
		else if (~read_out_fifo_full) begin
			if(channel_data_debug)begin
				data_r_1 <= {2'b00,data_in[39],16'h0ff0,data_in[23:0]};
			end else if(enable_trigger&(trigger_head|trigger_trail))begin
				data_r_1 <= trigger_head ? {2'b00,data_in[39],16'h00ff,data_in[23:0]} : {2'b00,data_in[39],16'hff00,data_in[23:0]};
			end else begin
				if(enable_leading) begin
					data_r_1 <= {2'b00,data_in[39],16'h000,data_in[44:40],1'b0,data_in[36],data_in[16:0]};
				end
				else begin
					data_r_1 <= matching_error ?{2'b10,data_in[39],data_in[44:40],1'b1,enable_pair,data_in[16:0],16'hffff}: 
												{2'b10,data_in[39],data_in[44:40],1'b1,enable_pair,data_in[16:0],data_in[32:17]};
					trailing_edge_MSB <= data_in[33];
					matching_error_r <= matching_error;
				end
			end
		end
	end
	always @(posedge clk  ) begin
		if (rst) begin
			// reset
			data_ready_1 <= 1'b0;
		end
		else if (~read_out_fifo_full) begin
			data_ready_1 <= data_ready;
		end
	end

	reg [`READ_OUT_FIFO_SIZE-1:0] data_r_2;
	reg data_ready_2;
	reg matching_error_r_2;
	wire [16:0] full_width;
	reg full_width_MSB;
	roll_over_difference 
    	roll_over_difference_inst(
    		.enable(enable_pair),
    		.roll_over(roll_over),
    		.a({trailing_edge_MSB,data_r_1[15:0]}),
    		.b(data_r_1[32:16]),
    		.a_b(full_width)
    	);

	always @(posedge clk  ) begin
		if (rst) begin
			// reset
			data_r_2 <= 'b0;
			full_width_MSB <='b0;
			matching_error_r_2 <= 'b0;
		end
		else if ((~read_out_fifo_full)&enable_pair) begin
			data_r_2 <= ((data_r_1[`READ_OUT_FIFO_SIZE-1:`READ_OUT_FIFO_SIZE-2]==2'b10)&(~matching_error_r)) ? {data_r_1[`READ_OUT_FIFO_SIZE-1:16],full_width[15:0]} : data_r_1;
			full_width_MSB <= full_width[16];
			matching_error_r_2 <= matching_error_r;
		end
	end

	always @(posedge clk  ) begin
		if (rst) begin
			// reset
			data_ready_2 <=  'b0;
		end
		else if ((~read_out_fifo_full)&enable_pair) begin
			data_ready_2 <= data_ready_1;
		end
	end
	
	wire [16:0] data_width;
	wire [7:0] width,width_int;
	wire over_flow;
	assign  data_width = {full_width_MSB,data_r_2[15:0]};
  	assign over_flow = matching_error_r_2 | 
      (width_select == 7) & ( | data_width[16:15] ) |
      (width_select == 6) & ( | data_width[16:14] ) |
      (width_select == 5) & ( | data_width[16:13] ) |
      (width_select == 4) & ( | data_width[16:12] ) |
      (width_select == 3) & ( | data_width[16:11] ) |
      (width_select == 2) & ( | data_width[16:10] ) |
      (width_select == 1) & ( | data_width[16:9] ) |
      (width_select == 0) & ( | data_width[16:8] ) ;
  	assign width_int = width_select[2] ? 
      ( width_select[1] ? (width_select[0] ? data_width[14:7] : data_width[13:6]) : 
              (width_select[0] ? data_width[12:5] : data_width[11:4]) ) :
      ( width_select[1] ? (width_select[0] ? data_width[10:3] : data_width[9:2]) : 
              (width_select[0] ? data_width[8:1] : data_width[7:0]) ) ;
    assign  width = over_flow ? 8'hff : width_int;
    
	wire [`READ_OUT_FIFO_SIZE-1:0] data_r_2_effective;
	assign data_r_2_effective = (full_width_res|(data_r_2[`READ_OUT_FIFO_SIZE-1:`READ_OUT_FIFO_SIZE-2]==2'b00)) ? data_r_2 : {2'b01,data_r_2[`READ_OUT_FIFO_SIZE-3:16],width,8'b0};

	assign read_out_fifo_write = (enable_pair & (~enable_leading)& (~channel_data_debug))? data_ready_2 : data_ready_1;
	assign read_out_fifo_data = (enable_pair & (~enable_leading) & (~channel_data_debug))? data_r_2_effective : data_r_1;

endmodule




  