/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : edge_mux.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : mux_sel=1 : data_l = data_r , data_t= data_f ; mux_sel=0 : data_l = data_f, data_t = data_r
// 
`include "common_definition.v"
module edge_mux 
#(parameter 
    WIDTH = 18
)
(
  input [WIDTH-1 : 0] data_r,
  input [WIDTH-1 : 0]data_f,
  output [WIDTH-1 : 0] data_l,
  output [WIDTH-1 : 0] data_t,
  input mux_sel
);

  assign data_l = mux_sel ? data_r : data_f;
  assign data_t = mux_sel ? data_f : data_r;

endmodule 