/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fake_hit_generator.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module fake_hit_generator #(
	parameter WIDTH=12
	)
(
    input clk,
    input rst,
    input enable,
    output fake_hit,
    input [WIDTH-1:0] time_interval
    );
localparam counter_WIDTH = WIDTH+2;
reg [WIDTH+1:0] counter;
wire sent_time;
assign sent_time = counter[WIDTH+1:0]=={time_interval,2'b11};
always @(posedge clk) begin
	if (rst) begin
		// reset
		counter <=  {counter_WIDTH{1'b0}};
	end else if (enable) begin
    if (sent_time) begin
      counter <= {counter_WIDTH{1'b0}};
    end else begin
      counter <=  counter + 'b1;
    end
  end
end

assign fake_hit=sent_time&enable;

endmodule