/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo16.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 13st, 2018
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 16
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area
//		depth is fixed to 16


module fifo16 #(parameter DSIZE = 17,parameter ASIZE = 4,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output wfull,
output rempty,
input [DSIZE-1:0] wdata,
input winc, wclk, wrst,
input rinc, rclk, rrst
);

wire [ASIZE-1:0] waddr, raddr;
wire [ASIZE:0] wptr, rptr, wq2_rptr, rq2_wptr;

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_r2w (
.data_out(wq2_rptr), .data_in(rptr),
.clk(wclk), .rst(wrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_w2r (
.data_out(rq2_wptr), .data_in(wptr),
.clk(rclk), .rst(rrst));

fifomem16 #(.DATASIZE(DSIZE), .ADDRSIZE(ASIZE)) fifomem_syn(
.rdata(rdata), .wdata(wdata),
.waddr(waddr), .raddr(raddr),
.wclken(winc&&(!wfull)), .wclk(wclk));

rptr_empty #(.ADDRSIZE(ASIZE)) rptr_empty_inst(
.rempty(rempty),
.raddr(raddr),
.rptr(rptr), .rq2_wptr(rq2_wptr),
.rinc(rinc), .rclk(rclk),
.rrst(rrst));

wptr_full #(.ADDRSIZE(ASIZE)) wptr_full_inst(
.wfull(wfull), .waddr(waddr),
.wptr(wptr), .wq2_rptr(wq2_rptr),
.winc(winc), .wclk(wclk),
.wrst(wrst));

endmodule