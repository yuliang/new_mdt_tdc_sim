/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifomem2.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : FIFO buffer memory array
// 


module fifomem16 #(parameter DATASIZE = 17, // Memory data word width
parameter ADDRSIZE = 4) // Number of mem address bits
(
output reg [DATASIZE-1:0] rdata,
input [DATASIZE-1:0] wdata,
input [ADDRSIZE-1:0] waddr, raddr,
input wclken, wclk
);

localparam DEPTH = 1<<ADDRSIZE;
reg [DATASIZE-1:0] mem [0:DEPTH-1];
always @(*) begin
	case(raddr)
		4'b0000:rdata = mem[0];
		4'b0001:rdata = mem[1];
		4'b0010:rdata = mem[2];
		4'b0011:rdata = mem[3];
		4'b0100:rdata = mem[4];
		4'b0101:rdata = mem[5];
		4'b0110:rdata = mem[6];
		4'b0111:rdata = mem[7];
		4'b1000:rdata = mem[8];
		4'b1001:rdata = mem[9];
		4'b1010:rdata = mem[10];
		4'b1011:rdata = mem[11];
		4'b1100:rdata = mem[12];
		4'b1101:rdata = mem[13];
		4'b1110:rdata = mem[14];
		4'b1111:rdata = mem[15];
	endcase
end

always @(posedge wclk) begin
	if (wclken) begin
		case(waddr)
			4'b0000: mem[0]  <= wdata; 
			4'b0001: mem[1]  <= wdata;
			4'b0010: mem[2]  <= wdata;
			4'b0011: mem[3]  <= wdata;
			4'b0100: mem[4]  <= wdata;
			4'b0101: mem[5]  <= wdata;
			4'b0110: mem[6]  <= wdata;
			4'b0111: mem[7]  <= wdata;
			4'b1000: mem[8]  <= wdata;
			4'b1001: mem[9]  <= wdata;
			4'b1010: mem[10] <= wdata;
			4'b1011: mem[11] <= wdata;
			4'b1100: mem[12] <= wdata;
			4'b1101: mem[13] <= wdata;
			4'b1110: mem[14] <= wdata;
			4'b1111: mem[15] <= wdata;
		endcase		
	end
end


endmodule