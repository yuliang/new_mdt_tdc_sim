/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : sync_DFFs.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : cross time domain synchronize DFF, do not touch when syn and imp
// 

module sync_DFFs #(parameter WIDTH = 3,parameter SYN_DEPTH = 2)
(
output[WIDTH-1:0] data_out,
input [WIDTH-1:0] data_in,
input clk, rst
);

wire [(WIDTH)*(SYN_DEPTH+1)-1:0] q_inner;
generate
	genvar i;
	for (i = 0; i < SYN_DEPTH; i = i + 1)
	begin:synchronize_DFF_generate
		multi_DFF_with_reset #(.WIDTH(WIDTH)) syn_DFF_inst
			(
				.d(q_inner[(i+1)*(WIDTH)-1:i*WIDTH]),
				.q(q_inner[(i+2)*(WIDTH)-1:(i+1)*WIDTH]),
				.clk(clk),
				.rst(rst)
			);
		
	end
endgenerate
assign q_inner[WIDTH-1:0] = data_in;
assign data_out = q_inner[(SYN_DEPTH+1)*WIDTH-1:SYN_DEPTH*WIDTH];
endmodule