/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : jtag_controller.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 

`include "common_definition.v"
module jtag_controller( 
	input tck, 
	input tms, 
	input tdi, 
	input trst, 
	output reg tdo, 
	output reg enable, 

	output tdi_to_dr, 


	output setup0_shift,
	input  setup0_in, 

	output setup1_shift,
	input  setup1_in, 

	output setup2_shift,
	input  setup2_in, 


	output control0_enable, 
	output control0_update, 
	output control0_shift,
	input  control0_in, 

	output control1_enable, 
	output control1_update, 
	output control1_shift,
	input  control1_in, 

//	output status1_enable,
	output status0_shift,
	input  status0_in, 

//	output status2_enable,
	output status1_shift,
	input  status1_in, 

    output ASD_TCK,
    output ASD_shift_out,
    output ASD_update_out,
    output ASD_data_out,
    input  ASD_data_in,

	output instruction_error);


reg[3:0]	tap_state;
parameter	state_test_logic_reset = 	4'hf,
		state_run_test_idle = 		4'hc , 
		state_select_dr_scan = 		4'h7 , 
		state_capture_dr = 		4'h6 , 
		state_shift_dr = 		4'h2 , 
		state_exit1_dr = 		4'h1 , 
		state_pause_dr = 		4'h3 , 
		state_exit2_dr = 		4'h0 , 
		state_update_dr = 		4'h5 , 
		state_select_ir_scan = 		4'h4 , 
		state_capture_ir = 		4'he , 
		state_shift_ir = 		4'ha , 
		state_exit1_ir = 		4'h9 , 
		state_pause_ir = 		4'hb , 
		state_exit2_ir = 		4'h8 , 
		state_update_ir = 		4'hd ; 

reg[4:0]	ir_shift_reg, ir_update_reg;
reg		bypass_reg;
reg		reset;


reg[31:0]	idcode_reg;
//changed in version3
parameter	idcode = 32'b10000100011100001101101011001110;

wire	reset_ir;
wire    m_idcode, m_bypass;
wire    m_setup0,m_setup1,m_setup2;
wire    m_control0,m_control1; 
wire    m_status0, m_status1;
wire    m_ASD;
wire	tdo_int;


always @(posedge tck or negedge trst)
        if ( ~trst )
            tap_state <= state_test_logic_reset;
        else begin
		case (tap_state)
		state_test_logic_reset :
			begin
			if (tms) 	tap_state <=  state_test_logic_reset ;
			else		tap_state <=  state_run_test_idle ;
			end
		state_run_test_idle :
			begin
			if (tms) 	tap_state <= state_select_dr_scan;
			else		tap_state <= state_run_test_idle ;
			end
		state_select_dr_scan :
			begin
			if (tms) 	tap_state <= state_select_ir_scan ;
			else		tap_state <= state_capture_dr ;
			end
		state_capture_dr :
			begin
			if (tms) 	tap_state <=  state_exit1_dr ;
			else		tap_state <=  state_shift_dr ;
			end
		state_shift_dr :
			begin
			if (tms) 	tap_state <=  state_exit1_dr ;
			else		tap_state <=  state_shift_dr ;
			end
		state_exit1_dr :
			begin
			if (tms) 	tap_state <=  state_update_dr ;
			else		tap_state <=  state_pause_dr ;
			end
		state_pause_dr :
			begin
			if (tms) 	tap_state <=  state_exit2_dr ;
			else		tap_state <=  state_pause_dr ;
			end
		state_exit2_dr :
			begin
			if (tms) 	tap_state <=  state_update_dr ;
			else		tap_state <=  state_shift_dr ;
			end
		state_update_dr :
			begin
			if (tms) 	tap_state <=  state_select_dr_scan ;
			else		tap_state <=  state_run_test_idle ;
			end
		state_select_ir_scan :
			begin
			if (tms) 	tap_state <=  state_test_logic_reset ;
			else		tap_state <=  state_capture_ir ;
			end
		state_capture_ir :
			begin
			if (tms) 	tap_state <= state_exit1_ir ;
			else		tap_state <= state_shift_ir ;
			end
		state_shift_ir :
			begin
			if (tms) 	tap_state <= state_exit1_ir ;
			else		tap_state <= state_shift_ir ;
			end
		state_exit1_ir :
			begin
			if (tms) 	tap_state <= state_update_ir ;
			else		tap_state <= state_pause_ir ;
			end
		state_pause_ir :
			begin
			if (tms) 	tap_state <= state_exit2_ir ;
			else		tap_state <= state_pause_ir ;
			end
		state_exit2_ir :
			begin
			if (tms) 	tap_state <= state_update_ir ;
			else		tap_state <= state_shift_ir ;
			end
		state_update_ir :
			begin
			if (tms) 	tap_state <= state_select_dr_scan ;
			else		tap_state <= state_run_test_idle ;
			end
		endcase
		end


always @(negedge tck)
	begin
	reset <= (tap_state == state_test_logic_reset); 
	end

always @(negedge tck)
	begin
	enable <= ( (tap_state == state_capture_dr) | 
		(tap_state == state_shift_dr) | 
		(tap_state == state_exit1_dr) | 
		(tap_state == state_pause_dr) | 
		(tap_state == state_exit2_dr) | 
		(tap_state == state_capture_ir) | 
		(tap_state == state_shift_ir) | 
		(tap_state == state_exit1_ir) | 
		(tap_state == state_pause_ir) | 
		(tap_state == state_exit2_ir) ); 
	end

always @(posedge tck) begin
	if ((tap_state == state_shift_ir) | (tap_state == state_capture_ir)) begin
		if (tap_state == state_shift_ir) begin
			ir_shift_reg <=  {tdi,ir_shift_reg[4:1]}; 
		end else begin
			ir_shift_reg <=  ir_update_reg;
		end
	end
end

assign reset_ir = ~trst | reset;

always @(negedge tck or posedge reset_ir) begin
	if (reset_ir) begin
		// reset
		ir_update_reg <= 5'b10001;
	end	else if (tap_state == state_update_ir) begin
		ir_update_reg <= ir_shift_reg;
	end
end

//parity error detection in instruction register
assign 	instruction_error = ^ ir_update_reg;

//instruction decoder
assign  
	//m_extest = (ir_update_reg[3:0] == 4'h0), 
	m_idcode = (ir_update_reg[3:0] == 4'h1), 
	//m_sample = (ir_update_reg[3:0] == 4'h2), 
	//m_intest = (ir_update_reg[3:0] == 4'h3), 
	m_bypass = (ir_update_reg[3:0] == 4'hf), 
	//m_readouttest  = (ir_update_reg[3:0] == 4'h7),
	m_setup0  = (ir_update_reg[3:0] == 4'h2), 
	m_setup1  = (ir_update_reg[3:0] == 4'h3), 
	m_setup2  = (ir_update_reg[3:0] == 4'h4),
	m_control0 = (ir_update_reg[3:0] == 4'h5),  
	m_control1 = (ir_update_reg[3:0] == 4'h6), 
	m_status0 = (ir_update_reg[3:0] == 4'h7), 
	m_status1 = (ir_update_reg[3:0] == 4'h8);
assign	
    m_ASD    = (ir_update_reg[3:0] == 4'h9);
	//m_coretest = (ir_update_reg[3:0] == 4'hb), 
	//m_bist = (ir_update_reg[3:0] == 4'h4), 
	//m_scan = (ir_update_reg[3:0] == 4'h5);

//generation of setup signals
assign 
//	setup_enable = m_setup&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	setup0_shift  = m_setup0&(tap_state == state_shift_dr),
//	setup_update = m_setup & (tap_state == state_update_dr);
//	setup_enable = m_setup&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	setup1_shift  = m_setup1&(tap_state == state_shift_dr),
//	setup_update = m_setup & (tap_state == state_update_dr);assign 
//	setup_enable = m_setup&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	setup2_shift  = m_setup2&(tap_state == state_shift_dr);
//	setup_update = m_setup & (tap_state == state_update_dr);

//generation of control signals
assign 
	control0_enable = m_control0&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	control0_shift  = m_control0&(tap_state == state_shift_dr),
	control0_update = m_control0&(tap_state == state_update_dr);
assign 
	control1_enable = m_control1&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	control1_shift  = m_control1&(tap_state == state_shift_dr),
	control1_update = m_control1&(tap_state == state_update_dr);

	
//generation of status signals
assign 
//	status1_enable = m_status1 &((tap_state == state_shift_dr)|(tap_state == state_capture_dr)), 
	status0_shift = m_status0 & (tap_state == state_shift_dr),
	status1_shift = m_status1 & (tap_state == state_shift_dr);

	
//tdo output
assign 	tdo_int = 	( (tap_state == state_select_ir_scan) | 
		  (tap_state == state_capture_ir) |
		  (tap_state == state_shift_ir) |
		  (tap_state == state_exit1_ir) |
		  (tap_state == state_pause_ir) |
		  (tap_state == state_exit2_ir) |
		  (tap_state == state_update_ir) ) ? 
		ir_shift_reg[0] :
		(m_idcode & idcode_reg[0] |
		 m_bypass & bypass_reg    | 
		 m_setup0 & setup0_in     | 
		 m_setup1 & setup1_in     |
		 m_setup2 & setup2_in     |
		 m_control0 & control0_in |
		 m_control1 & control1_in |
		 m_status0 & status0_in   |
		 m_status1 & status1_in    |
		 m_ASD & ASD_data_in);

always @(negedge tck)
	begin
	tdo <=  tdo_int;
	end

//tdi to scan paths
assign 	tdi_to_dr =  tdi;

always @(posedge tck) begin
	if (m_bypass&((tap_state == state_shift_dr) | (tap_state == state_capture_dr))) begin
		if ((tap_state == state_shift_dr)) bypass_reg <=  tdi_to_dr;
		else bypass_reg <=  1'b0;
	end
end

always @(posedge tck) begin
	if (m_idcode&((tap_state == state_shift_dr) | (tap_state == state_capture_dr))) begin
		if ((tap_state == state_shift_dr)) idcode_reg <=  {tdi_to_dr,idcode_reg[31:1]};
		else idcode_reg <=  idcode;
	end
end

wire ASD_enable,ASD_shift,ASD_update;
assign 
	ASD_enable = m_ASD&((tap_state == state_shift_dr)|(tap_state == state_capture_dr)),
	ASD_shift  = m_ASD&(tap_state == state_shift_dr),
	ASD_update = m_ASD&(tap_state == state_update_dr);
	
reg ASD_enable_r,ASD_shift_r,ASD_update_r;
always @(negedge tck)begin
    ASD_enable_r <= ASD_enable;
    ASD_shift_r <= ASD_shift;
    ASD_update_r <= ASD_update;
end
assign ASD_TCK = ASD_enable_r&tck;
assign ASD_shift_out = ~ASD_shift_r;
assign ASD_update_out = ASD_update_r;
assign ASD_data_out = ASD_enable_r & tdi;

endmodule

