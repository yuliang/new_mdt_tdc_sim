/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : legacy_serial_interface.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//     
`include "common_definition.v"
module legacy_serial_interface
(
    input clk160,
    input rst,

    input enable,
//config
	input enable_pair,
	//input enable_trigger,
	input enable_leading,
	input [18:0] TDC_ID,

    input read_out_fifo_empty,
	output read_out_fifo_read,
	input [`READ_OUT_FIFO_SIZE-1:0] read_out_fifo_data,

	output reg [1:0] d_line_slow
);
reg high;
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		high <= 1'b0;
	end else if (~enable) begin
		high <= 1'b0;
	end	else begin
		high <= ~high; 
	end
end

reg [35:0] packet_data;
reg [5:0] data_number;
reg [11:0] event_id;
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		packet_data <= 36'b0;
		event_id <= 12'b0;
	end else if(~enable)begin
		packet_data <= 36'b0;
		event_id <= 12'b0;	
	end else if(high) begin
		if (|data_number) begin
			packet_data <= {packet_data[34:0],1'b0};
		end else if(~read_out_fifo_empty) begin
			if (read_out_fifo_data[42:41]==2'b00) begin
				if ((read_out_fifo_data[39:24]==16'h00ff) ) begin
					packet_data <= {2'b01,4'b1010,TDC_ID[3:0],read_out_fifo_data[23:0],
									~(^{TDC_ID[3:0],read_out_fifo_data[23:0]}),1'b0};
					event_id <= read_out_fifo_data[23:12];
				end else if((read_out_fifo_data[39:24]==16'hff00)) begin
					packet_data <= {2'b01,4'b1100,TDC_ID[3:0],event_id,2'b00,read_out_fifo_data[9:0],
									~(^{TDC_ID[3:0],event_id,2'b00,read_out_fifo_data[9:0]}),1'b0};
				end else if(enable_leading) begin
					packet_data <= {2'b01,4'b0011,TDC_ID[3:0],read_out_fifo_data[23:19],read_out_fifo_data[17],read_out_fifo_data[40],read_out_fifo_data[16:0],
									~(^{TDC_ID[3:0],read_out_fifo_data[23:19],read_out_fifo_data[23:17],read_out_fifo_data[40],read_out_fifo_data[16:0]}),1'b0};
				end
			end else if(enable_pair&(read_out_fifo_data[42:41]==2'b01)) begin
				packet_data <= {2'b01,4'b0100,TDC_ID[3:0],read_out_fifo_data[39:35],read_out_fifo_data[15:8],read_out_fifo_data[26:16],
								(^{TDC_ID[3:0],read_out_fifo_data[39:35],read_out_fifo_data[15:8],read_out_fifo_data[26:16]}),1'b0};
			end else begin
				packet_data <= 36'b0;
			end
		end else begin
			packet_data <= 36'b0;
		end
	end
end

always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		data_number <= 6'b0;
	end else if(~enable)begin
		data_number <= 6'b0;
	end else if(high) begin
		if (|data_number) begin
			data_number <= data_number -6'b1;
		end else if(~read_out_fifo_empty) begin
			data_number <= 6'd35;
		end else begin
			data_number <= 6'd0;
		end
	end
end

assign read_out_fifo_read = (~|data_number)&(high)&(~read_out_fifo_empty);

always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		d_line_slow <= 2'b0;
	end	else begin
		d_line_slow[1] <= packet_data[35] &(enable);
		d_line_slow[0] <= ~packet_data[35] &(enable);
	end
end
endmodule