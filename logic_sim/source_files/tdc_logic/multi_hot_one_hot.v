/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : multi_hot_one_hot.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module multi_hot_one_hot #(
	parameter WIDTH=32
	)
(
    input [WIDTH-1:0] multi_hot_code,
    output [WIDTH-1:0] one_hot_code
    );
/*if
always @(*) begin
	if     (multi_hot_code[31]) one_hot_code = 32'h8000_0000;
	else if(multi_hot_code[30]) one_hot_code = 32'h4000_0000;   
	else if(multi_hot_code[29]) one_hot_code = 32'h2000_0000; 
	else if(multi_hot_code[28]) one_hot_code = 32'h1000_0000;
	else if(multi_hot_code[27]) one_hot_code = 32'h0800_0000;
	else if(multi_hot_code[26]) one_hot_code = 32'h0400_0000;
	else if(multi_hot_code[25]) one_hot_code = 32'h0200_0000;
	else if(multi_hot_code[24]) one_hot_code = 32'h0100_0000;
	else if(multi_hot_code[23]) one_hot_code = 32'h0080_0000;
	else if(multi_hot_code[22]) one_hot_code = 32'h0040_0000;
	else if(multi_hot_code[21]) one_hot_code = 32'h0020_0000;
	else if(multi_hot_code[20]) one_hot_code = 32'h0010_0000;
	else if(multi_hot_code[19]) one_hot_code = 32'h0008_0000;
	else if(multi_hot_code[18]) one_hot_code = 32'h0004_0000;
	else if(multi_hot_code[17]) one_hot_code = 32'h0002_0000;
	else if(multi_hot_code[16]) one_hot_code = 32'h0001_0000;
	else if(multi_hot_code[15]) one_hot_code = 32'h0000_8000;
	else if(multi_hot_code[14]) one_hot_code = 32'h0000_4000;
	else if(multi_hot_code[13]) one_hot_code = 32'h0000_2000;
	else if(multi_hot_code[12]) one_hot_code = 32'h0000_1000;
	else if(multi_hot_code[11]) one_hot_code = 32'h0000_0800;
	else if(multi_hot_code[10]) one_hot_code = 32'h0000_0400;
	else if(multi_hot_code[9])  one_hot_code = 32'h0000_0200;
	else if(multi_hot_code[8])  one_hot_code = 32'h0000_0100;
	else if(multi_hot_code[7])  one_hot_code = 32'h0000_0080;
	else if(multi_hot_code[6])  one_hot_code = 32'h0000_0040;
	else if(multi_hot_code[5])  one_hot_code = 32'h0000_0020;
	else if(multi_hot_code[4])  one_hot_code = 32'h0000_0010;
	else if(multi_hot_code[3])  one_hot_code = 32'h0000_0008;
	else if(multi_hot_code[2])  one_hot_code = 32'h0000_0004;
	else if(multi_hot_code[1])  one_hot_code = 32'h0000_0002;
	else if(multi_hot_code[0])  one_hot_code = 32'h0000_0001;	
	else                        one_hot_code = 32'h0000_0000;
end
*/
wire [WIDTH-1:0] multi_hot_code_inv;
assign  multi_hot_code_inv = ~multi_hot_code;
assign one_hot_code[WIDTH-1] = multi_hot_code[WIDTH-1];
generate
	genvar i;
	for (i = 1; i < WIDTH; i = i + 1)
	begin:multi_hot_one_hot_trans
		assign one_hot_code[WIDTH-1-i] = (&multi_hot_code_inv[WIDTH-1:WIDTH-i])&multi_hot_code[WIDTH-1-i];
	end
endgenerate
endmodule