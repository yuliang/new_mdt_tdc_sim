/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ring_buffer1.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module ring_buffer1 #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP	=32
	)
(
    input clk,
    input rst,
    input write,
    input [DATA_WIDTH-1:0] data_in,
    output [(DATA_WIDTH*DATA_DEEP-1):0] data_out
    );
	reg [DATA_WIDTH-1:0] data [DATA_DEEP-1:0];
	wire [DATA_DEEP-1:0] enable;

generate
	genvar i;
	for (i = 0; i < DATA_DEEP; i = i + 1)
	begin:ringbuffer_reg
		always @(posedge clk) begin
			if (rst) begin
				data[i] <= {DATA_WIDTH{1'b0}}; 		
			end
			else if (enable[i]) begin
				data[i] <= data_in;
			end
		end
	end
endgenerate

ring_buffer1_adder_decoder #(.DATA_DEEP(DATA_DEEP)
	) ring_buffer1_adder_decoder_inst
(
	.clk(clk),
	.rst(rst),
	.write(write),
	.enable(enable)
	);

generate
	genvar i1;
	for (i1 = 0; i1 < DATA_DEEP; i1 = i1 + 1)
	begin:ring_buffer_data_out_generate
		assign data_out[(i1+1)*DATA_WIDTH-1:i1*DATA_WIDTH]= data[i1];
	end
endgenerate


endmodule