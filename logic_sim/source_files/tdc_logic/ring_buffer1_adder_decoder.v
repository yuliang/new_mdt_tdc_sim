/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ring_buffer1_adder_decoder.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Jan 15st, 2018
//  Note       : 
// 

`include "common_definition.v"
module ring_buffer1_adder_decoder #(
	parameter DATA_DEEP=32
	) 
(
	input clk,
	input rst,
	input write,
	output [DATA_DEEP-1:0] enable
	);


`ifdef ring_buffer1_adder_decoder_binary
	localparam DATA_DEEP_binary = `CLOG2(DATA_DEEP);
	reg [DATA_DEEP_binary-1:0] adder;
	always @(posedge clk  ) begin
		if (rst) begin
			// reset
			adder <= {DATA_DEEP_binary{1'b0}};
		end
		else if (write) begin
			adder <= adder + 'b1;
		end
	end
	assign enable = ({1'b1,{(DATA_DEEP-1){1'b0}}}>>adder)&({DATA_DEEP{write}});
`else
    reg [DATA_DEEP-1:0] enable_inner;
	always @(posedge clk  ) begin
		if (rst) begin
		    enable_inner <={1'b1,{(DATA_DEEP-1){1'b0}}};
		end
		else if (write) begin
			//enable_inner <= {enable[DATA_DEEP-2:0],enable[DATA_DEEP-1]}; for radiation tolerence
			if(enable_inner[0]==1'b1) begin
				enable_inner <={1'b1,{(DATA_DEEP-1){1'b0}}};
			end
			else begin
				enable_inner <= {enable[0],enable[DATA_DEEP-1:1]};
			end				
		end
	end
	assign enable = enable_inner&({DATA_DEEP{write}});
`endif
endmodule