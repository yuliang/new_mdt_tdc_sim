/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : roll_over_difference.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : calculator if a>b, a_b= a-b;if a<b, a_b=a-b+roll_over+1
// 
`include "common_definition.v"
module roll_over_difference
(
    input enable,
    input [11 :0 ] roll_over,
    input [16 :0 ] a,
    input [16:0 ] b,
    output [16 :0 ] a_b 
);

    wire [16 :0 ] a_gated,b_gated;
    assign a_gated = a & ({17{enable}});
    assign b_gated = b & ({17{enable}});

//    `ifdef roll_over_difference_structure
        wire carry_out_high,carry_out_low;
        wire [11:0] direct_a_b,direct_a_b_minus_1,a_b_add_roll_over,a_b_add_roll_over_minus_1;
        wire [4:0] direct_a_b_low;
        `ifdef roll_over_difference_structure_timing_tight
            wire carry_out_high_high,carry_out_med;
            wire [5:0] direct_a_b_high,direct_a_b_high_minus_1,a_b_add_roll_over_high,a_b_add_roll_over_high_plus_1,a_b_add_roll_over_high_minus_1;
            wire [5:0] direct_a_b_med,direct_a_b_med_minus_1,a_b_add_roll_over_med,a_b_add_roll_over_med_minus_1;            
            adder_speed #(6)
                sub_high_1(.a(a_gated[16:11]),.b(~b_gated[16:11]),.carry_in(1'b1),.s(direct_a_b_high),.carry_out(carry_out_high_high)),
                sub_high_2(.a(a_gated[16:11]),.b(~b_gated[16:11]),.carry_in(1'b0),.s(direct_a_b_high_minus_1),.carry_out()),
                add_high_1(.a(direct_a_b_high),.b(roll_over[11:6]),.carry_in(1'b0),.s(a_b_add_roll_over_high),.carry_out()),
                add_high_2(.a(direct_a_b_high),.b(roll_over[11:6]),.carry_in(1'b1),.s(a_b_add_roll_over_high_plus_1),.carry_out()),
                add_high_3(.a(direct_a_b_high_minus_1),.b(roll_over[11:6]),.carry_in(1'b0),.s(a_b_add_roll_over_high_minus_1),.carry_out()),
    
                sub_med_1(.a(a_gated[10:5]),.b(~b_gated[10:5]),.carry_in(1'b1),.s(direct_a_b_med),.carry_out(carry_out_med)),
                sub_med_2(.a(a_gated[10:5]),.b(~b_gated[10:5]),.carry_in(1'b0),.s(direct_a_b_med_minus_1),.carry_out(carry_out_med_minus_1)),
                add_med_1(.a(direct_a_b_med),.b(roll_over[5:0]),.carry_in(1'b1),.s(a_b_add_roll_over_med),.carry_out(carry_out_med_roll_over)),
                add_med_2(.a(direct_a_b_med),.b(roll_over[5:0]),.carry_in(1'b0),.s(a_b_add_roll_over_med_minus_1),.carry_out(carry_out_med_roll_over_minus_1));
            
            assign  direct_a_b = (~carry_out_med) ? {direct_a_b_high_minus_1,direct_a_b_med}: {direct_a_b_high,direct_a_b_med};
            assign  direct_a_b_minus_1 = (~carry_out_med_minus_1)? {direct_a_b_high_minus_1,direct_a_b_med_minus_1}:{direct_a_b_high,direct_a_b_med_minus_1};
            
            assign  a_b_add_roll_over =(carry_out_med^carry_out_med_roll_over)?{a_b_add_roll_over_high,a_b_add_roll_over_med} :
                                       ((carry_out_med&carry_out_med_roll_over)? {a_b_add_roll_over_high_plus_1,a_b_add_roll_over_med}:
                                        {a_b_add_roll_over_high_minus_1,a_b_add_roll_over_med} 
                                        ) ;
            assign  a_b_add_roll_over_minus_1 = (carry_out_med^carry_out_med_roll_over_minus_1)?{a_b_add_roll_over_high,a_b_add_roll_over_med_minus_1} :
                                                ((carry_out_med&carry_out_med_roll_over_minus_1)? {a_b_add_roll_over_high_plus_1,a_b_add_roll_over_med_minus_1}:
                                                {a_b_add_roll_over_high_minus_1,a_b_add_roll_over_med_minus_1} 
                                                ) ;
            assign carry_out_high  = carry_out_high_high&(carry_out_med|(|direct_a_b_high));
        `else
            adder_speed #(12)
                sub_high_1(.a(a_gated[16:5]),.b(~b_gated[16:5]),.carry_in(1'b1),.s(direct_a_b),.carry_out(carry_out_high)),
                sub_high_2(.a(a_gated[16:5]),.b(~b_gated[16:5]),.carry_in(1'b0),.s(direct_a_b_minus_1),.carry_out()),
                add_high_1(.a(direct_a_b),.b(roll_over),.carry_in(1'b1),.s(a_b_add_roll_over),.carry_out()),
                add_high_2(.a(direct_a_b),.b(roll_over),.carry_in(1'b0),.s(a_b_add_roll_over_minus_1),.carry_out());
        `endif
        adder_speed #(5)
            sub_low_1(.a(a_gated[4:0]),.b(~b_gated[4:0]),.carry_in(1'b1),.s(direct_a_b_low),.carry_out(carry_out_low));
        reg [16:0] a_b_reg;
        always @(*) begin
            if(carry_out_high)begin
                if (carry_out_low) begin
                    a_b_reg = {direct_a_b,direct_a_b_low};
                end 
                else if(|direct_a_b) begin
                    a_b_reg = {direct_a_b_minus_1,direct_a_b_low};
                end
                else begin
                    a_b_reg = {a_b_add_roll_over_minus_1,direct_a_b_low};
                end
            end
            else begin
                if (carry_out_low) begin
                    a_b_reg = {a_b_add_roll_over,direct_a_b_low};
                end 
                else begin
                    a_b_reg = {a_b_add_roll_over_minus_1,direct_a_b_low};
                end
            end
        end
        assign  a_b = a_b_reg;
//    `else
//            assign a_b = (a_gated < b_gated) ? (a_gated - b_gated + {roll_over+1'b1,5'b0}) : (a_gated - b_gated) ;
//    `endif
endmodule
