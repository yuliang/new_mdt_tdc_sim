/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 8st, 2018
//  Note       : 
//    

`include "common_definition.v"
module serial_interface
(
    input clk,
    input rst,

	input enable_320,
	input enable,

	input interface_fifo_empty,
	output interface_fifo_read,
	input [9:0] interface_fifo_data,

	output [1:0] d_line
);

wire fifo_empty; assign  fifo_empty = interface_fifo_empty;
reg  fifo_read; assign interface_fifo_read = fifo_read;
reg [3:0] send_number;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		send_number <= 4'b0;
	end	else if (~fifo_empty&(~|send_number)&enable) begin
		send_number <= enable_320 ? 4'h4 : 4'h9;
	end else if((|send_number))begin
		send_number <= send_number-4'h1;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		fifo_read <= 1'b0;
	end
	else if (~fifo_empty&(~|send_number)&enable) begin
		fifo_read <= 1'b1;
	end else begin
		fifo_read <= 1'b0;
	end
end
//always @(*) begin
//	fifo_read = ~fifo_empty&(~|send_number)&enable;
//end


reg high;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		high <= 1'b0;
	end	else if (fifo_read) begin
		high <= 1'b0;
	end else if((~enable_320)&enable) begin
		high <= ~high;
	end
end

reg [9:0] data_send;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_send <= 10'b0;
	end
	else if (fifo_read) begin
		data_send <= interface_fifo_data;
	end else if(high|enable_320) begin
		data_send <= {data_send[7:0],data_send[9:8]};
	end
end


assign	d_line[0] = enable & data_send[8];
assign 	d_line[1] = enable & data_send[9];



endmodule