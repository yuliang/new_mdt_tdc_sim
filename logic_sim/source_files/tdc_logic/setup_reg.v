/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : setup_reg.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 

`include "common_definition.v"
module setup_reg
#(parameter
	WIDTH = 1,
	initial_value = 0
)
(
	input clk,
	input trst,
	input shift,
	input serial_data_in,
	output serial_data_out,
	output  [WIDTH-1:0] parallel_data_out
);

	reg [WIDTH-1:0] serial_reg;
	assign  serial_data_out = serial_reg[0];
	always @(posedge clk or negedge trst) begin
		if (~trst) begin
			serial_reg <= initial_value;
		end else if (shift) begin
			serial_reg <= {serial_data_in,serial_reg[WIDTH-1:1]};
	
		end
	end
	assign 	parallel_data_out = serial_reg;

endmodule
