/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_channel_dual_edge.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 7st, 2018
//  Note       : 
// 

`include "common_definition.v"
module tdc_channel_dual_edge 
#(parameter 
    DATA_DEEP	=16,
    FIFO_ASIZE = 2//FIFO_DEEP= 2^FIFO_ASIZE
)
(
  input             rst,      // reset, not BC reset
  input             clk160,

  input              channel_enable_r,channel_enable_f,
  //config
  input  [3  :0]    fine_sel, //encoding for fine time selection
  input  [1  :0]    lut0, lut1, lut2, lut3, lut4, lut5, lut6, lut7, lut8, lut9,
                    luta, lutb, lutc, lutd, lute, lutf,
  //input [2:0] width_select,
  //input [11:0] roll_over,
  //input enable_pair,
  input enable_leading,
  input [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config, 
  input rising_is_leading,
  input enable_trigger,
  input channel_data_debug,

  input [4:0] chnl_ID,
  
  
  input  Rdy_r,Rdy_f, // ready
  input  [3  :0]    q_r,q_f, // raw fine codes 
  input  [14 :0]    cnt_r, cnt_inv_r,cnt_f, cnt_inv_f, // coarse counter
  
  input fake_hit,
  
  input fifo_read,
  input fifo_read_clk,
  output [`CHANNEL_FIFO_WIDTH+5-1:0] channel_data_out,
  output fifo_empty,
  output fifo_full,

  input trigger,
  input [11:0] limit_up,
  input [11:0] limit_down,
  output trigger_processing,


  output chnl_fifo_overflow,//work in triggerless mode when fifo is full, but still need to write
  input  chnl_fifo_overflow_clear
);
    reg chnl_fifo_overflow_clear_r;
    reg chnl_fifo_overflow_0;
    reg chnl_fifo_overflow_1;

  wire data_ready_r; wire [`FULL_DATA_SIZE-1:0] full_data_r;//wire [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_r;
  wire data_ready_f; wire [`FULL_DATA_SIZE-1:0] full_data_f;//wire [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_f; 
  wire data_ready_l; wire [`FULL_DATA_SIZE-1:0] full_data_l;//wire [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_l;
  wire data_ready_t; wire [`FULL_DATA_SIZE-1:0] full_data_t;//wire [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_t;
  
  wire [(`CHANNEL_BUFFER_WIDTH*DATA_DEEP-1):0] ring_buffer_data;
  wire chnl_fifo_full;assign  fifo_full= chnl_fifo_full;
  wire chnl_fifo_write,chnl_fifo_write_trigger,chnl_fifo_write_triggerless;
  wire [`CHANNEL_FIFO_WIDTH-1:0] fifo_data_in,fifo_data_in_triggerless;
  wire [`CHANNEL_BUFFER_WIDTH-2:0] fifo_data_in_trigger;
  wire [`CHANNEL_FIFO_WIDTH-1:0] fifo_data_out;
  wire combine_data_ready;
  wire [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data;
  wire [1:0] combine_flag;
  
  chnl_tdc_front 
    chnl_tdc_front_r_inst (
      .fine_sel(fine_sel), //encoding for fine time selection
      
      .lut0(lut0), .lut1(lut1), .lut2(lut2), .lut3(lut3), .lut4(lut4), .lut5(lut5), .lut6(lut6), .lut7(lut7), 
      .lut8(lut8), .lut9(lut9), .luta(luta), .lutb(lutb), .lutc(lutc), .lutd(lutd), .lute(lute), .lutf(lutf),
      
      .channel_enable(channel_enable_r),
      .rst(rst),      // reset, not BC reset
      .edge_mode(1'b1),
      .clk160(clk160),
      .Rdy(Rdy_r), // ready
      .q(q_r), // raw fine codes 
      .cnt(cnt_r), .cnt_inv(cnt_inv_r), // coarse counter
      
      .data_ready(data_ready_r),
      .effictive_data(),
      .full_data(full_data_r)
    ),
    chnl_tdc_front_f_inst(
      .fine_sel(fine_sel), //encoding for fine time selection
      
      .lut0(lut0), .lut1(lut1), .lut2(lut2), .lut3(lut3), .lut4(lut4), .lut5(lut5), .lut6(lut6), .lut7(lut7), 
      .lut8(lut8), .lut9(lut9), .luta(luta), .lutb(lutb), .lutc(lutc), .lutd(lutd), .lute(lute), .lutf(lutf),
      
      .channel_enable(channel_enable_f),
      .rst(rst),      // reset, not BC reset
      .edge_mode(1'b0),
      .clk160(clk160),
      .Rdy(Rdy_f), // ready
      .q(q_f), // raw fine codes 
      .cnt(cnt_f), .cnt_inv(cnt_inv_f), // coarse counter
      
      .data_ready(data_ready_f),
      .effictive_data(),
      .full_data(full_data_f)
    );

  edge_mux #(.WIDTH(`FULL_DATA_SIZE+1)) 
    edge_mux_inst(
      .data_r({data_ready_r,full_data_r}),
      .data_f({data_ready_f,full_data_f}),
      .data_l({data_ready_l,full_data_l}),
      .data_t({data_ready_t,full_data_t}),
      .mux_sel(rising_is_leading)
    );

  channel_leading_trailing_edge_combine2 
    channel_leading_trailing_edge_combine_inst
    (
      .clk(clk160),
      .rst(rst),
      .data_ready_l(data_ready_l),.data_ready_t(data_ready_t),
      .effictive_data_l(full_data_l[`FULL_EDGE_TIME_SIZE-1:0]),.effictive_data_t(full_data_t[`FULL_EDGE_TIME_SIZE-1:0]),
      .debug_data(full_data_l[`FULL_DATA_SIZE-1:`FULL_EDGE_TIME_SIZE]),
       //config 
      //.enable_pair(enable_pair),
      .enable_leading(enable_leading),
      //.roll_over(roll_over),
      //.width_select(width_select),
      .rising_is_leading(rising_is_leading),
      //config for combine leading and trailing edge
      .combine_time_out_config(combine_time_out_config), 
      .channel_data_debug(channel_data_debug),
    
      .combine_data_ready(combine_data_ready),
      .effictive_combine_data(effictive_combine_data),
      .combine_flag(combine_flag),
    
      .chnl_fifo_write_triggerless(chnl_fifo_write_triggerless),
      .fifo_data_in_triggerless(fifo_data_in_triggerless)
    );

  //trigger mode
  channel_ring_buffer1 #(.DATA_WIDTH(`CHANNEL_BUFFER_WIDTH),.DATA_DEEP(DATA_DEEP))
    channel_ring_buffer_inst(
      .clk(clk160),
      .rst(rst),
      .enable(enable_trigger),
      .data_ready(combine_data_ready),
      .effictive_data({combine_flag,effictive_combine_data}),
      .fake_hit(fake_hit),
      .data_out(ring_buffer_data)
    );
  
  chnl_trigger_matching2 #(.DATA_WIDTH(`CHANNEL_BUFFER_WIDTH),.DATA_DEEP(DATA_DEEP)) 
    chnl_trigger_matching_inst (
      .clk(clk160),
      .rst(rst),
      .enable(enable_trigger),
      //ring buffer interface
    	.data_in(ring_buffer_data),
      //trigger interface
      .trigger(trigger),
      .limit_up(limit_up),
      .limit_down(limit_down),
      .trigger_processing(trigger_processing),
      //channel fifo interface
      .chnl_fifo_full(chnl_fifo_full),
      .chnl_fifo_write(chnl_fifo_write_trigger),
      .data_out(fifo_data_in_trigger)    
    );

  mux #(.WIDTH(`CHANNEL_FIFO_WIDTH+1))
    fifo_control_mux (
      .a({chnl_fifo_write_trigger,{3'b001,rising_is_leading,fifo_data_in_trigger}}),
      //.b({chnl_fifo_write_triggerless,fifo_data_in_triggerless}),
      .b({chnl_fifo_write_triggerless,chnl_fifo_overflow_1,fifo_data_in_triggerless[38:0]}),
      .out({chnl_fifo_write,fifo_data_in}),
      .sel(enable_trigger)
    );

  //channle FIFO
  fifo1 #(.DSIZE(`CHANNEL_FIFO_WIDTH),.ASIZE(FIFO_ASIZE),.SYN_DEPTH(1)) 
    chnl_fifo_inst (
      .rdata(fifo_data_out),
      .wfull(chnl_fifo_full),
      .rempty(fifo_empty),
      .wdata(fifo_data_in),
      .winc(chnl_fifo_write), .wclk(clk160), .wrst(rst),
      .rinc(fifo_read), .rclk(fifo_read_clk), .rrst(rst)
    );

  assign channel_data_out = {chnl_ID,fifo_data_out};


    always @(posedge clk160  ) begin
      if (rst) begin
        // reset
        chnl_fifo_overflow_clear_r <= 1'b0;
      end
      else begin
        chnl_fifo_overflow_clear_r <= chnl_fifo_overflow_clear;
      end
    end

    always @(posedge clk160  ) begin
      if (rst) begin
        // reset
        chnl_fifo_overflow_0 <= 1'b0;
      end
      else if (chnl_fifo_full&chnl_fifo_write_triggerless) begin
        chnl_fifo_overflow_0 <= 1'b1;
      end
      else if((~chnl_fifo_overflow_clear_r)&chnl_fifo_overflow_clear) begin
        chnl_fifo_overflow_0 <= 1'b0;
      end
    end
    assign chnl_fifo_overflow = chnl_fifo_overflow_0;

    always @(posedge clk160  ) begin
      if (rst) begin
        // reset
        chnl_fifo_overflow_1 <= 1'b0;
      end
      else if (chnl_fifo_full&chnl_fifo_write_triggerless) begin
        chnl_fifo_overflow_1 <= 1'b1;
      end
      else if((~chnl_fifo_full)&chnl_fifo_write_triggerless) begin
        chnl_fifo_overflow_1 <= 1'b0;
      end
    end


endmodule