/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_core_logic.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 23st, 2017
//  Note       : 
// 

`include "common_definition.v"
module tdc_core_logic
(
  input  rst,
  input  clk160,
  input  clk320,

//configuration
  input channel_data_debug,
  input enable_trigger,
  input [11:0] bunch_offset,
  input [11:0] event_offset,
  input [11:0] match_window,
  input enbale_fake_hit,
  input [11:0] fake_hit_time_interval,
  input enable_trigger_timeout,
  input enable_leading,
  input enable_pair,
  input [11:0] roll_over,
  input full_width_res,
  input [2:0] width_select,
  input enable_8b10b,
  input enable_insert,
  input enable_error_packet,
  input enable_TDC_ID,
  input [18:0] TDC_ID, 
  input enable_config_error_notify,
  input config_error,  
  input enable_high_speed, 
  input enable_legacy,
  input [`MAX_PACKET_NUM_SIZE-1:0] syn_packet_number,
  input [23:0] rising_is_leading,
  input [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config,
  input [23:0] channel_enable_r,
  input [23:0] channel_enable_f,
  input [3:0] fine_sel,
  input [1:0] lut0,lut1,lut2,lut3,lut4,lut5,lut6,lut7,lut8,lut9,luta,lutb,lutc,lutd,lute,lutf,

  output [23:0] chnl_fifo_overflow,
  input   chnl_fifo_overflow_clear,
  
  
  output [5:0] debug_port,


  input Rdy_r_0,Rdy_r_1,Rdy_r_2,Rdy_r_3,Rdy_r_4,Rdy_r_5,Rdy_r_6,Rdy_r_7,Rdy_r_8,Rdy_r_9,Rdy_r_10,Rdy_r_11,
  input Rdy_r_12,Rdy_r_13,Rdy_r_14,Rdy_r_15,Rdy_r_16,Rdy_r_17,Rdy_r_18,Rdy_r_19,Rdy_r_20,Rdy_r_21,Rdy_r_22,Rdy_r_23,

  input Rdy_f_0,Rdy_f_1,Rdy_f_2,Rdy_f_3,Rdy_f_4,Rdy_f_5,Rdy_f_6,Rdy_f_7,Rdy_f_8,Rdy_f_9,Rdy_f_10,Rdy_f_11,
  input Rdy_f_12,Rdy_f_13,Rdy_f_14,Rdy_f_15,Rdy_f_16,Rdy_f_17,Rdy_f_18,Rdy_f_19,Rdy_f_20,Rdy_f_21,Rdy_f_22,Rdy_f_23,

  input [3:0] q_r_0,q_r_1,q_r_2,q_r_3,q_r_4,q_r_5,q_r_6,q_r_7,q_r_8,q_r_9,q_r_10,q_r_11,
  input [3:0] q_r_12,q_r_13,q_r_14,q_r_15,q_r_16,q_r_17,q_r_18,q_r_19,q_r_20,q_r_21,q_r_22,q_r_23,

  input [3:0] q_f_0,q_f_1,q_f_2,q_f_3,q_f_4,q_f_5,q_f_6,q_f_7,q_f_8,q_f_9,q_f_10,q_f_11,
  input [3:0] q_f_12,q_f_13,q_f_14,q_f_15,q_f_16,q_f_17,q_f_18,q_f_19,q_f_20,q_f_21,q_f_22,q_f_23,

  input [14:0] cnt_r_0,cnt_r_1,cnt_r_2,cnt_r_3,cnt_r_4,cnt_r_5,cnt_r_6,cnt_r_7,cnt_r_8,cnt_r_9,cnt_r_10,cnt_r_11,
  input [14:0] cnt_r_12,cnt_r_13,cnt_r_14,cnt_r_15,cnt_r_16,cnt_r_17,cnt_r_18,cnt_r_19,cnt_r_20,cnt_r_21,cnt_r_22,cnt_r_23,

  input [14:0] cnt_inv_r_0,cnt_inv_r_1,cnt_inv_r_2,cnt_inv_r_3,cnt_inv_r_4,cnt_inv_r_5,cnt_inv_r_6,cnt_inv_r_7,cnt_inv_r_8,cnt_inv_r_9,cnt_inv_r_10,cnt_inv_r_11,
  input [14:0] cnt_inv_r_12,cnt_inv_r_13,cnt_inv_r_14,cnt_inv_r_15,cnt_inv_r_16,cnt_inv_r_17,cnt_inv_r_18,cnt_inv_r_19,cnt_inv_r_20,cnt_inv_r_21,cnt_inv_r_22,cnt_inv_r_23,

  input [14:0] cnt_f_0,cnt_f_1,cnt_f_2,cnt_f_3,cnt_f_4,cnt_f_5,cnt_f_6,cnt_f_7,cnt_f_8,cnt_f_9,cnt_f_10,cnt_f_11,
  input [14:0] cnt_f_12,cnt_f_13,cnt_f_14,cnt_f_15,cnt_f_16,cnt_f_17,cnt_f_18,cnt_f_19,cnt_f_20,cnt_f_21,cnt_f_22,cnt_f_23,

  input [14:0] cnt_inv_f_0,cnt_inv_f_1,cnt_inv_f_2,cnt_inv_f_3,cnt_inv_f_4,cnt_inv_f_5,cnt_inv_f_6,cnt_inv_f_7,cnt_inv_f_8,cnt_inv_f_9,cnt_inv_f_10,cnt_inv_f_11,
  input [14:0] cnt_inv_f_12,cnt_inv_f_13,cnt_inv_f_14,cnt_inv_f_15,cnt_inv_f_16,cnt_inv_f_17,cnt_inv_f_18,cnt_inv_f_19,cnt_inv_f_20,cnt_inv_f_21,cnt_inv_f_22,cnt_inv_f_23,


  input reset_event_id,
  input trigger_count_reset,
  input trigger,

  output [1:0] d_line
);

wire [23:0] chnl_fifo_empty;
wire [23:0] chnl_fifo_full;
wire [23:0] chnl_fifo_read;
wire [1079:0] chnl_data;
wire [23:0] trigger_processing;
wire channel_fifo_clear;
wire get_trigger;
wire [11:0] limit_up;
wire [11:0] limit_down;
wire [(24-`TRIGGER_MATCHING_HIT_COUNT_SIZE)-1-1:0] error_flag;
wire trigger_fifo_empty;
wire [11:0] trigger_event_id;
wire [11:0] trigger_bunch_id;
wire trigger_lost;
wire fake_hit;
assign error_flag = {12'b0,trigger_lost};

wire [23:0] Rdy_r,Rdy_f;
wire [3:0] q_r [23:0];
wire [3:0] q_f [23:0];
wire [14:0] cnt_r [23:0];
wire [14:0] cnt_inv_r [23:0];
wire [14:0] cnt_f [23:0];
wire [14:0] cnt_inv_f [23:0];

assign  Rdy_r[0] = Rdy_r_0, Rdy_r[1] = Rdy_r_1, Rdy_r[2] = Rdy_r_2, Rdy_r[3] = Rdy_r_3, Rdy_r[4] = Rdy_r_4, Rdy_r[5] = Rdy_r_5, Rdy_r[6] = Rdy_r_6, Rdy_r[7] = Rdy_r_7, 
        Rdy_r[8] = Rdy_r_8, Rdy_r[9] = Rdy_r_9, Rdy_r[10] = Rdy_r_10, Rdy_r[11] = Rdy_r_11, Rdy_r[12] = Rdy_r_12, Rdy_r[13] = Rdy_r_13, Rdy_r[14] = Rdy_r_14, Rdy_r[15] = Rdy_r_15, 
        Rdy_r[16] = Rdy_r_16, Rdy_r[17] = Rdy_r_17, Rdy_r[18] = Rdy_r_18, Rdy_r[19] = Rdy_r_19, Rdy_r[20] = Rdy_r_20, Rdy_r[21] = Rdy_r_21, Rdy_r[22] = Rdy_r_22, Rdy_r[23] = Rdy_r_23;


assign Rdy_f[0] = Rdy_f_0, Rdy_f[1] = Rdy_f_1, Rdy_f[2] = Rdy_f_2, Rdy_f[3] = Rdy_f_3, Rdy_f[4] = Rdy_f_4, Rdy_f[5] = Rdy_f_5, Rdy_f[6] = Rdy_f_6, Rdy_f[7] = Rdy_f_7, 
       Rdy_f[8] = Rdy_f_8, Rdy_f[9] = Rdy_f_9, Rdy_f[10] = Rdy_f_10, Rdy_f[11] = Rdy_f_11, Rdy_f[12] = Rdy_f_12, Rdy_f[13] = Rdy_f_13, Rdy_f[14] = Rdy_f_14, Rdy_f[15] = Rdy_f_15, 
       Rdy_f[16] = Rdy_f_16, Rdy_f[17] = Rdy_f_17, Rdy_f[18] = Rdy_f_18, Rdy_f[19] = Rdy_f_19, Rdy_f[20] = Rdy_f_20, Rdy_f[21] = Rdy_f_21, Rdy_f[22] = Rdy_f_22, Rdy_f[23] = Rdy_f_23;


assign q_r[0] = q_r_0, q_r[1] = q_r_1, q_r[2] = q_r_2, q_r[3] = q_r_3, q_r[4] = q_r_4, q_r[5] = q_r_5, q_r[6] = q_r_6, q_r[7] = q_r_7, 
       q_r[8] = q_r_8, q_r[9] = q_r_9, q_r[10] = q_r_10, q_r[11] = q_r_11, q_r[12] = q_r_12, q_r[13] = q_r_13, q_r[14] = q_r_14, q_r[15] = q_r_15, 
       q_r[16] = q_r_16, q_r[17] = q_r_17, q_r[18] = q_r_18, q_r[19] = q_r_19, q_r[20] = q_r_20, q_r[21] = q_r_21, q_r[22] = q_r_22, q_r[23] = q_r_23; 

assign q_f[0] = q_f_0, q_f[1] = q_f_1, q_f[2] = q_f_2, q_f[3] = q_f_3, q_f[4] = q_f_4, q_f[5] = q_f_5, q_f[6] = q_f_6, q_f[7] = q_f_7, 
       q_f[8] = q_f_8, q_f[9] = q_f_9, q_f[10] = q_f_10, q_f[11] = q_f_11, q_f[12] = q_f_12, q_f[13] = q_f_13, q_f[14] = q_f_14, q_f[15] = q_f_15, 
       q_f[16] = q_f_16, q_f[17] = q_f_17, q_f[18] = q_f_18, q_f[19] = q_f_19, q_f[20] = q_f_20, q_f[21] = q_f_21, q_f[22] = q_f_22, q_f[23] = q_f_23; 


assign cnt_r[0] = cnt_r_0, cnt_r[1] = cnt_r_1, cnt_r[2] = cnt_r_2, cnt_r[3] = cnt_r_3, cnt_r[4] = cnt_r_4, cnt_r[5] = cnt_r_5, cnt_r[6] = cnt_r_6, cnt_r[7] = cnt_r_7, 
       cnt_r[8] = cnt_r_8, cnt_r[9] = cnt_r_9, cnt_r[10] = cnt_r_10, cnt_r[11] = cnt_r_11, cnt_r[12] = cnt_r_12, cnt_r[13] = cnt_r_13, cnt_r[14] = cnt_r_14, cnt_r[15] = cnt_r_15, 
       cnt_r[16] = cnt_r_16, cnt_r[17] = cnt_r_17, cnt_r[18] = cnt_r_18, cnt_r[19] = cnt_r_19, cnt_r[20] = cnt_r_20, cnt_r[21] = cnt_r_21, cnt_r[22] = cnt_r_22, cnt_r[23] = cnt_r_23;


assign cnt_inv_r[0] = cnt_inv_r_0, cnt_inv_r[1] = cnt_inv_r_1, cnt_inv_r[2] = cnt_inv_r_2, cnt_inv_r[3] = cnt_inv_r_3, cnt_inv_r[4] = cnt_inv_r_4, cnt_inv_r[5] = cnt_inv_r_5, cnt_inv_r[6] = cnt_inv_r_6, cnt_inv_r[7] = cnt_inv_r_7, 
       cnt_inv_r[8] = cnt_inv_r_8, cnt_inv_r[9] = cnt_inv_r_9, cnt_inv_r[10] = cnt_inv_r_10, cnt_inv_r[11] = cnt_inv_r_11, cnt_inv_r[12] = cnt_inv_r_12, cnt_inv_r[13] = cnt_inv_r_13, cnt_inv_r[14] = cnt_inv_r_14, cnt_inv_r[15] = cnt_inv_r_15, 
       cnt_inv_r[16] = cnt_inv_r_16, cnt_inv_r[17] = cnt_inv_r_17, cnt_inv_r[18] = cnt_inv_r_18, cnt_inv_r[19] = cnt_inv_r_19, cnt_inv_r[20] = cnt_inv_r_20, cnt_inv_r[21] = cnt_inv_r_21, cnt_inv_r[22] = cnt_inv_r_22, cnt_inv_r[23] = cnt_inv_r_23;

assign cnt_f[0] = cnt_f_0, cnt_f[1] = cnt_f_1, cnt_f[2] = cnt_f_2, cnt_f[3] = cnt_f_3, cnt_f[4] = cnt_f_4, cnt_f[5] = cnt_f_5, cnt_f[6] = cnt_f_6, cnt_f[7] = cnt_f_7, 
       cnt_f[8] = cnt_f_8, cnt_f[9] = cnt_f_9, cnt_f[10] = cnt_f_10, cnt_f[11] = cnt_f_11, cnt_f[12] = cnt_f_12, cnt_f[13] = cnt_f_13, cnt_f[14] = cnt_f_14, cnt_f[15] = cnt_f_15, 
       cnt_f[16] = cnt_f_16, cnt_f[17] = cnt_f_17, cnt_f[18] = cnt_f_18, cnt_f[19] = cnt_f_19, cnt_f[20] = cnt_f_20, cnt_f[21] = cnt_f_21, cnt_f[22] = cnt_f_22, cnt_f[23] = cnt_f_23;

assign cnt_inv_f[0] = cnt_inv_f_0, cnt_inv_f[1] = cnt_inv_f_1, cnt_inv_f[2] = cnt_inv_f_2, cnt_inv_f[3] = cnt_inv_f_3, cnt_inv_f[4] = cnt_inv_f_4, cnt_inv_f[5] = cnt_inv_f_5, cnt_inv_f[6] = cnt_inv_f_6, cnt_inv_f[7] = cnt_inv_f_7, 
       cnt_inv_f[8] = cnt_inv_f_8, cnt_inv_f[9] = cnt_inv_f_9, cnt_inv_f[10] = cnt_inv_f_10, cnt_inv_f[11] = cnt_inv_f_11, cnt_inv_f[12] = cnt_inv_f_12, cnt_inv_f[13] = cnt_inv_f_13, cnt_inv_f[14] = cnt_inv_f_14, cnt_inv_f[15] = cnt_inv_f_15, 
       cnt_inv_f[16] = cnt_inv_f_16, cnt_inv_f[17] = cnt_inv_f_17, cnt_inv_f[18] = cnt_inv_f_18, cnt_inv_f[19] = cnt_inv_f_19, cnt_inv_f[20] = cnt_inv_f_20, cnt_inv_f[21] = cnt_inv_f_21, cnt_inv_f[22] = cnt_inv_f_22, cnt_inv_f[23] = cnt_inv_f_23;
 

generate
  genvar i;
  for (i = 0; i < 24; i = i + 1)
  begin:tdc_channel
    tdc_channel_dual_edge  #(.DATA_DEEP(16), .FIFO_ASIZE(2))//FIFO_DEEP= 2^FIFO_ASIZE
      tdc_channel_dual_edge_inst(
          .rst(rst),      // reset, not BC reset
          .clk160(clk160),
        
          .channel_enable_r(channel_enable_r[i]),.channel_enable_f(channel_enable_f[i]),
          .fine_sel(fine_sel), //encoding for fine time selection
          .lut0(lut0), .lut1(lut1), .lut2(lut2), .lut3(lut3), .lut4(lut4), .lut5(lut5), .lut6(lut6), .lut7(lut7), .lut8(lut8), .lut9(lut9),
          .luta(luta), .lutb(lutb), .lutc(lutc), .lutd(lutd), .lute(lute), .lutf(lutf),
          //.width_select(width_select),
          //.roll_over(roll_over),
          //.enable_pair(enable_pair),
          .enable_leading(enable_leading),
          .combine_time_out_config(combine_time_out_config), 
          .rising_is_leading(rising_is_leading[i]),
          .enable_trigger(enable_trigger),
          .channel_data_debug(channel_data_debug),
          
          .chnl_ID(i),
          
          .Rdy_r(Rdy_r[i]),.Rdy_f(Rdy_f[i]), // ready
          .q_r(q_r[i]),.q_f(q_f[i]), // raw fine codes 
          .cnt_r(cnt_r[i]), .cnt_inv_r(cnt_inv_r[i]),.cnt_f(cnt_f[i]), .cnt_inv_f(cnt_inv_f[i]), // coarse counter
          
          .fake_hit(fake_hit),
          
          .fifo_read(chnl_fifo_read[i]),
          .fifo_read_clk(clk160),
          .channel_data_out(chnl_data[(i*45+44) : i*45]),
          .fifo_empty(chnl_fifo_empty[i]),
          .fifo_full(chnl_fifo_full[i]),


          .trigger(get_trigger),
          .limit_up(limit_up),
          .limit_down(limit_down),
          .trigger_processing(trigger_processing[i]),

          .chnl_fifo_overflow(chnl_fifo_overflow[i]),//work in triggerless mode when fifo is full, but still need to write
          .chnl_fifo_overflow_clear(chnl_fifo_overflow_clear)
      );
  end
endgenerate

tdc_readout_logic #( .CHANNEL_NUMBER(24), .READ_OUT_FIFO_ASIZE(4))
  tdc_readout_logic_inst(
    .rst(rst),      // reset, not BC reset
    .clk160(clk160),
    .clk320(clk320),

    .d_line(d_line),

    //config
    .channel_data_debug(channel_data_debug),
    .enable_trigger(enable_trigger),
    .enable_trigger_timeout(enable_trigger_timeout),
    .enable_leading(enable_leading),
    .enable_pair(enable_pair),
    .roll_over(roll_over),
    .full_width_res(full_width_res),
    .width_select(width_select),  
    .enable_error_packet(enable_error_packet),
    .enable_8b10b(enable_8b10b),
    .enable_insert(enable_insert),
    .enable_TDC_ID(enable_TDC_ID),
    .TDC_ID(TDC_ID),
    .enable_config_error_notify(enable_config_error_notify),
    .config_error(config_error),
    .enable_high_speed(enable_high_speed),
    .enable_legacy(enable_legacy),
    .syn_packet_number(syn_packet_number),
    
    //interface to channel logic
    .chnl_fifo_empty(chnl_fifo_empty),
    .chnl_fifo_full(chnl_fifo_full),
    .chnl_fifo_read(chnl_fifo_read),
    .chnl_data(chnl_data),
    
    //interface to channel logic for trigger
    .trigger_processing(trigger_processing),
    .channel_fifo_clear(channel_fifo_clear),
    .get_trigger(get_trigger),
    
    //interface to trigger interface 
    .trigger_fifo_empty(trigger_fifo_empty),
    .trigger_event_id(trigger_event_id),
    .trigger_bunch_id(trigger_bunch_id),
    .error_flag(error_flag),

    .debug_port(debug_port[3:0])
  );


trigger_interface 
  trigger_interface_inst(
    .clk(clk160),
    .rst(rst),
    
    .enable(enable_trigger),
  
    .reset_event_id(reset_event_id),
    .trigger_count_reset(trigger_count_reset),
    .bunch_offset(bunch_offset),
    .event_offset(event_offset),
    .roll_over(roll_over),
    .match_window(match_window),
  
    .trigger_in(trigger),
  
    .get_trigger(get_trigger),
    .trigger_fifo_rd_clk(clk160),
    .trigger_fifo_empty(trigger_fifo_empty),
    .event_id(trigger_event_id),
    .bunch_id(trigger_bunch_id),
    .limit_up(limit_up),
    .limit_down(limit_down),
  
    .trigger_lost(trigger_lost)
  );
fake_hit_generator #(.WIDTH(12))
  fake_hit_generator_inst(
     .clk(clk160),
     .rst(rst),
     .enable(enable_trigger&enbale_fake_hit),
     .fake_hit(fake_hit),
     .time_interval(fake_hit_time_interval)
  );

reg debug_port_4_r,debug_port_5_r;
always @(posedge clk160) begin
  debug_port_4_r <= fake_hit;
  debug_port_5_r <= get_trigger;
end

assign debug_port[4]=debug_port_4_r;
assign debug_port[5]=debug_port_5_r;

endmodule