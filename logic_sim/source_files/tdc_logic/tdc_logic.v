/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_logic.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 2st, 2017
//  Note       : 
// 
`include "common_definition.v"
module tdc_logic(
    input clk320,
    input clk160,
    
    input tck, 
    input tms, 
    input tdi, 
    input trst, 
    output tdo, 
    
    output ASD_TCK,
    output ASD_shift_out,
    output ASD_update_out,
    output reg ASD_data_out_debug,
    input  ASD_data_in,

    input encoded_control_in,
    input reset_in,
//    input trigger_direct_in,
    input bunch_reset_direct_in,
//    input event_reset_direct_in,
//    output reg debug_output,
    
    input Rdy_r_0,Rdy_r_1,Rdy_r_2,Rdy_r_3,Rdy_r_4,Rdy_r_5,Rdy_r_6,Rdy_r_7,Rdy_r_8,Rdy_r_9,Rdy_r_10,Rdy_r_11,
    input Rdy_r_12,Rdy_r_13,Rdy_r_14,Rdy_r_15,Rdy_r_16,Rdy_r_17,Rdy_r_18,Rdy_r_19,Rdy_r_20,Rdy_r_21,Rdy_r_22,Rdy_r_23,
    
    input Rdy_f_0,Rdy_f_1,Rdy_f_2,Rdy_f_3,Rdy_f_4,Rdy_f_5,Rdy_f_6,Rdy_f_7,Rdy_f_8,Rdy_f_9,Rdy_f_10,Rdy_f_11,
    input Rdy_f_12,Rdy_f_13,Rdy_f_14,Rdy_f_15,Rdy_f_16,Rdy_f_17,Rdy_f_18,Rdy_f_19,Rdy_f_20,Rdy_f_21,Rdy_f_22,Rdy_f_23,
    
    input [3:0] q_r_0,q_r_1,q_r_2,q_r_3,q_r_4,q_r_5,q_r_6,q_r_7,q_r_8,q_r_9,q_r_10,q_r_11,
    input [3:0] q_r_12,q_r_13,q_r_14,q_r_15,q_r_16,q_r_17,q_r_18,q_r_19,q_r_20,q_r_21,q_r_22,q_r_23,
    
    input [3:0] q_f_0,q_f_1,q_f_2,q_f_3,q_f_4,q_f_5,q_f_6,q_f_7,q_f_8,q_f_9,q_f_10,q_f_11,
    input [3:0] q_f_12,q_f_13,q_f_14,q_f_15,q_f_16,q_f_17,q_f_18,q_f_19,q_f_20,q_f_21,q_f_22,q_f_23,
    
    input [14:0] cnt_r_0,cnt_r_1,cnt_r_2,cnt_r_3,cnt_r_4,cnt_r_5,cnt_r_6,cnt_r_7,cnt_r_8,cnt_r_9,cnt_r_10,cnt_r_11,
    input [14:0] cnt_r_12,cnt_r_13,cnt_r_14,cnt_r_15,cnt_r_16,cnt_r_17,cnt_r_18,cnt_r_19,cnt_r_20,cnt_r_21,cnt_r_22,cnt_r_23,
    
    input [14:0] cnt_inv_r_0,cnt_inv_r_1,cnt_inv_r_2,cnt_inv_r_3,cnt_inv_r_4,cnt_inv_r_5,cnt_inv_r_6,cnt_inv_r_7,cnt_inv_r_8,cnt_inv_r_9,cnt_inv_r_10,cnt_inv_r_11,
    input [14:0] cnt_inv_r_12,cnt_inv_r_13,cnt_inv_r_14,cnt_inv_r_15,cnt_inv_r_16,cnt_inv_r_17,cnt_inv_r_18,cnt_inv_r_19,cnt_inv_r_20,cnt_inv_r_21,cnt_inv_r_22,cnt_inv_r_23,
    
    input [14:0] cnt_f_0,cnt_f_1,cnt_f_2,cnt_f_3,cnt_f_4,cnt_f_5,cnt_f_6,cnt_f_7,cnt_f_8,cnt_f_9,cnt_f_10,cnt_f_11,
    input [14:0] cnt_f_12,cnt_f_13,cnt_f_14,cnt_f_15,cnt_f_16,cnt_f_17,cnt_f_18,cnt_f_19,cnt_f_20,cnt_f_21,cnt_f_22,cnt_f_23,
    
    input [14:0] cnt_inv_f_0,cnt_inv_f_1,cnt_inv_f_2,cnt_inv_f_3,cnt_inv_f_4,cnt_inv_f_5,cnt_inv_f_6,cnt_inv_f_7,cnt_inv_f_8,cnt_inv_f_9,cnt_inv_f_10,cnt_inv_f_11,
    input [14:0] cnt_inv_f_12,cnt_inv_f_13,cnt_inv_f_14,cnt_inv_f_15,cnt_inv_f_16,cnt_inv_f_17,cnt_inv_f_18,cnt_inv_f_19,cnt_inv_f_20,cnt_inv_f_21,cnt_inv_f_22,cnt_inv_f_23,


    output coarse_bcr,

    output [1:0] d_line,

    input [5:0] debug_input,       
    
    output [4  :0] phase_clk160,
    output [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2,
    output         rst_ePLL,    
    output [3  :0] ePllResA, ePllResB, ePllResC,
    output [3  :0] ePllIcpA, ePllIcpB, ePllIcpC,
    output [1  :0] ePllCapA, ePllCapB, ePllCapC,    
    input          ePll_lock
);

wire        config_reset_jtag_in;
wire        config_event_teset_jtag_in;
wire        config_enable_new_ttc;
wire        config_enable_master_reset_code;
wire        config_enable_trigger;
wire [11:0] config_bunch_offset;
wire [11:0] config_event_offset;
wire [11:0] config_match_window;
wire        config_enable_direct_trigger;
wire        config_enable_direct_bunch_reset;
wire        config_enable_direct_event_reset;
wire [11:0] config_roll_over;
wire [11:0] config_coarse_count_offset;
wire        config_auto_roll_over;
wire        config_bypass_bcr_distribution;
wire        config_channel_data_debug;
wire        config_enbale_fake_hit;
wire [11:0] config_fake_hit_time_interval;
wire        config_enable_trigger_timeout;
wire        config_enable_leading;
wire        config_enable_pair;
wire 		config_full_width_res;
wire [2:0]  config_width_select;
wire        config_enable_8b10b;
wire        config_enable_insert;
wire        config_enable_error_packet;
wire        config_enable_TDC_ID;
wire [18:0] config_TDC_ID;
wire        config_enable_config_error_notify;
wire        config_config_error;
wire        config_enable_high_speed;
wire        config_enable_legacy;
wire [`MAX_PACKET_NUM_SIZE-1:0] config_syn_packet_number;
wire [23:0] config_rising_is_leading;
wire [`COMBINE_TIME_OUT_SIZE-1:0] config_combine_time_out_config;
wire [23:0] config_channel_enable_r;
wire [23:0] config_channel_enable_f;
wire [3:0]  config_fine_sel;
wire [1:0]  config_lut0,config_lut1,config_lut2,config_lut3,config_lut4,config_lut5,config_lut6,config_lut7;
wire [1:0]  config_lut8,config_lut9,config_luta,config_lutb,config_lutc,config_lutd,config_lute,config_lutf;
wire [3:0]  config_debug_port_select;

wire [23:0] chnl_fifo_overflow;
wire  config_chnl_fifo_overflow_clear;

wire trigger;
wire event_reset;
wire master_reset;

wire trigger_count_bcr;
wire ASD_data_out;
wire [8:0] debug_port;
assign debug_port[6] = trigger;
assign debug_port[7] = coarse_bcr;
assign debug_port[8] = trigger_count_bcr;
always @(*) begin
  case(config_debug_port_select)
    4'h0:ASD_data_out_debug = ASD_data_out;
    4'h1:ASD_data_out_debug = debug_port[0];
    4'h2:ASD_data_out_debug = debug_port[1];
    4'h3:ASD_data_out_debug = debug_port[2]; 
    4'h4:ASD_data_out_debug = debug_port[3];
    4'h5:ASD_data_out_debug = debug_port[4];
    4'h6:ASD_data_out_debug = debug_port[5];
    4'h7:ASD_data_out_debug = debug_port[6];
    4'h8:ASD_data_out_debug = debug_port[7];
    4'h9:ASD_data_out_debug = debug_port[8];
    4'ha:ASD_data_out_debug = debug_input[0];
    4'hb:ASD_data_out_debug = debug_input[1];
    4'hc:ASD_data_out_debug = debug_input[2];
    4'hd:ASD_data_out_debug = debug_input[3];
    4'he:ASD_data_out_debug = debug_input[4];
    4'hf:ASD_data_out_debug = debug_input[5];
    default: ASD_data_out_debug = ASD_data_out;
  endcase
end

wire tdi_to_dr;
wire setup0_shift,setup0_in;
wire setup1_shift,setup1_in;
wire setup2_shift,setup2_in;
wire control0_enable,control0_update,control0_shift,control0_in;
wire control1_enable,control1_update,control1_shift,control1_in;
wire status0_shift,status0_in;
wire status1_shift,status1_in;

wire instruction_error;
wire tdo_jtag;
wire tdo_b;
assign tdo = trst ? tdo_jtag : tdo_b;


jtag_controller
    jtag_controller_inst( 
	   .tck(tck), 
	   .tms(tms), 
	   .tdi(tdi), 
	   .trst(trst), 
	   .tdo(tdo_jtag), 
	   .enable(), 
    
	   .tdi_to_dr(tdi_to_dr), 
    
        .setup0_shift(setup0_shift),
        .setup0_in(setup0_in), 
    
        .setup1_shift(setup1_shift),
        .setup1_in(setup1_in), 
    
        .setup2_shift(setup2_shift),
        .setup2_in(setup2_in), 
    
        .control0_enable(control0_enable), 
        .control0_update(control0_update), 
        .control0_shift(control0_shift),
        .control0_in(control0_in), 
    
        .control1_enable(control1_enable), 
        .control1_update(control1_update), 
        .control1_shift(control1_shift),
        .control1_in(control1_in), 
    
        .status0_shift(status0_shift),
        .status0_in(status0_in), 
    
        .status1_shift(status1_shift),
        .status1_in(status1_in), 
    
        .ASD_TCK(ASD_TCK),
        .ASD_shift_out(ASD_shift_out),
        .ASD_update_out(ASD_update_out),
        .ASD_data_out(ASD_data_out),
        .ASD_data_in(ASD_data_in),

	   .instruction_error(instruction_error)
    );

common_setup
    common_setup_inst(
        .tck(tck),
        .trst(trst),
        .tdi(tdi),
        .tdo_b(tdo_b),
        .tms(tms),
    
        .tdi_to_dr(tdi_to_dr),
    
        .setup0_shift(setup0_shift),
        .setup0_data(setup0_in),
    
        .setup1_shift(setup1_shift),
        .setup1_data(setup1_in),
    
        .setup2_shift(setup2_shift),
        .setup2_data(setup2_in),
    
    
        .control0_shift(control0_shift),
        .control0_update(control0_update),
        .control0_enable(control0_enable),
        .control0_data(control0_in),
    
        .control1_shift(control1_shift),
        .control1_update(control1_update),
        .control1_enable(control1_enable),
        .control1_data(control1_in),
    
        .status0_shift(status0_shift),
        .status0_data(status0_in),
    
        .status1_shift(status1_shift),
        .status1_data(status1_in),
    
    
        .phase_clk160(phase_clk160),
        .phase_clk320_0(phase_clk320_0), .phase_clk320_1(phase_clk320_1), .phase_clk320_2(phase_clk320_2),
        .rst_ePLL(rst_ePLL),
        
        .ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
        .ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
        .ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),
        
        .ePll_lock(ePll_lock),
        
        
        .config_error(config_config_error),
        
        
        .debug_port_select(config_debug_port_select),
        .chnl_fifo_overflow_clear(config_chnl_fifo_overflow_clear),
        .chnl_fifo_overflow(chnl_fifo_overflow),
        .instruction_error(instruction_error),
        //for TTC interface
        .reset_jtag_in(config_reset_jtag_in),
        .event_teset_jtag_in(config_event_teset_jtag_in),
        
        .enable_new_ttc(config_enable_new_ttc),
        .enable_master_reset_code(config_enable_master_reset_code),
        .enable_direct_bunch_reset(config_enable_direct_bunch_reset),
        .enable_direct_event_reset(config_enable_direct_event_reset),
        .enable_direct_trigger(config_enable_direct_trigger),
        
        //for bcr
        .roll_over(config_roll_over),
        .coarse_count_offset(config_coarse_count_offset),
        .auto_roll_over(config_auto_roll_over),
        .bypass_bcr_distribution(config_bypass_bcr_distribution),
        
        //channel model
        .enable_trigger(config_enable_trigger),
        .channel_data_debug(config_channel_data_debug),
        .enable_leading(config_enable_leading),
        .enable_pair(config_enable_pair),
        .enbale_fake_hit(config_enbale_fake_hit),
        .fake_hit_time_interval(config_fake_hit_time_interval),
        .rising_is_leading(config_rising_is_leading),
        .combine_time_out_config(config_combine_time_out_config),
        .channel_enable_r(config_channel_enable_r),
        .channel_enable_f(config_channel_enable_f),
        
        //for trigger para
        .bunch_offset(config_bunch_offset),
        .event_offset(config_event_offset),
        .match_window(config_match_window),
        
        
        //for readout interface
        .enable_trigger_timeout(config_enable_trigger_timeout),
        .enable_high_speed(config_enable_high_speed),
        .enable_legacy(config_enable_legacy),
        .full_width_res(config_full_width_res),
        .width_select(config_width_select),
        .enable_8b10b(config_enable_8b10b),
        .enable_insert(config_enable_insert),
        .syn_packet_number(config_syn_packet_number),
        .enable_error_packet(config_enable_error_packet),
        .enable_TDC_ID(config_enable_TDC_ID),
        .TDC_ID(config_TDC_ID),
        .enable_error_notify(config_enable_config_error_notify),
        
        
        .fine_sel(config_fine_sel),
        .lut0(config_lut0),.lut1(config_lut1),.lut2(config_lut2),.lut3(config_lut3),.lut4(config_lut4),.lut5(config_lut5),.lut6(config_lut6),.lut7(config_lut7),
        .lut8(config_lut8),.lut9(config_lut9),.luta(config_luta),.lutb(config_lutb),.lutc(config_lutc),.lutd(config_lutd),.lute(config_lute),.lutf(config_lutf)
    );

ttc_control_decoder
    ttc_control_decoder_inst( 
       .clk160(clk160), 
    
       .reset_in(reset_in), 
       .trigger_direct_in(1'b0),
       .bunch_reset_direct_in(bunch_reset_direct_in),
       .event_reset_direct_in(1'b0),
    
       .reset_jtag_in(config_reset_jtag_in), 
       .event_reset_jtag_in(config_event_teset_jtag_in),

       .encoded_control_in(encoded_control_in),
    
    
       .enable_new_ttc(config_enable_new_ttc),
       .enable_master_reset_code(config_enable_master_reset_code),
       .enable_trigger(config_enable_trigger),
       .enable_direct_trigger(config_enable_direct_trigger),
       .enable_direct_bunch_reset(config_enable_direct_bunch_reset),
       .enable_direct_event_reset(config_enable_direct_event_reset),
    
       .trigger(trigger),
    
       .event_reset(event_reset),
       .master_reset(master_reset),
    
//       .clk320(clk320),
    
       .roll_over(config_roll_over),
       .coarse_count_offset(config_coarse_count_offset),
       .auto_roll_over(config_auto_roll_over),
        .bypass_bcr_distribution(config_bypass_bcr_distribution),
    
       .coarse_bcr(coarse_bcr),
       .trigger_count_bcr(trigger_count_bcr)
    );

tdc_core_logic
	tdc_core_logic_inst(
        .rst(master_reset),
        .clk160(clk160),
        .clk320(clk320),

        //configuration
        .channel_data_debug(config_channel_data_debug),
        .enable_trigger(config_enable_trigger),
        .bunch_offset(config_bunch_offset),
        .event_offset(config_event_offset),
        .match_window(config_match_window),
        .enbale_fake_hit(config_enbale_fake_hit),
        .fake_hit_time_interval(config_fake_hit_time_interval),
        .enable_trigger_timeout(config_enable_trigger_timeout),
        .enable_leading(config_enable_leading),
        .enable_pair(config_enable_pair),
        .roll_over(config_roll_over),
        .full_width_res(config_full_width_res),
        .width_select(config_width_select),
        .enable_8b10b(config_enable_8b10b),
        .enable_insert(config_enable_insert),
        .enable_error_packet(config_enable_error_packet),
        .enable_TDC_ID(config_enable_TDC_ID),
        .TDC_ID(config_TDC_ID), 
        .enable_config_error_notify(config_enable_config_error_notify),
        .config_error(config_config_error),  
        .enable_high_speed(config_enable_high_speed), 
        .enable_legacy(config_enable_legacy),
        .syn_packet_number(config_syn_packet_number),
        .rising_is_leading(config_rising_is_leading),
        .combine_time_out_config(config_combine_time_out_config),
        .channel_enable_r(config_channel_enable_r),
        .channel_enable_f(config_channel_enable_f),
        .fine_sel(config_fine_sel),
        .lut0(config_lut0),.lut1(config_lut1),.lut2(config_lut2),.lut3(config_lut3),.lut4(config_lut4),.lut5(config_lut5),.lut6(config_lut6),.lut7(config_lut7),
        .lut8(config_lut8),.lut9(config_lut9),.luta(config_luta),.lutb(config_lutb),.lutc(config_lutc),.lutd(config_lutd),.lute(config_lute),.lutf(config_lutf),
  
        .chnl_fifo_overflow(chnl_fifo_overflow),
        .chnl_fifo_overflow_clear(config_chnl_fifo_overflow_clear),
      
        .debug_port(debug_port[5:0]),
  
  
        .Rdy_r_0(Rdy_r_0), .Rdy_r_1(Rdy_r_1), .Rdy_r_2(Rdy_r_2), .Rdy_r_3(Rdy_r_3), .Rdy_r_4(Rdy_r_4), .Rdy_r_5(Rdy_r_5), .Rdy_r_6(Rdy_r_6), .Rdy_r_7(Rdy_r_7), .Rdy_r_8(Rdy_r_8), .Rdy_r_9(Rdy_r_9), .Rdy_r_10(Rdy_r_10), .Rdy_r_11(Rdy_r_11), 
        .Rdy_r_12(Rdy_r_12), .Rdy_r_13(Rdy_r_13), .Rdy_r_14(Rdy_r_14), .Rdy_r_15(Rdy_r_15), .Rdy_r_16(Rdy_r_16), .Rdy_r_17(Rdy_r_17), .Rdy_r_18(Rdy_r_18), .Rdy_r_19(Rdy_r_19), .Rdy_r_20(Rdy_r_20), .Rdy_r_21(Rdy_r_21), .Rdy_r_22(Rdy_r_22), .Rdy_r_23(Rdy_r_23), 
    
        .Rdy_f_0(Rdy_f_0), .Rdy_f_1(Rdy_f_1), .Rdy_f_2(Rdy_f_2), .Rdy_f_3(Rdy_f_3), .Rdy_f_4(Rdy_f_4), .Rdy_f_5(Rdy_f_5), .Rdy_f_6(Rdy_f_6), .Rdy_f_7(Rdy_f_7), .Rdy_f_8(Rdy_f_8), .Rdy_f_9(Rdy_f_9), .Rdy_f_10(Rdy_f_10), .Rdy_f_11(Rdy_f_11), 
        .Rdy_f_12(Rdy_f_12), .Rdy_f_13(Rdy_f_13), .Rdy_f_14(Rdy_f_14), .Rdy_f_15(Rdy_f_15), .Rdy_f_16(Rdy_f_16), .Rdy_f_17(Rdy_f_17), .Rdy_f_18(Rdy_f_18), .Rdy_f_19(Rdy_f_19), .Rdy_f_20(Rdy_f_20), .Rdy_f_21(Rdy_f_21), .Rdy_f_22(Rdy_f_22), .Rdy_f_23(Rdy_f_23),
    
        .q_r_0(q_r_0), .q_r_1(q_r_1), .q_r_2(q_r_2), .q_r_3(q_r_3), .q_r_4(q_r_4), .q_r_5(q_r_5), .q_r_6(q_r_6), .q_r_7(q_r_7), .q_r_8(q_r_8), .q_r_9(q_r_9), .q_r_10(q_r_10), .q_r_11(q_r_11), 
        .q_r_12(q_r_12), .q_r_13(q_r_13), .q_r_14(q_r_14), .q_r_15(q_r_15), .q_r_16(q_r_16), .q_r_17(q_r_17), .q_r_18(q_r_18), .q_r_19(q_r_19), .q_r_20(q_r_20), .q_r_21(q_r_21), .q_r_22(q_r_22), .q_r_23(q_r_23),
        
        .q_f_0(q_f_0), .q_f_1(q_f_1), .q_f_2(q_f_2), .q_f_3(q_f_3), .q_f_4(q_f_4), .q_f_5(q_f_5), .q_f_6(q_f_6), .q_f_7(q_f_7), .q_f_8(q_f_8), .q_f_9(q_f_9), .q_f_10(q_f_10), .q_f_11(q_f_11), 
        .q_f_12(q_f_12), .q_f_13(q_f_13), .q_f_14(q_f_14), .q_f_15(q_f_15), .q_f_16(q_f_16), .q_f_17(q_f_17), .q_f_18(q_f_18), .q_f_19(q_f_19), .q_f_20(q_f_20), .q_f_21(q_f_21), .q_f_22(q_f_22), .q_f_23(q_f_23), 
        
        .cnt_r_0(cnt_r_0), .cnt_r_1(cnt_r_1), .cnt_r_2(cnt_r_2), .cnt_r_3(cnt_r_3), .cnt_r_4(cnt_r_4), .cnt_r_5(cnt_r_5), .cnt_r_6(cnt_r_6), .cnt_r_7(cnt_r_7), .cnt_r_8(cnt_r_8), .cnt_r_9(cnt_r_9), .cnt_r_10(cnt_r_10), .cnt_r_11(cnt_r_11), 
        .cnt_r_12(cnt_r_12), .cnt_r_13(cnt_r_13), .cnt_r_14(cnt_r_14), .cnt_r_15(cnt_r_15), .cnt_r_16(cnt_r_16), .cnt_r_17(cnt_r_17), .cnt_r_18(cnt_r_18), .cnt_r_19(cnt_r_19), .cnt_r_20(cnt_r_20), .cnt_r_21(cnt_r_21), .cnt_r_22(cnt_r_22), .cnt_r_23(cnt_r_23), 
    
        .cnt_inv_r_0(cnt_inv_r_0), .cnt_inv_r_1(cnt_inv_r_1), .cnt_inv_r_2(cnt_inv_r_2), .cnt_inv_r_3(cnt_inv_r_3), .cnt_inv_r_4(cnt_inv_r_4), .cnt_inv_r_5(cnt_inv_r_5), .cnt_inv_r_6(cnt_inv_r_6), .cnt_inv_r_7(cnt_inv_r_7), .cnt_inv_r_8(cnt_inv_r_8), .cnt_inv_r_9(cnt_inv_r_9), .cnt_inv_r_10(cnt_inv_r_10), .cnt_inv_r_11(cnt_inv_r_11), 
        .cnt_inv_r_12(cnt_inv_r_12), .cnt_inv_r_13(cnt_inv_r_13), .cnt_inv_r_14(cnt_inv_r_14), .cnt_inv_r_15(cnt_inv_r_15), .cnt_inv_r_16(cnt_inv_r_16), .cnt_inv_r_17(cnt_inv_r_17), .cnt_inv_r_18(cnt_inv_r_18), .cnt_inv_r_19(cnt_inv_r_19), .cnt_inv_r_20(cnt_inv_r_20), .cnt_inv_r_21(cnt_inv_r_21), .cnt_inv_r_22(cnt_inv_r_22), .cnt_inv_r_23(cnt_inv_r_23), 
    
        .cnt_f_0(cnt_f_0), .cnt_f_1(cnt_f_1), .cnt_f_2(cnt_f_2), .cnt_f_3(cnt_f_3), .cnt_f_4(cnt_f_4), .cnt_f_5(cnt_f_5), .cnt_f_6(cnt_f_6), .cnt_f_7(cnt_f_7), .cnt_f_8(cnt_f_8), .cnt_f_9(cnt_f_9), .cnt_f_10(cnt_f_10), .cnt_f_11(cnt_f_11), 
        .cnt_f_12(cnt_f_12), .cnt_f_13(cnt_f_13), .cnt_f_14(cnt_f_14), .cnt_f_15(cnt_f_15), .cnt_f_16(cnt_f_16), .cnt_f_17(cnt_f_17), .cnt_f_18(cnt_f_18), .cnt_f_19(cnt_f_19), .cnt_f_20(cnt_f_20), .cnt_f_21(cnt_f_21), .cnt_f_22(cnt_f_22), .cnt_f_23(cnt_f_23), 
    
        .cnt_inv_f_0(cnt_inv_f_0), .cnt_inv_f_1(cnt_inv_f_1), .cnt_inv_f_2(cnt_inv_f_2), .cnt_inv_f_3(cnt_inv_f_3), .cnt_inv_f_4(cnt_inv_f_4), .cnt_inv_f_5(cnt_inv_f_5), .cnt_inv_f_6(cnt_inv_f_6), .cnt_inv_f_7(cnt_inv_f_7), .cnt_inv_f_8(cnt_inv_f_8), .cnt_inv_f_9(cnt_inv_f_9), .cnt_inv_f_10(cnt_inv_f_10), .cnt_inv_f_11(cnt_inv_f_11), 
        .cnt_inv_f_12(cnt_inv_f_12), .cnt_inv_f_13(cnt_inv_f_13), .cnt_inv_f_14(cnt_inv_f_14), .cnt_inv_f_15(cnt_inv_f_15), .cnt_inv_f_16(cnt_inv_f_16), .cnt_inv_f_17(cnt_inv_f_17), .cnt_inv_f_18(cnt_inv_f_18), .cnt_inv_f_19(cnt_inv_f_19), .cnt_inv_f_20(cnt_inv_f_20), .cnt_inv_f_21(cnt_inv_f_21), .cnt_inv_f_22(cnt_inv_f_22), .cnt_inv_f_23(cnt_inv_f_23), 
    
        .reset_event_id(event_reset),
        .trigger_count_reset(trigger_count_bcr),
        .trigger(trigger),
    
        .d_line(d_line)
    );

endmodule
