/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_readout_logic.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 12nd, 2018
//  Note       : 
// 

`include "common_definition.v"
module tdc_readout_logic 
#(parameter 
    CHANNEL_NUMBER=24,
    READ_OUT_FIFO_ASIZE=4
)
(
  input             rst,      // reset, not BC reset
  input             clk160,
  input             clk320,

  output reg [1:0] d_line,

  //config
  input channel_data_debug,
  input enable_trigger,
  input enable_trigger_timeout,
  input enable_leading,
  input enable_pair,
  input [11:0] roll_over,
  input full_width_res,
  input [2:0] width_select,  
  input enable_error_packet,
  input enable_insert,
  input enable_8b10b,
  input enable_TDC_ID,
  input [18:0] TDC_ID,
  input enable_config_error_notify,
  input config_error,
  input enable_high_speed,
  input enable_legacy,
  input  [`MAX_PACKET_NUM_SIZE-1:0] syn_packet_number,

  //interface to channel logic
  input [CHANNEL_NUMBER -1 : 0] chnl_fifo_empty,
  input [CHANNEL_NUMBER -1 : 0] chnl_fifo_full,
  output [CHANNEL_NUMBER -1 : 0] chnl_fifo_read,
  input [CHANNEL_NUMBER*45-1 : 0] chnl_data,
  
  //interface to channel logic for trigger
  input [CHANNEL_NUMBER -1 : 0] trigger_processing,
  output channel_fifo_clear,
  output get_trigger,

  //interface to trigger interface 
  input trigger_fifo_empty,
  input[11:0] trigger_event_id,
  input[11:0] trigger_bunch_id,
  input[(24-`TRIGGER_MATCHING_HIT_COUNT_SIZE)-1-1:0] error_flag,

  output reg [3:0] debug_port
);

  wire channel_fifo_clear_trigger;
  assign channel_fifo_clear = channel_fifo_clear_trigger & enable_trigger ;
  wire read_out_fifo_full;
  wire data_ready_trigger;
  wire [44:0] data_out_trigger;
  wire trigger_head,trigger_trail;
  wire [CHANNEL_NUMBER-1:0] chnl_fifo_read_trigger;
  trigger_event_builder2 #(.CHANNEL_NUMBER(CHANNEL_NUMBER), .CHANNEL_DATA_WIDTH(45))
    trigger_event_builder2_inst(
      .clk(clk160),
      .rst(rst),

      .enable(enable_trigger),
      .enable_timeout(enable_trigger_timeout),

      .trigger_fifo_empty(trigger_fifo_empty),
      .trigger_event_id(trigger_event_id),
      .trigger_bunch_id(trigger_bunch_id),
      .error_flag(error_flag),

      .trigger_processing(trigger_processing),
      .chnl_fifo_empty(chnl_fifo_empty),
      .chnl_fifo_read(chnl_fifo_read_trigger),
      .chnl_data(chnl_data),
      .channel_fifo_clear(channel_fifo_clear_trigger),

      .get_trigger(get_trigger), 

      .read_out_fifo_full(read_out_fifo_full),

      .data_ready(data_ready_trigger),
      .data_out(data_out_trigger),
      .trigger_head(trigger_head),
      .trigger_trail(trigger_trail)
    );

  wire data_ready_triggerless;
  wire [44:0] data_out_triggerless;
  wire [CHANNEL_NUMBER-1:0] chnl_fifo_read_triggerless;
  triggerless_channel_mux #(.CHANNEL_NUMBER(CHANNEL_NUMBER),.CHANNEL_DATA_WIDTH(45))
    triggerless_channel_mux_inst(
      .clk(clk160),
      .rst(rst),
      .enable(~enable_trigger),
      .channel_data_debug(channel_data_debug),

      .channel_data(chnl_data),
      .channel_fifo_full(chnl_fifo_full),
      .channel_fifo_empty(chnl_fifo_empty),
      .channel_fifo_read(chnl_fifo_read_triggerless),

      .read_out_fifo_full(read_out_fifo_full),    

      .data_ready(data_ready_triggerless),
      .data_out(data_out_triggerless)
    );
  
  mux #(.WIDTH(CHANNEL_NUMBER))
    channel_read_mux(
      .a(chnl_fifo_read_trigger),
      .b(chnl_fifo_read_triggerless),
      .out(chnl_fifo_read),
      .sel(enable_trigger)
    );



  wire data_ready;
  wire [44:0] data_out;
  mux #(.WIDTH(46))
    read_out_mux(
      .a({data_ready_trigger,data_out_trigger}),
      .b({data_ready_triggerless,data_out_triggerless}),
      .out({data_ready,data_out}),
      .sel(enable_trigger)
    );
//  mux_r #(.WIDTH(46))
//    read_out_mux(
//      .a({data_ready_trigger,data_out_trigger}),
//      .b({data_ready_triggerless,data_out_triggerless}),
//      .enable(~read_out_fifo_full),
//      .clk(clk160),
//      .rst(rst),
//      .out({data_ready,data_out}),
//      .sel(enable_trigger)
//    );



  wire read_out_fifo_write;
  wire [`READ_OUT_FIFO_SIZE-1:0] read_out_data_write;
  data_recombine_process
    data_recombine_process_inst(
      .clk(clk160),
      .rst(rst),

      .channel_data_debug(channel_data_debug),

      .data_ready(data_ready),
      .data_in(data_out),
      .read_out_fifo_full(read_out_fifo_full),
      .read_out_fifo_write(read_out_fifo_write),
      .read_out_fifo_data(read_out_data_write),

      .enable_trigger(enable_trigger),
      .enable_leading(enable_leading),
      .enable_pair(enable_pair),
      .roll_over(roll_over),
      .full_width_res(full_width_res),
      .width_select(width_select),  

      .trigger_head(trigger_head&enable_trigger),
      .trigger_trail(trigger_trail&enable_trigger)
    );



  wire read_out_fifo_empty;
  wire read_out_fifo_read,read_out_fifo_read_high_speed,read_out_fifo_read_legacy;
  assign read_out_fifo_read = enable_legacy ? read_out_fifo_read_legacy : read_out_fifo_read_high_speed;
  wire [`READ_OUT_FIFO_SIZE-1:0] read_out_fifo_data;
  //read out FIFO
  fifo16 #(.DSIZE(`READ_OUT_FIFO_SIZE),.ASIZE(READ_OUT_FIFO_ASIZE),.SYN_DEPTH(1)) 
    readout_fifo_inst (
      .rdata(read_out_fifo_data),
      .wfull(read_out_fifo_full),
      .rempty(read_out_fifo_empty),
      .wdata(read_out_data_write),
      .winc(read_out_fifo_write), .wclk(clk160), .wrst(rst),
      .rinc(read_out_fifo_read), .rclk(clk160), .rrst(rst)
    );



wire interface_fifo_full;
wire interface_fifo_write;
wire [9:0] interface_fifo_data_write;
data_packeter2
  data_packeter_inst(
  .clk(clk160),
  .rst(rst),

//config
  .enable_pair(enable_pair),
  .enable_trigger(enable_trigger),
  .enable_leading(enable_leading),
  .enable_error_packet(enable_error_packet),
  .enable_8b10b(enable_8b10b),
  .enable_insert(enable_insert),
  .enable_TDC_ID(enable_TDC_ID),
  .TDC_ID(TDC_ID),
  .enable_config_error_notify(enable_config_error_notify),
  .config_error(config_error),
  .syn_packet_number(syn_packet_number),

  .read_out_fifo_empty(read_out_fifo_empty),
  .read_out_fifo_read(read_out_fifo_read_high_speed),
  .read_out_fifo_data(read_out_fifo_data),

  .interface_fifo_full(interface_fifo_full),
  .interface_fifo_write(interface_fifo_write),
  .interface_fifo_data(interface_fifo_data_write)
  );

wire interface_fifo_empty;
wire interface_fifo_read;
wire [9:0] interface_fifo_data;
  //interface  FIFO
fifo1 #(.DSIZE(10),.ASIZE(2),.SYN_DEPTH(2)) 
//fifo_no_syn #(.DSIZE(10),.ASIZE(2),.SYN_DEPTH(2)) 
//fifo_two_depth #(.DSIZE(10),.ASIZE(1),.SYN_DEPTH(1)) 
//fifo_exchange #(.DSIZE(10),.ASIZE(2),.SYN_DEPTH(2)) 
//fifo_width #(.DSIZE(10),.ASIZE(2),.SYN_DEPTH(2)) 
  interface_fifo_inst (
    .rdata(interface_fifo_data),
    .wfull(interface_fifo_full),
    .rempty(interface_fifo_empty),
    .wdata(interface_fifo_data_write),
    .winc(interface_fifo_write), .wclk(clk160), .wrst(rst),
    .rinc(interface_fifo_read), .rclk(clk320), .rrst(rst)
  );

wire [1:0] d_line_highspeed;
 serial_interface
  serial_interface_inst(
    .clk(clk320),
    .rst(rst),
  
    .enable_320(enable_high_speed),
    .enable(~enable_legacy),
  
    .interface_fifo_empty(interface_fifo_empty),
    .interface_fifo_read(interface_fifo_read),
    .interface_fifo_data(interface_fifo_data),
  
    .d_line(d_line_highspeed)
  );

wire [1:0] d_line_legacy;
legacy_serial_interface
  legacy_serial_interface_inst(
    .clk160(clk160),
    .rst(rst),

    .enable(enable_legacy),
//config
    .enable_pair(enable_pair),
    //.enable_trigger(enable_trigger),
    .enable_leading(enable_leading),
    .TDC_ID(TDC_ID),

    .read_out_fifo_empty(read_out_fifo_empty),
    .read_out_fifo_read(read_out_fifo_read_legacy),
    .read_out_fifo_data(read_out_fifo_data),

    .d_line_slow(d_line_legacy)
);

always @(posedge clk320  ) begin
  if (rst) begin
    // reset
    d_line <= 2'b0;
  end
  else  begin
    d_line <= enable_legacy ? d_line_legacy : d_line_highspeed;
  end
end
always @(posedge clk320) begin
  debug_port <= {d_line_highspeed[1],d_line_highspeed[0],d_line_legacy[1],d_line_legacy[0]};
end

endmodule