/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : trigger_interface.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 21st, 2018
//  Note       : 
//     
`include "common_definition.v"
module trigger_interface 
(
	input clk,
	input rst,
	
	input enable,

	input reset_event_id,
	input trigger_count_reset,
	input [11:0] bunch_offset,
	input [11:0] event_offset,
	input [11:0] roll_over,
	input [11:0] match_window,

	input trigger_in,

	input get_trigger,
	input trigger_fifo_rd_clk,
	output trigger_fifo_empty,
	output reg [11:0] event_id,
	output reg [11:0] bunch_id,
	output reg [11:0] limit_up,
	output reg [11:0] limit_down,

	output reg trigger_lost
);

wire trigger;
reg [1:0] trigger_syn_r;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_syn_r <= 2'b0;
	end	else begin
		trigger_syn_r <= {trigger_syn_r[0],trigger_in};
	end
end
assign  trigger= trigger_syn_r[1];

reg [1:0] trigger_r;
wire tigger_inner;
assign tigger_inner = (~trigger_r[1])&trigger_r[0]&enable ;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_r <= 2'b00;
	end	else  begin
		trigger_r  <= {trigger_r[0],trigger};
	end
end

//event counter
reg [11:0] event_count;
always @(posedge clk) begin
	if (reset_event_id ) begin
		event_count <=  event_offset;
	end else if (tigger_inner & enable) begin
		event_count <=  event_count + 12'b1;
	end
end

//bunch counter
wire trigger_count_reset_inner;
reg[1:0] trigger_count_reset_r;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_count_reset_r <= 2'b00;
	end	else if(enable) begin
		trigger_count_reset_r <= {trigger_count_reset_r[0],trigger_count_reset};
	end
end
assign trigger_count_reset_inner = (~trigger_count_reset_r[1])&trigger_count_reset_r[0];

wire roll_over_detect;
reg [13:0] trigger_count_r;
assign roll_over_detect = (trigger_count_r == {roll_over,2'b11});
wire [11:0] trigger_count; assign  trigger_count=trigger_count_r[13:2];
always @(posedge clk) begin
	if(enable) begin
		if (trigger_count_reset_inner) begin
			trigger_count_r <= {bunch_offset,2'b00};
		end else if(roll_over_detect) begin
			trigger_count_r <= 14'b0;
		end else trigger_count_r <= trigger_count_r + 14'b1;
	end
end


//combine trigger data
wire [23:0] trigger_data;
assign  trigger_data = {event_count,trigger_count};


wire [23:0] trigger_fifo_dataout;
fifo16 #(.DSIZE(24),.ASIZE(4),.SYN_DEPTH(2)) 
trigger_fifo(
	.rdata(trigger_fifo_dataout),
	.wfull(),
	.rempty(trigger_fifo_empty),
	.wdata(trigger_data),
	.winc(tigger_inner), .wclk(clk), .wrst(rst),
	.rinc(get_trigger), .rclk(trigger_fifo_rd_clk), .rrst(rst)
	);

reg [11:0] roll_over_plus_1_minus_match_windows;
always @(posedge clk  ) begin
	roll_over_plus_1_minus_match_windows <= roll_over+12'b1-match_window;
end


always @(posedge clk  ) begin
	if (rst) begin
		// reset
		bunch_id	<=12'b0;
		limit_up	<=12'hfff;
		limit_down	<=12'b0;
	end
	else if (get_trigger) begin
		bunch_id   <=  trigger_fifo_dataout[11:0] ;
		limit_up   <=  trigger_fifo_dataout[11:0] ;
		limit_down <=  trigger_fifo_dataout[11:0] < match_window ? (roll_over_plus_1_minus_match_windows + trigger_fifo_dataout[11:0]):(trigger_fifo_dataout[11:0] - match_window);
	end
end

reg [11:0] last_event_id;
always @(posedge clk ) begin
	if (reset_event_id) begin
		// reset
		last_event_id <= event_offset - 12'h002;
		event_id      <=event_offset - 12'h001;
	end
	else if (get_trigger) begin
		last_event_id <= event_id;
		event_id   <=  trigger_fifo_dataout[23:12];
	end
end
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_lost <= 1'b0;
	end
	else  begin
		trigger_lost <= ~((last_event_id + 12'b1)== event_id);
	end
end
endmodule