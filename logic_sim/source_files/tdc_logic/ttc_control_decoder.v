/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_control_decoder.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    

`include "common_definition.v"
module ttc_control_decoder
( 
	input clk160, 


	input reset_in, 
	input trigger_direct_in,
	input bunch_reset_direct_in,
	input event_reset_direct_in,

	input reset_jtag_in, 
	input event_reset_jtag_in,

	input encoded_control_in,
    
    input enable_new_ttc,
	input enable_master_reset_code,
	input enable_trigger,
	input enable_direct_trigger,
	input enable_direct_bunch_reset,
	input enable_direct_event_reset,

	output reg trigger,

	output reg event_reset,
	output master_reset,

//	input clk320,

	input [11:0] roll_over,
	input [11:0] coarse_count_offset,

	input auto_roll_over,
	input bypass_bcr_distribution,

	output  coarse_bcr,
	output  trigger_count_bcr

);

reg [1:0] reset_jtag_syn;
wire reset_jtag;
assign reset_jtag = reset_jtag_syn[1];
//sync jtag_reset
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		reset_jtag_syn <= 2'b0;
	end else begin
		reset_jtag_syn <=  {reset_jtag_syn[0],reset_jtag_in};
	end
end

reg [1:0] event_reset_jtag_syn;
wire event_reset_jtag;
assign  event_reset_jtag = event_reset_jtag_syn[1];
//sync jtag_reset
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		event_reset_jtag_syn <= 2'b0;
	end else begin
		event_reset_jtag_syn <=  {event_reset_jtag_syn[0],event_reset_jtag_in};
	end
end


wire bcr_TTC,trigger_TTC,event_reset_TTC,master_reset_TTC;
control_decoder
	control_decoder_inst( 
		.clk160(clk160), 
		.enable(~enable_new_ttc),	
		.reset_in(reset_in), 
	
		.encoded_control_in(encoded_control_in),
	
		.enable_master_reset_code(enable_master_reset_code),
		.enable_trigger(enable_trigger),

		.trigger(trigger_TTC),
		.bunch_reset(bcr_TTC),
		.event_reset(event_reset_TTC),
		.master_reset(master_reset_TTC)
	);
wire bcr_newTTC,trigger_newTTC,event_reset_newTTC,master_reset_newTTC;
control_decoder_with_newTTC
control_decoder_with_newTTC_inst( 
		.clk160(clk160), 
		.enable(enable_new_ttc),	
		.reset_in(reset_in), 
	
		.encoded_control_in(encoded_control_in),
	
		.enable_master_reset_code(enable_master_reset_code),
		.enable_trigger(enable_trigger),

		.trigger(trigger_newTTC),
		.bunch_reset(bcr_newTTC),
		.event_reset(event_reset_newTTC),
		.master_reset(master_reset_newTTC)
	);

reg  master_reset_r;
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		// reset
		master_reset_r  <= 1'b0;
	end	else begin
		master_reset_r <= enable_master_reset_code& (enable_new_ttc ? master_reset_newTTC : master_reset_TTC);
	end
end


reg [2:0] reset_r;
always @(posedge clk160) begin
	reset_r <= {reset_r[1:0],reset_jtag |master_reset_r|reset_in};
end
//assign  master_reset =  reset_in | reset_jtag |master_reset_r;
assign  master_reset = reset_r[2];

reg bunch_reset_direct_in_r;
always @(posedge clk160) begin
	bunch_reset_direct_in_r <= bunch_reset_direct_in;
end

reg bcr;
always @(posedge clk160) begin
	bcr <= enable_direct_bunch_reset ? bunch_reset_direct_in_r : (enable_new_ttc ? bcr_newTTC : bcr_TTC);
	event_reset <= (enable_direct_event_reset ? event_reset_direct_in: (enable_new_ttc ? event_reset_newTTC : event_reset_TTC)) | event_reset_jtag;
	trigger  <=  enable_trigger & (enable_direct_trigger ? trigger_direct_in : (enable_new_ttc ? trigger_newTTC : trigger_TTC));
end

bcr_distribution
	bcr_distribution_inst(
	.clk160(clk160),
//	.clk320(clk320),
	.rst(master_reset),

	.bcr(bcr),
	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),
	//input [11:0] bunch_offset,

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_bcr)
	);

endmodule
