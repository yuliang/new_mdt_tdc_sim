`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/01/2017 05:26:35 PM
// Design Name: 
// Module Name: JTAG_master
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module JTAG_master(
    input clk,
    output TCK,
    output TMS,
    output TDI,
    input TDO,
    input start_action,
    input [11:0] config_period,
    input [511:0] JTAG_bits,
    input [8:0] bit_length,
    input [4:0] JTAG_inst,
    output [511:0] JTAG_data_out,
    output reg JTAG_busy
    );
  wire tick;
  reg start_action_r=1'b0;
  always @(posedge clk) begin
    start_action_r <= start_action;
  end

  reg start_action_syn;
  always @(posedge clk) begin
    if ((~start_action_r)&start_action) begin
       start_action_syn <= 1'b1;
    end else if(start_action_syn&tick&TCK) begin
       start_action_syn <= 1'b0;
    end
  end  





    reg [11:0] counter= 12'b0;
    always @(posedge clk)
    begin
        if(counter != config_period) 
           counter <= counter + 12'b1;
        else 
           counter <= 12'b0;  
   end

   assign  tick = (counter == config_period);
   
   
   reg TCK_reg=1'b0;
   always @(posedge clk)
   begin
    if(tick)
        TCK_reg <= ~TCK_reg;
  end

  reg [4:0] TAP_state=5'b00000;
  reg TMS_reg=1'b1;
  reg TDI_reg=1'b1;
  reg [4:0] ir_reg=5'b10001;
  reg [511:0] JTAG_data='b0;
  reg [8:0]bit_counter='b0;
  reg [511:0] JTAG_bits_reg='b0;
  assign TDI = TDI_reg;
  assign TCK = TCK_reg;
  assign TMS = TMS_reg;
  assign  JTAG_data_out = JTAG_data;
  always @(posedge clk)
  begin 
    if(tick&TCK)begin
//        TMS_reg <= 1'b1;
        if(start_action_syn)begin
          TAP_state<=5'b00001;
          ir_reg <=JTAG_inst;
          JTAG_busy <= 1'b1;
          JTAG_bits_reg <= JTAG_bits;
        end
        else begin 
          if((TAP_state==5'b00001))begin//Reset->Idle
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end
          if((TAP_state==5'b00010))begin//Idle->Select-DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
          end
          if((TAP_state==5'b00011))begin//Select-DR->Select-IR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
          end        
          if((TAP_state==5'b00100))begin//Select-IR->Capture-IR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end   
          if((TAP_state==5'b00101))begin//Capture-IR->Shift IR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end     
          if((TAP_state< 5'b01010)&(TAP_state>5'b00101))begin//Shift IR->Shift IR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
              TDI_reg <= ir_reg[0];
              ir_reg <= {ir_reg[0],ir_reg[4:1]};
          end
          if((TAP_state==5'b01010))begin//shift IR->Exit1 IR
              TDI_reg <= ir_reg[0];
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
              ir_reg <= {ir_reg[0],ir_reg[4:1]};            
          end
          if((TAP_state==5'b01011))begin//Exit1 IR->Update IR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
          end  
          if((TAP_state==5'b01100))begin//Update IR->Idle
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end     
          if((TAP_state==5'b01101))begin//Idle->Idle
              TAP_state <= 5'b10001;
              TMS_reg <= 1'b0;
          end     
          if((TAP_state==5'b10001))begin//Idle->Idle
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end
          if((TAP_state==5'b10010))begin//Idle->Select-DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
          end
          if((TAP_state==5'b10011))begin//Select-DR->Capture-DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end        
          if((TAP_state==5'b10100))begin//Capture-DR->Shift DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
              bit_counter<=8'b0;
          end   
          if((TAP_state==5'b10101))begin//Shift DR->Shift DR
              if(bit_counter==bit_length-9'b1) TAP_state <= TAP_state + 8'b1;
              TMS_reg <= 1'b0;
              bit_counter <= bit_counter + 8'b1;
              TDI_reg <=  JTAG_bits_reg[0];
              JTAG_bits_reg <= {JTAG_bits_reg[0],JTAG_bits_reg[511:1]};
          end     
          if(TAP_state == 5'b10110)begin//Shift DR->Exit1 DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
              bit_counter<=8'b0;
              TDI_reg <=  JTAG_bits_reg[0];
              JTAG_bits_reg <= {JTAG_bits_reg[0],JTAG_bits_reg[511:1]};
          end
          if((TAP_state==5'b10111))begin//Exit1 DR>update DR
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b1;
          end
          if((TAP_state==5'b11000))begin//update DR->Idle
              TAP_state <= TAP_state + 5'b1;
              TMS_reg <= 1'b0;
          end  
          if((TAP_state==5'b11001))begin//Idle->Select-DR
              TAP_state <= 5'b11010;
              TMS_reg <= 1'b1;
          end   
          if((TAP_state==5'b11010))begin//Select-DR->Select-IR
              TAP_state <= 5'b11011;
              TMS_reg <= 1'b1;
          end  
          if((TAP_state==5'b11011))begin//Select-IR->reset
              TAP_state <= 5'b11100;
              TMS_reg <= 1'b1;
              JTAG_busy <= 1'b0;
          end 
        end  
    end   
  end
    
  reg TDO_valid=1'b0;
  always @(posedge clk) begin
    if(tick&~TCK)begin
      TDO_valid <= (TAP_state == 5'b10101)|(TAP_state == 5'b10110);
    end
  end

  always @(posedge clk) begin
  if(tick&~TCK)begin
    if (TAP_state == 5'b10100) begin
      JTAG_data <= 'b0;
    end
    if (TDO_valid) begin
      JTAG_data <= {TDO,JTAG_data[511:1]};
    end
  end
end  
    


//ila_Jtag_master Jtag_master (
//  .clk(clk), // input wire clk
//
//
//  .probe0(TCK), // input wire [0:0]  probe0  
//  .probe1(TMS), // input wire [0:0]  probe1 
//  .probe2(TDI), // input wire [0:0]  probe2 
//  .probe3(TDO), // input wire [0:0]  probe3 
//  .probe4(counter), // input wire [11:0]  probe4 
//  .probe5(tick), // input wire [0:0]  probe5 
//  .probe6(config_period), // input wire [11:0]  probe6 
//  .probe7(TAP_state), // input wire [4:0]  probe7 
//  .probe8(bit_counter), // input wire [8:0]  probe8 
//  .probe9(JTAG_inst), // input wire [4:0]  probe9 
//  .probe10(bit_length), // input wire [8:0]  probe10 
//  .probe11(TDO_valid), // input wire [0:0]  probe11 
//  .probe12(JTAG_data), // input wire [511:0]  probe12 
//  .probe13(JTAG_bits_reg) // input wire [511:0]  probe13 
//);


   
endmodule
