/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TDC_readout_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on 2019-02-12 16:56:15
//  Note       : 
//     
`timescale 1 ns / 1 fs
module TDC_readout_sim(
	input clk_hit,
	input reset,
	output  [23:0] hit
);

/*for random hit generator*/
wire [23:0] hit_random;
reg [23:0] hit_random_enable = 24'b0;
assign hit = hit_random&hit_random_enable;
reg hit_enable_out=1'b0;
reg [23:0] random_hit_last;
reg [31:0] random_hit_counter[23:0];
generate
	genvar i;
	for (i = 0; i < 24; i = i + 1)
	begin:tdc_channel
		random_hit_generator 
			random_hit_generator_inst(
				.clk(clk_hit),
				.rst(reset),
				.hit_out(hit_random[i]),
				.enable_out(hit_enable_out),   //flag when to enable random hit
				//.RATE(32'h0054FF42),//160M clk, 400kHz rate
				.RATE(32'd14800000),//32'h004C0720->200k,32'h00A0DDF9->400k,32'hFFFF_FFFF-> high ednsity h00A4E386->410k
				.seed1(32'hAD1659EF),
				.seed2(32'h3D9EF9ED),
				.dead_time_input(8'd158)//8'ha0
			);
		always @(posedge clk_hit  ) begin
			if (reset) begin
				// reset
				random_hit_last[i] <= 1'b0;
				random_hit_counter[i] <= 32'b0; 
			end	else begin
				random_hit_last[i] <= hit_random[i];
				random_hit_counter[i] <= random_hit_counter[i] +((~random_hit_last[i])&(hit_random[i]));
			end
	    end
	end
endgenerate

task random_hit_out_enable();
	begin 
		@(posedge clk_hit)
		hit_enable_out = 1'b1;
	end
endtask

task random_hit_out_disable();
	begin 
		@(posedge clk_hit)
		hit_enable_out = 1'b0;
	end
endtask
endmodule