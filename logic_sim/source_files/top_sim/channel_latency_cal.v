/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_latency_cal.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on Feb 18th, 2019
//  Note       :  hit latency monitor
// 
//////////////////////////////////////////////////////////////////////////////////

`include "common_definition.v"
`define TIME_OFFSET 118.75
`timescale 1 ns / 1 fs
module channel_latency_cal(
	input clk,
	input hit_in,
  input data_ready,
  input [31:0] data_receive,
  input [4:0] interest_channel,
  input cal_enable,
  input reset,
  output data_interval_ready
	);

localparam HIT_STORAGE_LENGTH=24;
real time_fifo[HIT_STORAGE_LENGTH-1:0];
integer write_point = 0;

always @(negedge hit_in or posedge reset) begin
  if (reset) begin
    write_point = 0;
  end else begin
    time_fifo[write_point] = $realtime;
    write_point = (write_point == HIT_STORAGE_LENGTH-1) ? 0: write_point + 1; 
  end
end

reg data_interval_ready = 1'b0;
always @(posedge clk) begin
  if (data_ready & data_receive[31:27]==interest_channel) begin
    data_interval_ready <= cal_enable;
  end else begin
    data_interval_ready <= 1'b0;
  end
end

real time_interval;
integer read_point = 0;
always @(posedge clk or posedge reset) begin
  if (reset) begin
    read_point = 0;
  end else if (data_ready & data_receive[31:27]==interest_channel) begin
      time_interval = $realtime - time_fifo[read_point] - `TIME_OFFSET;
      // $display("%f %f %f %f ",time_interval,$realtime,time_fifo[read_point],`TIME_OFFSET);
      read_point = (read_point == HIT_STORAGE_LENGTH-1) ? 0: read_point + 1;
  end
end





endmodule