/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_decoder.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-22 12:14:12
//  Note       : 
//     
module data_decoder(
	input clk_320,
	input [1:0] d_line,
  output  output_enable,
  output [39:0] data_out
	);

reg high_speed = 1'b1;
reg trigger = 1'b0;
reg [2:0] length = 3'd4;

reg reset = 1'b0;
wire [9:0] deserial_raw_data;
wire i_enable;
input_deserial input_deserial_inst (
    .clk(clk_320), 
    .rst(reset), 
    .data_even(d_line[0]), 
    .data_odd(d_line[1]), 
    .high_speed(high_speed),
    .raw_data_fifo_write(),
    .data(deserial_raw_data));

wire [7:0] deserial_data;
wire o_Kout,o_DErr,o_KErr,o_DpErr;
mDec8b10bMem_tb  inst_mDec8b10bMem_tb (
    .o8_Dout          (deserial_data),
    .o_Kout           (o_Kout),
    .o_DErr           (o_DErr),
    .o_KErr           (o_KErr),
    .o_DpErr          (o_DpErr),
    .i_ForceDisparity (1'b0),
    .i_Disparity      (1'b0),
    .i10_Din          (deserial_raw_data),
    .o_Rd             (),
    .i_Clk            (clk_320),
    .i_ARst_L         (~reset),
    .soft_reset_i     (1'b0),
    .i_enable         (i_enable)
  );
assign i_enable = input_deserial_inst.tick&(input_deserial_inst.data_shift_counter == input_deserial_inst.data_update_position);
initial begin
	#5 reset = 1'b1;
	#20 reset =1'b0;
end

tdc_data_interface_forsim tdc_data_interface_forsim_inst
  (
    .clk             (clk_320),
    .rst             (reset),
    .trigger         (trigger),
    .K_in_input      (o_Kout),
    .enable_input    (i_enable),
    .length          (length),
    .decode_data_input(deserial_data),
    .fifo_data_write (output_enable),
    .fifo_data       (data_out)
  );



endmodule