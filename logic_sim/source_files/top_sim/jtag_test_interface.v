/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : jtag_test_interface.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-21 18:33:10
//  Note       : 
//     
module jtag_test_interface(
    input clk,
    output TCK,
    output TMS,
    output TDI,
    input TDO,
    output reg TRST
    );

reg [511:0] JTAG_bits;
reg [8:0] bit_length;
reg [4:0] JTAG_inst;
reg start_jtag_configration = 1'b0;
wire [511:0] JTAG_data_out;
wire jtag_master_busy;

wire jtag_TCK, jtag_TMS, jtag_TDI, jtag_TDO;
JTAG_master
JTAG_master_inst
	(
    .clk(clk),
    .TCK(jtag_TCK),
    .TMS(jtag_TMS),
    .TDI(jtag_TDI),
    .TDO(jtag_TDO),
    .start_action(start_jtag_configration),
    .config_period(12'h001),
    .JTAG_bits(JTAG_bits),
    .bit_length(bit_length),
    .JTAG_inst(JTAG_inst),
    .JTAG_data_out(JTAG_data_out),
    .JTAG_busy(jtag_master_busy)
    );

reg start_spi_configration = 1'b0;
wire spi_TCK, spi_TMS, spi_TDI, spi_TDO;
reg [511:0] SPI_bits;
reg [8:0] SPI_bit_length;
wire [511:0] SPI_data_out;
wire SPI_busy;
SPI_master inst_SPI_master
	(
		.clk           (clk),
		.TCK           (spi_TCK),
		.TMS           (spi_TMS),
		.TDI           (spi_TDI),
		.TDO           (spi_TDO),
		.start_action  (start_spi_configration),
		.config_period (12'h001),
		.SPI_bits      (SPI_bits),
		.bit_length    (SPI_bit_length),
		.SPI_data_out  (SPI_data_out),
		.SPI_busy      (SPI_busy)
	);
assign TCK = TRST ? jtag_TCK : spi_TCK;
assign #2 TMS = TRST ? jtag_TMS : spi_TMS;
assign #2 TDI = TRST ? jtag_TDI : spi_TDI;
assign jtag_TDO =TDO ;
assign  spi_TDO= TDO;

parameter	idcode = 32'b10000100011100001101101011001110;
parameter	setup_0_reg = 115'b0000010000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000;                               
parameter	setup_1_reg = 94'b0000101000000100000000111111111111111111111111000000000000111110011100000000000000000000011111;
parameter	setup_2_reg = 36'b001100011001110010100000000111001100;
parameter   control_0_reg = 8'b00000000;
//parameter   control_1_reg = 27'b010000100001000000010010010;
parameter   control_1_reg = 47'b00000010000000010001001001000100100100010010010;
parameter   spi_reg = 300'h042ffffffffffffffffff0aaaa820140807fffff8007ce00000f98ce500e600010089224892;


reg clean = 1'b1;
wire [46:0] control1_curret;
assign  control1_curret= {testbench_inst.phase_clk160,
		testbench_inst.phase_clk320_0,testbench_inst.phase_clk320_1,testbench_inst.phase_clk320_2,
		testbench_inst.ePllResA,testbench_inst.ePllIcpA,testbench_inst.ePllCapA,
		testbench_inst.ePllResB,testbench_inst.ePllIcpB,testbench_inst.ePllCapB,
		testbench_inst.ePllResC,testbench_inst.ePllIcpC,testbench_inst.ePllCapC};
reg [46:0] control1_reg_last;
always @(posedge TCK) begin
	control1_reg_last <= control1_curret;
end
wire control1_update;
assign  control1_update = (control1_reg_last !=  control1_curret);
reg error_flag = 1'b0;
always @(posedge TCK or posedge start_jtag_configration) begin
	if (start_jtag_configration) begin
		// reset
		clean <= 1'b1;
		error_flag <= 1'b0;
	end	else if (control1_update) begin
		clean <= 1'b0;
		error_flag <= ~clean;
	end 
end
reg spi_clean = 1'b1;
reg spi_error_flag = 1'b0;
always @(posedge TCK or posedge start_spi_configration) begin
	if (start_spi_configration) begin
		// reset
		spi_clean <= 1'b1;
		spi_error_flag <= 1'b0;
	end	else if (control1_update) begin
		spi_clean <= 1'b0;
		spi_error_flag <= ~spi_clean;
	end 
end



task TRST_0;
begin
	@(negedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b0;
	testbench_inst.enable_320 = 1'b0;
	@(negedge jtag_TCK)
	@(negedge testbench_inst.clk160)
	TRST <= 1'b0;
	#50;
	@(posedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b1;
	testbench_inst.enable_320 = 1'b1;
end
endtask
task TRST_1;
begin
	@(negedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b0;
	testbench_inst.enable_320 = 1'b0;
	@(negedge jtag_TCK)
	@(negedge testbench_inst.clk160)
	TRST <= 1'b1;
	#50
	@(posedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b1;
	testbench_inst.enable_320 = 1'b1;
end
endtask

task spi_config(
	input [511:0] spi_content,
	input [8:0] spi_length
);begin
	@(negedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b0;
	testbench_inst.enable_320 = 1'b0;
	@(negedge clk)
	SPI_bits 	<= spi_content;
	SPI_bit_length 	<= spi_length;
	start_spi_configration <=1'b1;
	@(negedge clk)
	start_spi_configration <=1'b0;
	@(negedge SPI_busy);
	#25
	@(posedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b1;
	testbench_inst.enable_320 = 1'b1;
end
endtask

reg [299:0] spi_control1_target;
reg [279:0] spi_control1_target_temp;
task spi_ePLL_control_test;
	begin
		repeat (10) begin
			spi_control1_target_temp = {$random,$random,$random,$random,$random,$random,$random,$random,$random,$random};
			spi_control1_target= {spi_control1_target_temp,spi_control1_target_temp[9:0],spi_control1_target_temp[9:0]};
			spi_config(spi_control1_target,9'd299);
			@(posedge TCK) begin
				if ((spi_control1_target[46:0] != control1_curret)|(spi_error_flag)) begin
					$fdisplay(testbench_inst.logfile,"%t : SPI ePLL control test fail",$time);
					$display("%t : SPI ePLL control test fail",$time);
					$stop;
				end 
			end
			#50;
		end
		$fdisplay(testbench_inst.logfile,"%t : SPI ePLL control test pass",$time);
		$display("%t : SPI ePLL control test pass",$time);
	end
endtask

task spi_test;
begin
	$fdisplay(testbench_inst.logfile,"%t : Start spi interface simulation========================",$time);
	$display("%t : Start spi interface simulation========================",$time);
	spi_config('b0,9'd299);
	if (SPI_data_out[511:212] == spi_reg) begin
		$fdisplay(testbench_inst.logfile,"%t : SPI default check pass ",$time);
		$display("%t : SPI default check pass ",$time);
	end
	else begin
		$fdisplay(testbench_inst.logfile,"%t : SPI default check fail ",$time);
		$display("%t : SPI default check fail ",$time);
	end
	#50
	spi_config({512{1'b1}},9'd299);
	if (SPI_data_out[511:212] == 'b0) begin
		$fdisplay(testbench_inst.logfile,"%t : SPI all 0s check pass ",$time);
		$display("%t : SPI all 0s check pass ",$time);
	end
	else begin
		$fdisplay(testbench_inst.logfile,"%t : SPI all 0s check fail ",$time);
		$display("%t : SPI all 0s check fail ",$time);
	end
	#50
	spi_config(spi_reg,9'd299);
	if (SPI_data_out[511:212] == {300{1'b1}}) begin
		$fdisplay(testbench_inst.logfile,"%t : SPI all 1s check pass ",$time);
		$display("%t : SPI all 1s check pass ",$time);
	end
	else begin
		$fdisplay(testbench_inst.logfile,"%t : SPI all 1s check fail ",$time);
		$display("%t : SPI all 1s check fail ",$time);
	end
	#50
	spi_config(spi_reg,9'd299);
	if (SPI_data_out[511:212] ==spi_reg) begin
		$fdisplay(testbench_inst.logfile,"%t : SPI noramal operation check pass ",$time);
		$display("%t : SPI noramal operation check pass ",$time);
	end
	else begin
		$fdisplay(testbench_inst.logfile,"%t : SPI noramal operation check fail ",$time);
		$display("%t : SPI noramal operation check fail ",$time);
	end
	#50
	spi_ePLL_control_test;
	$fdisplay(testbench_inst.logfile,"%t : Finish spi interface simulation========================",$time);
	$display("%t : Finish spi interface simulation========================",$time);
end
endtask


task JTAG_config(
	input [511:0] JTAG_content,
	input [8:0] JTAG_length,
	input [4:0] JTAG_command
);begin
	@(negedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b0;
	testbench_inst.enable_320 = 1'b0;
	@(negedge clk)
	JTAG_bits 	<= JTAG_content;
	bit_length 	<= JTAG_length;
	JTAG_inst 	<= JTAG_command;
	start_jtag_configration <=1'b1;
	@(negedge clk)
	start_jtag_configration <=1'b0;
	@(negedge jtag_master_busy);
	#25;
	@(posedge testbench_inst.clk160) testbench_inst.enable_160 = 1'b1;
	testbench_inst.enable_320 = 1'b1;
end
endtask

task JTAG_setup_reg_test
(
	
	input [511:0] JTAG_content_default,
	input [8:0] JTAG_length,
	input [4:0] JTAG_command,
	input [31:0] setup_reg_index
);
	begin
		$fdisplay(testbench_inst.logfile,"%t : Start JTAG setup_%0d interface simulation",$time,setup_reg_index);
		$display("%t : Start JTAG setup_%0d interface simulation",$time,setup_reg_index);
		JTAG_config('b0,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0]== (JTAG_content_default<<(511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d default check pass ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d default check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d default check fail ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d default check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config({512{1'b1}},JTAG_length,JTAG_command);
		if (~|JTAG_data_out[511:0]) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d all 0s check pass ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d all 0s check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d all 0s check fail ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d all 0s check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config(JTAG_content_default,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0] == {512{1'b1}}<<((511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d all 1s check pass ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d all 1s check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d all 1s check fail ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d all 1s check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config(JTAG_content_default,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0]== (JTAG_content_default<<(511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d noramal operation check pass ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d noramal operation check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG setup_%0d noramal operation check fail ",$time,setup_reg_index);
			$display("%t : JTAG setup_%0d noramal operation check fail ",$time,setup_reg_index);
			$stop;
		end
		$fdisplay(testbench_inst.logfile,"%t : Finish JTAG setup_%0d interface simulation",$time,setup_reg_index);
		$display("%t : Finish JTAG setup_%0d interface simulation",$time,setup_reg_index);
	end
endtask

task JTAG_control_reg_test
(
	
	input [511:0] JTAG_content_default,
	input [8:0] JTAG_length,
	input [4:0] JTAG_command,
	input [31:0] setup_reg_index
);
	begin
		$fdisplay(testbench_inst.logfile,"%t : Start JTAG control_%0d interface simulation",$time,setup_reg_index);
		$display("%t : Start JTAG control_%0d interface simulation",$time,setup_reg_index);
		JTAG_config('b0,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0]== (JTAG_content_default<<(511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d default check pass ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d default check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d default check fail ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d default check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config({512{1'b1}},JTAG_length,JTAG_command);
		if (~|JTAG_data_out[511:0]) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d all 0s check pass ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d all 0s check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d all 0s check fail ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d all 0s check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config(JTAG_content_default,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0] == {512{1'b1}}<<((511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d all 1s check pass ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d all 1s check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d all 1s check fail ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d all 1s check fail ",$time,setup_reg_index);
			$stop;
		end
		JTAG_config(JTAG_content_default,JTAG_length,JTAG_command);
		if (JTAG_data_out[511:0]== (JTAG_content_default<<(511-JTAG_length))) begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d noramal operation check pass ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d noramal operation check pass ",$time,setup_reg_index);
		end else begin
			$fdisplay(testbench_inst.logfile,"%t : JTAG control_%0d noramal operation check fail ",$time,setup_reg_index);
			$display("%t : JTAG control_%0d noramal operation check fail ",$time,setup_reg_index);
			$stop;
		end
		$fdisplay(testbench_inst.logfile,"%t : Finish JTAG control_%0d interface simulation",$time,setup_reg_index);
		$display("%t : Finish JTAG control_%0d interface simulation",$time,setup_reg_index);
	end
endtask





reg [46:0] control1_target;
reg [26:0] control1_target_temp;
task JTAG_ePLL_control_test;
	begin
		repeat (10) begin
			control1_target_temp = $random;
			control1_target= {control1_target_temp,control1_target_temp[9:0],control1_target_temp[9:0]};
			JTAG_config(control1_target,9'd46,5'h06);
			@(posedge TCK) begin
				if ((control1_target != control1_curret)|(error_flag)) begin
					$fdisplay(testbench_inst.logfile,"%t : JTAG ePLL control test fail",$time);
					$display("%t : JTAG ePLL control test fail",$time);
					$stop;
				end 
			end
		end
		$fdisplay(testbench_inst.logfile,"%t : JTAG ePLL control test pass",$time);
		$display("%t : JTAG ePLL control test pass",$time);
	end
endtask




task JTAG_test();
begin
	$fdisplay(testbench_inst.logfile,"%t : Start JTAG interface simulation========================",$time);
	$display("%t : Start JTAG interface simulation========================",$time);

	$fdisplay(testbench_inst.logfile,"%t : Start JTAG ID_CODE interface simulation",$time);
	$display("%t : Start JTAG ID_CODE interface simulation",$time);
	JTAG_config('b0,9'd31,5'h11);
	if (JTAG_data_out[511:480]==idcode) begin
		$fdisplay(testbench_inst.logfile,"%t : JTAG ID_CODE check pass ",$time);
		$display("%t : JTAG ID_CODE check pass ",$time);
	end else begin
		$fdisplay(testbench_inst.logfile,"%t : JTAG ID_CODE check fail ",$time);
		$display("%t : JTAG ID_CODE check fail ",$time);
		$stop;
	end
	$fdisplay(testbench_inst.logfile,"%t : Finish JTAG ID_CODE interface simulation",$time);
	$display("%t : Finish JTAG ID_CODE interface simulation",$time);
	
	JTAG_setup_reg_test(setup_0_reg,9'd114,5'h12,0);
	JTAG_setup_reg_test(setup_1_reg,9'd93,5'h03,1);
	JTAG_setup_reg_test(setup_2_reg,9'd35,5'h14,2);
	JTAG_control_reg_test(control_0_reg,9'd7,5'h05,0);
	JTAG_control_reg_test(control_1_reg,9'd46,5'h06,1);
	JTAG_ePLL_control_test;
end
endtask

endmodule