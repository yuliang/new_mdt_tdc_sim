/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : latency_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on Feb 18th, 2019
//  Note       :  latency_sim
// 
//////////////////////////////////////////////////////////////////////////////////

`include "common_definition.v"

`timescale 1 ns / 1 fs
module latency_sim(
	input clk,
	input rst,
	input [23:0] hit_in,
	input data_ready,
  	input [31:0] data_receive
	);

integer fp[23:0];
wire [23:0] data_interval_ready;
reg [23:0] cal_enable = 24'b0;
reg [23:0] fifo_rst = 24'b0;
reg [31:0] hit_sum;
always @(posedge clk) begin
	if (rst) begin
		// reset
		hit_sum <= 32'b0;
	end
	else if (data_ready & |cal_enable) begin
		hit_sum <= hit_sum + 1'b1;	
	end
end


generate
	genvar i;
	for(i=0;i<24;i=i+1) 
		begin:channel_latency
			channel_latency_cal 
				channel_latency_cal_inst(									
				.clk                 (clk),
				.hit_in              (hit_in[i]),
				.data_ready          (data_ready),
				.data_receive        (data_receive),
				.interest_channel    (i),
				.cal_enable			 (cal_enable[i]),
				.reset				 (fifo_rst[i]),
				.data_interval_ready (data_interval_ready[i])
			);
			always @(posedge clk) begin
				if (data_interval_ready[i]) begin
					$fdisplay(fp[i],"%f",latency_sim_inst.channel_latency[i].channel_latency_cal_inst.time_interval);
					// $display("chl%d latency = %f,",i,latency_sim_inst.channel_latency[i].channel_latency_cal_inst.time_interval);						
				end
			end
		end	
endgenerate

task time_fifo_start();
begin
	fp[0] = $fopen("./latency_data/0.csv","w+");if(!fp[0])$display("cannot open file \"0.csv\"!");
	fp[1] = $fopen("./latency_data/1.csv","w+");if(!fp[1])$display("cannot open file \"1.csv\"!");
	fp[2] = $fopen("./latency_data/2.csv","w+");if(!fp[2])$display("cannot open file \"2.csv\"!");
	fp[3] = $fopen("./latency_data/3.csv","w+");if(!fp[3])$display("cannot open file \"3.csv\"!");
	fp[4] = $fopen("./latency_data/4.csv","w+");if(!fp[4])$display("cannot open file \"4.csv\"!");
	fp[5] = $fopen("./latency_data/5.csv","w+");if(!fp[5])$display("cannot open file \"5.csv\"!");
	fp[6] = $fopen("./latency_data/6.csv","w+");if(!fp[6])$display("cannot open file \"6.csv\"!");
	fp[7] = $fopen("./latency_data/7.csv","w+");if(!fp[7])$display("cannot open file \"7.csv\"!");
	fp[8] = $fopen("./latency_data/8.csv","w+");if(!fp[8])$display("cannot open file \"8.csv\"!");
	fp[9] = $fopen("./latency_data/9.csv","w+");if(!fp[9])$display("cannot open file \"9.csv\"!");
	fp[10] = $fopen("./latency_data/10.csv","w+");if(!fp[10])$display("cannot open file \"10.csv\"!");
	fp[11] = $fopen("./latency_data/11.csv","w+");if(!fp[11])$display("cannot open file \"11.csv\"!");
	fp[12] = $fopen("./latency_data/12.csv","w+");if(!fp[12])$display("cannot open file \"12.csv\"!");
	fp[13] = $fopen("./latency_data/13.csv","w+");if(!fp[13])$display("cannot open file \"13.csv\"!");
	fp[14] = $fopen("./latency_data/14.csv","w+");if(!fp[14])$display("cannot open file \"14.csv\"!");
	fp[15] = $fopen("./latency_data/15.csv","w+");if(!fp[15])$display("cannot open file \"15.csv\"!");
	fp[16] = $fopen("./latency_data/16.csv","w+");if(!fp[16])$display("cannot open file \"16.csv\"!");
	fp[17] = $fopen("./latency_data/17.csv","w+");if(!fp[17])$display("cannot open file \"17.csv\"!");
	fp[18] = $fopen("./latency_data/18.csv","w+");if(!fp[18])$display("cannot open file \"18.csv\"!");
	fp[19] = $fopen("./latency_data/19.csv","w+");if(!fp[19])$display("cannot open file \"19.csv\"!");
	fp[20] = $fopen("./latency_data/20.csv","w+");if(!fp[20])$display("cannot open file \"20.csv\"!");
	fp[21] = $fopen("./latency_data/21.csv","w+");if(!fp[21])$display("cannot open file \"21.csv\"!");
	fp[22] = $fopen("./latency_data/22.csv","w+");if(!fp[22])$display("cannot open file \"22.csv\"!");
	fp[23] = $fopen("./latency_data/23.csv","w+");if(!fp[23])$display("cannot open file \"23.csv\"!");
    fifo_rst = 24'b0;
    cal_enable = 24'b0;
    #10;
    fifo_rst = 24'HFFFFFF;
    #10;
    @(posedge clk)
    fifo_rst = 24'b0;
    cal_enable = 24'HFFFFFF;
end
endtask

integer j;
task time_fifo_stop();
begin
	for(j=0;j<24;j=j+1)
		$fclose(fp[j]);
	cal_enable = 24'b0;
end
endtask

endmodule