//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2017 01:46:18 PM
// Design Name: 
// Module Name: random_hit_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "common_definition.v"

module random_hit_generator(
    input clk,
    input rst,
    output hit_out,
    input enable_out,
    input [31:0] RATE,//32'h00417874->200k,32'h0083126E->400k
    input [31:0] seed1,
    input [31:0] seed2,
    input [7:0]dead_time_input
    );
    reg loaded=1'b0;
    reg load_seed=1'b0;
    wire[31:0] time_value;
    wire[31:0] time_width;
    reg hit=1'b0;
    reg [7:0] dead_time=8'b0;
    reg hit_inner=1'b0;
    assign #(23.5) hit_out=hit_inner;
 //   assign #(31) hit_out=hit_inner;

    always @(posedge clk)
    begin #(`ff_delay) hit_inner<=hit&enable_out;end
    
    reg [31:0] random_time;
    always@(posedge clk)  begin
          random_time <= $random;
    end
    assign time_value=random_time;
        
        
    reg width_enable=1'b0;
      
    reg [31:0] random_width=32'b0;
    always@(posedge clk)  begin
      if(width_enable) random_width <= $random;
    end
    assign time_width=random_width;     
            
            
    always @(posedge clk)begin
        if(rst)     
            loaded<=1'b0; 
        else if(~loaded) 
            loaded<=1'b1;
    end
    
    always @(posedge clk)begin
        if(rst)     
            load_seed<=1'b0; 
        else if(~loaded) 
            load_seed<=1'b1;
        else load_seed<=1'b0;
    end        
    
    reg [4:0] width_counter=5'h0a;
    


    always @(posedge clk)begin
        if(rst)
            hit<=1'b0;
        else if((time_value<RATE)&(~(|dead_time)))
            hit<=1'b1;
        else if(~(|width_counter))
            hit<=1'b0;
   end
   
    always @(posedge clk)begin
       if(rst)
           width_enable<=1'b0;
       else if(time_value<RATE)
           width_enable<=1'b1;
       else begin
        width_enable<=1'b0;end
  end     
  
   always @(posedge clk)begin
     if(rst)
         width_counter=5'h0a;
     else if((time_value<RATE)&(hit==1'b0))
         width_counter<=time_width[4:0]|5'b01000;
     else begin
        width_counter<=(|width_counter)?(width_counter-5'b1):width_counter;end
end          
     
     always@(posedge clk)begin
        if(rst)dead_time<=8'b0;
        else if((time_value<RATE)&(hit==1'b0))
            dead_time <= dead_time_input;
        else begin
            dead_time <= (|dead_time)?dead_time-8'b1:dead_time; end 
     end 
endmodule
