/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TDC_readout_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on 2019-02-12 18:04:15
//  Note       : 
// 
`timescale 1 ns / 1 fs
module readout_fifo_occupancy(
	input clk,
	input rst
);

//reg [15:0] fifo_inst = 16'b0; //represent the real 16 bit fifo, full = 16'h8000, empty = 16'b0, occu 5 = 16'b0000000000010000;
reg [4:0] fifo_inst = 5'b0;
reg [31:0] fifo_occupancy[16:0];
reg [35:0] hit_sum = 0;
reg [4:0] i;
wire fifo_write;
wire fifo_read;
assign fifo_write = tdc_dut_inst.tdc_logic_inst.tdc_core_logic_inst.tdc_readout_logic_inst.readout_fifo_inst.winc;
assign fifo_read = tdc_dut_inst.tdc_logic_inst.tdc_core_logic_inst.tdc_readout_logic_inst.readout_fifo_inst.rinc;
always @(posedge clk) begin
	if (rst) begin
		// reset
		fifo_inst <= 5'b0;		
	end	else if (fifo_write & (~fifo_read)) begin  
		fifo_inst <= fifo_inst + 5'b1;
		if (fifo_inst[4]) begin
			$display("%t Warning: readout fifo full!",$time);
			$stop;			
		end			
	end	else if ((~fifo_write) & fifo_read) begin 
		fifo_inst <= fifo_inst - 4'b1;
	end 
end
reg fifo_occupancy_clear = 1'b0;
reg fifo_occupancy_enable = 1'b0;
always @(posedge clk) begin
	if (rst|fifo_occupancy_clear) begin
		// reset
		// fifo_occupancy[0] <= 32'b0; fifo_occupancy[1] <= 32'b0; fifo_occupancy[2] <= 32'b0; fifo_occupancy[3] <= 32'b0;
		// fifo_occupancy[4] <= 32'b0; fifo_occupancy[5] <= 32'b0; fifo_occupancy[6] <= 32'b0; fifo_occupancy[7] <= 32'b0;
		// fifo_occupancy[8] <= 32'b0; fifo_occupancy[9] <= 32'b0; fifo_occupancy[10] <= 32'b0;fifo_occupancy[11] <= 32'b0;
		// fifo_occupancy[12] <= 32'b0;fifo_occupancy[13] <= 32'b0;fifo_occupancy[14] <= 32'b0;fifo_occupancy[15] <= 32'b0;
		// fifo_occupancy[16] <= 32'b0;
		for (i=0;i<24;i=i+1)
			fifo_occupancy[i] <= 32'b0;
		hit_sum <= 1'b0;
	end	else if (fifo_write&fifo_occupancy_enable) begin
		fifo_occupancy[fifo_inst] <= fifo_occupancy[fifo_inst] + 1'b1;
		hit_sum <= hit_sum +1'b1;
	end
end

task readout_fifo_occupancy_record_restart();
	begin 
		@(posedge clk)
		fifo_occupancy_clear <= 1'b1;
		@(posedge clk)
		fifo_occupancy_clear <= 1'b0;
		fifo_occupancy_enable <= 1'b1;
	end
endtask

task readout_fifo_occupancy_record_start();
	begin 
		@(posedge clk)
		fifo_occupancy_enable <= 1'b1;
	end
endtask

task readout_fifo_occupancy_record_stop();
	begin 
		@(posedge clk)
		fifo_occupancy_enable <= 1'b0;
		$display("%t Info: readout fifo occupance list below, sum = %d",$time, hit_sum);
		$display("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			fifo_occupancy[0],fifo_occupancy[1],fifo_occupancy[2],fifo_occupancy[3],
			fifo_occupancy[4],fifo_occupancy[5],fifo_occupancy[6],fifo_occupancy[7], 
			fifo_occupancy[8],fifo_occupancy[9],fifo_occupancy[10],fifo_occupancy[11],
			fifo_occupancy[12],fifo_occupancy[13],fifo_occupancy[14],fifo_occupancy[15],
			fifo_occupancy[16]);
		$fdisplay(testbench_inst.logfile,"%t Info: readout fifo occupance list below, sum = %d",$time, hit_sum);
		$fdisplay(testbench_inst.logfile,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			fifo_occupancy[0],fifo_occupancy[1],fifo_occupancy[2],fifo_occupancy[3],
			fifo_occupancy[4],fifo_occupancy[5],fifo_occupancy[6],fifo_occupancy[7], 
			fifo_occupancy[8],fifo_occupancy[9],fifo_occupancy[10],fifo_occupancy[11],
			fifo_occupancy[12],fifo_occupancy[13],fifo_occupancy[14],fifo_occupancy[15],
			fifo_occupancy[16]);

	end

endtask
endmodule







