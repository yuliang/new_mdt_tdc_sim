/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TDC_readout_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on 2019-02-12 18:04:15
//  Note       : 
// 
`timescale 1 ns / 1 fs
module readout_fifo_occupancy_hit(
	input clk,
	input rst
);
localparam FIFO_DEPTH = 30;

//reg [15:0] fifo_inst = 16'b0; //represent the real 16 bit fifo, full = 16'h8000, empty = 16'b0, occu 5 = 16'b0000000000010000;
reg [7:0] fifo_inst = 8'b0;
reg [7:0] fifo_inst_last = 8'b0;
reg [31:0] fifo_occupancy[FIFO_DEPTH-1:0];
reg [35:0] hit_sum = 0;
wire [23:0] hit_in;
reg [23:0] hit_arrive_reg;
reg [7:0] i;
reg fifo_write_reg;


generate
	genvar j;
	for(j=0;j<24;j=j+1)
		assign hit_in[j] = tdc_dut_inst.tdc_front_end[j].tdc_front_inst.t_Rdy_chnl;
endgenerate
// assign hit_in = tdc_dut_inst.Rdy_f;

wire fifo_write;
wire fifo_read;
assign fifo_write = tdc_dut_inst.tdc_logic_inst.tdc_core_logic_inst.tdc_readout_logic_inst.readout_fifo_inst.winc;
assign fifo_read = tdc_dut_inst.tdc_logic_inst.tdc_core_logic_inst.tdc_readout_logic_inst.readout_fifo_inst.rinc;
// always @(posedge clk) begin
// 	if (rst) begin
// 		// reset
// 		fifo_inst <= 5'b0;		
// 	end	else if (fifo_write & (~fifo_read)) begin  
// 		fifo_inst <= fifo_inst + 5'b1;
// 		if (fifo_inst[4]) begin
// 			$display("%t Warning: readout fifo full!",$time);
// 			$stop;			
// 		end			
// 	end	else if ((~fifo_write) & fifo_read) begin 
// 		fifo_inst <= fifo_inst - 4'b1;
// 	end 
// end
reg [23:0] hit_ready_0;
reg [23:0] hit_ready_1;

wire [23:0] hit_arrive;
always @(posedge clk) begin
	if (rst) begin
		// reset
		hit_ready_1 <= 24'b0;
		hit_ready_0 <= 24'b0;
		
	end
	else begin
		hit_ready_1 <= hit_ready_0;
		hit_ready_0 <= hit_in;	
	end
end
assign hit_arrive = hit_ready_0 & (~hit_ready_1);
always @(posedge clk) begin
	if (rst) begin
		// reset
		hit_arrive_reg <= 24'b0;
		fifo_write_reg <= 1'b0;		
	end
	else begin
		hit_arrive_reg <= hit_arrive;
		fifo_write_reg <= fifo_write;
	end
end
always @(posedge clk) begin
	if (rst) begin
		// reset
		fifo_inst <= 8'b0;
		fifo_inst_last <= 8'b0;	
	
	end	else if (|hit_arrive) begin
		fifo_inst_last <= fifo_inst;  
		fifo_inst <= fifo_inst 	+ hit_arrive[23] + hit_arrive[22] + hit_arrive[21] + hit_arrive[20]
								+ hit_arrive[19] + hit_arrive[18] + hit_arrive[17] + hit_arrive[16]
								+ hit_arrive[15] + hit_arrive[14] + hit_arrive[13] + hit_arrive[12]
								+ hit_arrive[11] + hit_arrive[10] + hit_arrive[9] + hit_arrive[8]
								+ hit_arrive[7] + hit_arrive[6] + hit_arrive[5] + hit_arrive[4]
								+ hit_arrive[3] + hit_arrive[2] + hit_arrive[1] + hit_arrive[0]
								- fifo_read ;		
	end	else if (fifo_read) begin 
		fifo_inst_last <= fifo_inst;
		fifo_inst <= fifo_inst - 1;
	end 
end

reg fifo_read_last;
always @(posedge clk) begin
	fifo_read_last <= fifo_read;
end

reg fifo_occupancy_clear = 1'b0;
reg fifo_occupancy_enable = 1'b0;
always @(posedge clk) begin
	if (rst|fifo_occupancy_clear) begin
		// reset
		for(i=0;i<FIFO_DEPTH;i=i+1) begin
			fifo_occupancy[i] <= 32'b0;
		end
		hit_sum <= 1'b0;
	end	else if (|hit_arrive_reg & fifo_occupancy_enable) begin
		for (i=0;i<FIFO_DEPTH;i=i+1) begin
			if((i >= fifo_inst_last) & (i < (fifo_inst+fifo_read_last))) begin
				fifo_occupancy[i] <= fifo_occupancy[i] + 1'b1;
			end
		end
		hit_sum <= hit_sum 	+ hit_arrive_reg[23] + hit_arrive_reg[22] + hit_arrive_reg[21] + hit_arrive_reg[20]
							+ hit_arrive_reg[19] + hit_arrive_reg[18] + hit_arrive_reg[17] + hit_arrive_reg[16]
							+ hit_arrive_reg[15] + hit_arrive_reg[14] + hit_arrive_reg[13] + hit_arrive_reg[12]
							+ hit_arrive_reg[11] + hit_arrive_reg[10] + hit_arrive_reg[9] + hit_arrive_reg[8]
							+ hit_arrive_reg[7] + hit_arrive_reg[6] + hit_arrive_reg[5] + hit_arrive_reg[4]
							+ hit_arrive_reg[3] + hit_arrive_reg[2] + hit_arrive_reg[1] + hit_arrive_reg[0];
	end
end

task readout_fifo_occupancy_record_restart();
	begin 
		@(posedge clk)
		fifo_occupancy_clear <= 1'b1;
		@(posedge clk)
		fifo_occupancy_clear <= 1'b0;
		fifo_occupancy_enable <= 1'b1;
	end
endtask

task readout_fifo_occupancy_record_start();
	begin 
		@(posedge clk)
		fifo_occupancy_enable <= 1'b1;
	end
endtask

task readout_fifo_occupancy_record_stop();
	begin 
		@(posedge clk)
		fifo_occupancy_enable <= 1'b0;
		$display("%t Info: total fifo occupance list below, sum = %d",$time, hit_sum);
		$display("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			fifo_occupancy[0],fifo_occupancy[1],fifo_occupancy[2],fifo_occupancy[3],
			fifo_occupancy[4],fifo_occupancy[5],fifo_occupancy[6],fifo_occupancy[7], 
			fifo_occupancy[8],fifo_occupancy[9],fifo_occupancy[10],fifo_occupancy[11],
			fifo_occupancy[12],fifo_occupancy[13],fifo_occupancy[14],fifo_occupancy[15],
			fifo_occupancy[16],fifo_occupancy[17],fifo_occupancy[18],fifo_occupancy[19]);
		$fdisplay(testbench_inst.logfile,"%t Info: total fifo occupance list below, sum = %d",$time, hit_sum);
		$fdisplay(testbench_inst.logfile,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
			fifo_occupancy[0],fifo_occupancy[1],fifo_occupancy[2],fifo_occupancy[3],
			fifo_occupancy[4],fifo_occupancy[5],fifo_occupancy[6],fifo_occupancy[7], 
			fifo_occupancy[8],fifo_occupancy[9],fifo_occupancy[10],fifo_occupancy[11],
			fifo_occupancy[12],fifo_occupancy[13],fifo_occupancy[14],fifo_occupancy[15],
			fifo_occupancy[16],fifo_occupancy[17],fifo_occupancy[18],fifo_occupancy[19]);

	end

endtask
endmodule







