/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : sim_clock_gen.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 12th, 2018
//  Note       :  generation clock for simulation

`timescale 1 ns / 1 fs
module sim_clock_gen
#(parameter
	clk_40_delay=0,
	clk_320_delay=0,
	clk_160_delay=0
)
(
	output clk_40,
	output clk_160,
	output clk_320,
	output clk_320M_4,
	output clk_hit
);

reg enable_40 = 1'b1;
reg enable_160= 1'b1;
reg enable_320= 1'b1;
reg enable_320M_4= 1'b1;
reg enable_hit= 1'b1;

reg clk40_temp;
	always begin
	    clk40_temp=1'b1;
	    #12.5;
	    clk40_temp=1'b0;
	    #12.5;
	end

	reg clk320_4_temp;
	always begin
	    clk320_4_temp=1'b1;
	    #0.390625;
	    clk320_4_temp=1'b0;
	    #0.390625;
	end
	
	reg clk320_temp;
	always begin
	  clk320_temp =1'b1;
	  #1.5625;
	  clk320_temp =1'b0;
	  #1.5625;
	end
	
	reg clk160_tmp;
	always begin
	  clk160_tmp =1'b0;
	  #3.125;
	  clk160_tmp =1'b1;
	  #3.125;
	end

	reg clk_hit_reg=1'b0;
	assign #1.3 clk_hit = enable_hit & clk_hit_reg;
	always begin    
    	clk_hit_reg = 1'b1;
    	#2.49995;
    	// #1.5625;
    	clk_hit_reg = 1'b0;
    	#2.49995;
    	// #1.5625;
	end
	assign #(clk_40_delay) clk_40 = enable_40 & clk40_temp;
	assign #(clk_320_delay) clk_320M_4 = enable_320M_4 & clk320_4_temp;
	assign #(clk_320_delay) clk_320 = enable_320 & clk320_temp;
	assign #(clk_160_delay) clk_160 =enable_160 & clk160_tmp;

endmodule