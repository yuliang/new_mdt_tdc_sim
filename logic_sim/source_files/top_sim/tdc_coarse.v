/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_coarse.v
//  Author     : Jinhong Wang,Yu Liang
//  Revision   : 
//               First created on April 27th, 2016 Modified on Feb 13th 2018
//  Note       : What actually connected to clk320 is clk320_inv!!!
//               V1.0, @20160519 remove clk40 input
//                       

`timescale 1 ps / 1 fs
module tdc_coarse
 (output [14 :0] coarse_time, coarse_time_inv,
  input  clk320, 
  input  hit,
  input  LHC_BC_rst
);

wire [14 :0] coarse_time_0, coarse_time_1;
reg  [14 :0] coarse_time_0_r, coarse_time_1_r;
reg         LHC_BCR_s;
reg         LHC_BCR_r;

assign coarse_time      = coarse_time_0_r;
assign coarse_time_inv  = coarse_time_1_r;

always @(posedge clk320)
      LHC_BCR_s <= LHC_BC_rst;     
always @(negedge clk320)
      LHC_BCR_r <= LHC_BCR_s;
      
always @(posedge hit) begin
      coarse_time_0_r <= coarse_time_0;
      coarse_time_1_r <= coarse_time_1;
end

//instantiation of coarse counter 0
coarse_cnt  coarse_cnt_clk320     (.coarse_time(coarse_time_0), .clk0(~clk320),
                                   .LHC_BC_rst(LHC_BCR_r));

coarse_cnt  coarse_cnt_clk320_inv (.coarse_time(coarse_time_1), .clk0(clk320),
                                   .LHC_BC_rst(LHC_BCR_r));


endmodule
