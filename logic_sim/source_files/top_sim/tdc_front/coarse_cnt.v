`timescale 1 ps / 1 ps


/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : coarse_cnt.v
//  Author     : Jinhong Wang,Yu Liang
//  Revision   : 
//               First created on April 27th, 2016
//               V1.0, @20160519, not necessarily to sample BCR with l_clk40


module coarse_cnt
 ( output [14 :0] coarse_time, // 15 bit coarse time
   input  clk0,
   input  LHC_BC_rst
);

wire l_load_w;
reg  [14:0]  cnt_r;

reg  LHC_BCR_r;
reg  [1:0] l_BCR_r;

//reg  [14:0] coarse_time_r;

//always @(posedge clk40)
//  LHC_BCR_r <= LHC_BC_rst;

always @(posedge clk0) 
   l_BCR_r <= {l_BCR_r[0], LHC_BC_rst};

assign l_load_w = (~l_BCR_r[1]) &  l_BCR_r[0];


always @(posedge clk0) begin
  if(l_load_w==1'b1)
    cnt_r <= 15'b0;
  else
    cnt_r <= cnt_r + 15'b1;
end



assign coarse_time = cnt_r;


endmodule

