/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_front.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-21 10:20:30
//  Note       : 
//     

`timescale 1 ns / 1 fs
module tdc_front(
input hit,
input clk320,
input bcr,
output  Rdy_chnl, t_Rdy_chnl,
output  [3:0] q_chnl, t_q_chnl,
output  [14:0] cnt_chnl, t_cnt_chnl,
output  [14:0] cnt_inv_chnl, t_cnt_inv_chnl);

	wire clk320_90;  assign #0.78125 clk320_90 = clk320;
	wire clk320_180; assign #0.78125 clk320_180 = clk320_90;
	wire clk320_270; assign #0.78125 clk320_270 = clk320_180;
	tdc_coarse 
		channel_tdc_coarse(
			.coarse_time     (cnt_chnl),
			.coarse_time_inv (cnt_inv_chnl),
			.clk320          (clk320_180),
			.hit             (hit),
			.LHC_BC_rst      (bcr)
		),
		channel_tdc_coarse_t(
			.coarse_time     (t_cnt_chnl),
			.coarse_time_inv (t_cnt_inv_chnl),
			.clk320          (clk320_180),
			.hit             (~hit),
			.LHC_BC_rst      (bcr)
		);
	
	reg [3:0] q_chnl_r = 4'b0, t_q_chnl_r = 4'b0;
	assign  q_chnl = q_chnl_r, t_q_chnl = t_q_chnl_r;
	always@(posedge hit)begin
	    q_chnl_r <= {clk320_270,clk320_180,clk320_90,clk320};
	end
	always@(negedge hit)begin
	    t_q_chnl_r <= {clk320_270,clk320_180,clk320_90,clk320};
	end	
	
	reg [1:0] hit_r;
	wire      pos_hit, trailing_hit;
	reg [4:0] pos_hit_r, trailing_hit_r;
	always @(posedge clk320) begin
	   hit_r <= {hit_r[0], hit};
	end
	assign pos_hit      = hit_r[0] & (~hit_r[1]);
	assign trailing_hit = (~hit_r[0]) & hit_r[1];
	always @(posedge clk320) begin
	   pos_hit_r <= {pos_hit_r[3:0], pos_hit};
	   trailing_hit_r <={trailing_hit_r[3:0], trailing_hit};
	end
	assign #1 Rdy_chnl = |pos_hit_r[4:0];
	assign #1 t_Rdy_chnl = |trailing_hit_r[4:0];

endmodule