/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : test_top.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-21 10:04:54
//  Note       : 
//     
`timescale 1 ns / 1 fs
module test_top();

wire clk40, clk160, clk320;
wire tck, tms, tdi, trst, tdo;
wire ASD_TCK, ASD_shift_out, ASD_update_out, ASD_data_out, ASD_data_in;
wire [23:0] hit;
wire encoded_control, reset, bunch_reset_direct;
wire [1:0] d_line;

wire  [4  :0] phase_clk160;
wire  [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2;
wire  rst_ePLL;
wire  [3  :0] ePllResA, ePllResB, ePllResC;
wire  [3  :0] ePllIcpA, ePllIcpB, ePllIcpC;
wire  [1  :0] ePllCapA, ePllCapB, ePllCapC;
wire ePll_lock;  

tdc_dut 
	tdc_dut_inst(
	.clk40(clk40),
	.clk160(clk160),
	.clk320(clk320),

	.tck(tck),
	.tms(tms),
	.tdi(tdi),
	.trst(trst),
	.tdo(tdo),
	
	.ASD_TCK(ASD_TCK),
	.ASD_shift_out(ASD_shift_out),
	.ASD_update_out(ASD_update_out),
	.ASD_data_out(ASD_data_out),
	.ASD_data_in(ASD_data_in),
	
	.hit(hit),
	
	.encoded_control_in(encoded_control),
	.reset_in(reset),
	.bunch_reset_direct_in(bunch_reset_direct),
	
	.d_line(d_line),
	
	.phase_clk160(phase_clk160),
	.phase_clk320_0(phase_clk320_0), .phase_clk320_1(phase_clk320_1), .phase_clk320_2(phase_clk320_2),
	.rst_ePLL(rst_ePLL),
	.ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
	.ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
	.ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),
	.ePll_lock(ePll_lock)  
);
//initial begin
//$sdf_annotate("../pnr/output/dfm.sdf",tdc_dut_inst.tdc_logic_inst,,"tdc_logic_sdf.log");
//end

testbench
	testbench_inst(
	.clk40_output(clk40),
	.clk160_output(clk160),
	.clk320_output(clk320),

	.tck(tck),
	.tms(tms),
	.tdi(tdi),
	.trst(trst),
	.tdo(tdo),

	.hit(hit),

	.encoded_control(encoded_control),
	.reset(reset),
	.bunch_reset_direct(bunch_reset_direct),

	.bcr(tdc_dut_inst.coarse_bcr_distribution),

	.d_line(d_line),
	
	.phase_clk160(phase_clk160),
	.phase_clk320_0(phase_clk320_0), .phase_clk320_1(phase_clk320_1), .phase_clk320_2(phase_clk320_2),
	.rst_ePLL(rst_ePLL),
	.ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
	.ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
	.ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),
	.ePll_lock(ePll_lock)  
);
endmodule