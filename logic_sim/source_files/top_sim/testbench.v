/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : testbench.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-18 17:48:41
//  Note       : 
//     
`timescale 1 ns / 1 fs
`define SIM_TIME #1000000
module testbench(
	output clk40_output,
	output clk160_output,
	output clk320_output,

	output trst,
	output tck,
	output tms,
	output tdi,
	input  tdo,

	input ASD_TCK,
	input ASD_shift_out,
	input ASD_update_out,
	input ASD_data_out,
	output  ASD_data_in,

	output [23:0] hit,

	output encoded_control,
	output reg reset,
	output bunch_reset_direct,

	input bcr,

	input [1:0] d_line,   
    
    input [4  :0] phase_clk160,
    input [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2,
    input         rst_ePLL,    
    input [3  :0] ePllResA, ePllResB, ePllResC,
    input [3  :0] ePllIcpA, ePllIcpB, ePllIcpC,
    input [1  :0] ePllCapA, ePllCapB, ePllCapC,    
    output        ePll_lock
);
wire clk_320M_4;
wire clk_hit;
wire clk40,clk160,clk320;
sim_clock_gen #(.clk_40_delay(0),.clk_320_delay(0),.clk_160_delay(0))
sim_clock_gen_inst(
	.clk_40(clk40),
	.clk_160(clk160),
	.clk_320(clk320),
	.clk_320M_4(clk_320M_4),
	.clk_hit(clk_hit)
);

reg enable_40 = 1'b1;assign clk40_output = enable_40 & clk40;
reg enable_160= 1'b1;assign clk160_output = enable_160 & clk160;
reg enable_320= 1'b1;assign clk320_output = enable_320 & clk320;


jtag_test_interface
jtag_test_interface_inst
	(
    .clk(clk160),
    .TCK(tck),
    .TMS(tms),
    .TDI(tdi),
    .TDO(tdo),
    .TRST(trst)
    );


always @(posedge tck) begin
	if((ePllResA != ePllResB)|
	   (ePllResA != ePllResC)|
	   (ePllIcpA != ePllIcpB)|
	   (ePllIcpA != ePllIcpC)|
	   (ePllCapA != ePllCapB)|
	   (ePllCapA != ePllCapC))begin
		$display("%t : ePLL control ABC not equal ",$time);
		$stop;
	end
end

wire  output_enable;
wire [39:0] data_out;
data_decoder 
	data_decoder_inst (
		.clk_320(clk320), 
		.d_line(d_line),
		.output_enable(output_enable),
		.data_out(data_out)
	);

latency_sim latency_sim_inst
	(
		.clk          (clk320),
		.rst 		  (reset),
		.hit_in       (hit),
		.data_ready   (output_enable),
		.data_receive (data_out[31:0])
	);


wire [23:0] hit_triggerless;
triggless_mode_sim 
	triggless_mode_sim_inst (
		.clk160(clk160), 
		.clk320(clk320), 
		.hit(hit_triggerless), 
		.d_line(d_line), 
		.bcr(bcr),
		.roll_over(12'hfff)
	);

ttc_generator_sim
	ttc_generator_sim_inst(
		.clk320(clk320),
		.clk160(clk160),
		.clk40(clk40),
		.encoded_control(encoded_control),
		.bunch_reset_direct(bunch_reset_direct),
		.bcr(bcr)
	);

trigger_mode_sim
	trigger_mode_sim_inst(.clk160(clk160));
integer logfile;

wire [23:0] hit_random;
TDC_readout_sim
	TDC_readout_sim_inst(
		.clk_hit(clk_hit),
		.reset(reset),
		.hit(hit_random)
	);
assign hit = hit_random | hit_triggerless;

readout_fifo_occupancy 
    readout_fifo_occupancy_inst(
	.clk(clk160),
	.rst(reset)
);

readout_fifo_occupancy_hit 
    readout_fifo_occupancy_hit_inst(
	.clk(clk160),
	.rst(reset)
);



    
integer indicator;

initial begin
	//shall print %t with scaled in ns (-9), with 2 precision digits, and would print the " ns" string
  	$timeformat(-9, 4, " ns", 20);
  	logfile = $fopen("logfile.log" , "w" );
  	reset = 1'b0;
  	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200;
  	#200
  	@(negedge clk160)
  	reset = 1'b1;
  	#200
  	@(negedge clk160)
  	reset = 1'b0;
    #200;
    latency_sim_inst.time_fifo_start();
    TDC_readout_sim_inst.hit_random_enable = 24'hffffff;
	TDC_readout_sim_inst.random_hit_out_enable();
    

    // $display("%t Info: Start fifo occpancy simulation",$realtime);      
   //  readout_fifo_occupancy_inst.readout_fifo_occupancy_record_restart();
   //  readout_fifo_occupancy_hit_inst.readout_fifo_occupancy_record_restart();
   //  TDC_readout_sim_inst.random_hit_out_enable();
    
     indicator=0;
    repeat (100) begin
    		`SIM_TIME;
    		indicator = indicator + 1;
    		$display("%t Info: %d%% finished",$time,indicator);
    end
    TDC_readout_sim_inst.random_hit_out_disable();
    latency_sim_inst.time_fifo_stop();
    $display("%t total hit = %d",$time,latency_sim_inst.hit_sum);
   
   // TDC_readout_sim_inst.random_hit_out_disable();
   // readout_fifo_occupancy_inst.readout_fifo_occupancy_record_stop();
   // readout_fifo_occupancy_hit_inst.readout_fifo_occupancy_record_stop();
   // $display("%t Info: Stop fifo occpancy simulation",$time);
   
   
   
////    jtag_test_interface_inst.spi_test;
////	#200;
////  	jtag_test_interface_inst.TRST_1;
////  	jtag_test_interface_inst.TRST_0;
////  	#200
////  	jtag_test_interface_inst.TRST_1;
////  	#200
////  	@(negedge clk160)
////  	reset = 1'b1;
////  	#200
////  	@(negedge clk160)
////  	reset = 1'b0;
////    #200;
////	jtag_test_interface_inst.JTAG_test;
////	#200	
////	triggless_mode_sim_inst.fine_time_decode_test;
////	triggless_mode_sim_inst.single_hit_pair_test;	
////	triggless_mode_sim_inst.multi_hit_pair_test;
////	triggless_mode_sim_inst.single_hit_leading_test;
////	triggless_mode_sim_inst.multi_hit_leading_test;
////	ttc_generator_sim_inst.direct_bcr_test;
////	ttc_generator_sim_inst.encode_bcr_test;
////	trigger_mode_sim_inst.bunch_id_test;
////	trigger_mode_sim_inst.trigger_matching_test;
//	$fclose(logfile);
//	#5000
	
	 $stop;
end
endmodule