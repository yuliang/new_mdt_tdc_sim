/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : triggless_mode_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-22 09:41:15
//  Note       : 
//     
`timescale 1 ns / 1 fs
module triggless_mode_sim(
input clk160,
input clk320,
output reg [23:0] hit,
input [1:0] d_line,
input bcr,
input [11:0] roll_over
);

reg [1:0] fine_ref;
reg [3:0] q_test;
wire clk320_90;
assign #0.78125 clk320_90 = clk320;
wire clk320_M4;
assign  clk320_M4 = clk320 ^ clk320_90;
reg [23:0] edge_data;
reg enable_hit_pair_analysis = 1'b0;
reg enable_hit_edge_analysis = 1'b0;
tdc_coarse 
coarse_ref(
	.coarse_time(), 
	.coarse_time_inv(),
  	.clk320(~clk320), 
  	.hit(),
  	.LHC_BC_rst(bcr)
);

initial begin
	hit = 24'b0;
end

reg [1:0] counter_fine = 2'b00;
reg [1:0] clk320_r;
wire syn;
assign syn = clk320_r[0] & ~clk320_r[1];

always @(posedge clk320_M4 or negedge clk320_M4) begin
	clk320_r <= {clk320_r[0],clk320};
end
always @(posedge clk320_M4 or negedge clk320_M4) begin
	counter_fine <= syn ? 2'b01 : (counter_fine + 2'b01);
end

always begin
	single_hit_pair_analysis;
end

always begin
    single_hit_edge_analysis;
end

reg [23:0] hit_inner = 24'b0;
reg [16:0] leading_edge_reference,trailing_edge_reference;
reg [16:0] width_reference;
task single_hit_gen(
	input [31:0] delay,
	input [4:0] channel_ID,
	input [31:0] width
	);begin
		@(posedge clk320);
		hit_inner = 24'b1 <<channel_ID;
		#(delay);
		#0.1
		hit = hit_inner;
		leading_edge_reference = {coarse_ref.coarse_time_0,counter_fine};		
		#(width);
		hit = 'b0;		
		hit_inner = 'b0;
		trailing_edge_reference = {coarse_ref.coarse_time_0,counter_fine};	
		width_reference  = (trailing_edge_reference >= leading_edge_reference) ? (trailing_edge_reference - leading_edge_reference) : (trailing_edge_reference - leading_edge_reference + {roll_over,5'b11111} + 17'b1);
	end
endtask

task multi_hit_gen(
	input [31:0] delay,
	input [23:0] hit_mask,
	input [31:0] width
	);begin
		@(posedge clk320);
		hit_inner = 24'hffffff & hit_mask;
		#(delay);
		#0.1
		hit = hit_inner;
		leading_edge_reference = {coarse_ref.coarse_time_0,counter_fine};		
		#(width);
		hit = 'b0;		
		hit_inner = 'b0;
		trailing_edge_reference = {coarse_ref.coarse_time_0,counter_fine};	
		width_reference  = (trailing_edge_reference >= leading_edge_reference) ? (trailing_edge_reference - leading_edge_reference) : (trailing_edge_reference - leading_edge_reference + {roll_over,5'b11111} + 17'b1);
	end
endtask

task fine_time_decode_check();
begin
	@(posedge testbench_inst.output_enable);
	@(negedge clk320) edge_data = testbench_inst.data_out[23:0];
	case(q_test)
  		4'b0000 : fine_ref = 2'b00;
  		4'b0001 : fine_ref = 2'b01;
  		4'b0010 : fine_ref = 2'b10;
  		4'b0011 : fine_ref = 2'b01;
  		4'b0100 : fine_ref = 2'b11;
  		4'b0101 : fine_ref = 2'b00;
  		4'b0110 : fine_ref = 2'b10;
  		4'b0111 : fine_ref = 2'b10;
  		4'b1000 : fine_ref = 2'b00;
  		4'b1001 : fine_ref = 2'b00;
  		4'b1010 : fine_ref = 2'b00;
  		4'b1011 : fine_ref = 2'b01;
  		4'b1100 : fine_ref = 2'b11;
  		4'b1101 : fine_ref = 2'b00;
  		4'b1110 : fine_ref = 2'b11;
  		4'b1111 : fine_ref = 2'b00;
  	endcase
  	$fdisplay(testbench_inst.logfile,"%t Info: q is b%b fine time is b%b,coarse time output is 0x%h, coarse time is 0x%h, coarse time inv is 0x%h  ",$time,q_test,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_r[0],tdc_dut_inst.cnt_inv_r[0]);
	$display("%t Info: q is b%b fine time is b%b,coarse time output is 0x%h,ref coarse time is 0x%h, coarse time inv is 0x%h",$time,q_test,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_r[0],tdc_dut_inst.cnt_inv_r[0]);
	if(edge_data[1:0] == fine_ref)begin
		if((fine_ref == 2'b00) | (fine_ref == 2'b01))begin
			if(edge_data[16:2] == tdc_dut_inst.cnt_inv_r[0])begin end
			else begin
				$fdisplay(testbench_inst.logfile,"%t Error:  fine time is b%b,coarse time is 0x%h,ref coarse time is 0x%h ",$time,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_inv_r[0]);
				$display("%t Error: fine time is b%b,coarse time is 0x%h,ref coarse time is 0x%h ",$time,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_inv_r[0]);
			end
		end else begin
			if(edge_data[16:2] == tdc_dut_inst.cnt_r[0])begin end
			else begin
				$fdisplay(testbench_inst.logfile,"%t Error: fine time is b%b,coarse time is 0x%h,ref coarse time is 0x%h ",$time,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_r[0]);
				$display("%t Error: fine time is b%b,coarse time is 0x%h,ref coarse time is 0x%h ",$time,edge_data[1:0],edge_data[16:2],tdc_dut_inst.cnt_r[0]);
			end
		end
	end else begin
		$fdisplay(testbench_inst.logfile,"%t Error: fine time is b%b, q is b%b",$time,edge_data[1:0],q_test);
		$display("%t Error: fine time is b%b, q is b%b",$time,edge_data[1:0],q_test);
	end
end
endtask

reg [31:0] pair_data;
task single_hit_pair_analysis();
	begin
		@(posedge testbench_inst.output_enable);
		if (enable_hit_pair_analysis) begin
			@(negedge clk320) pair_data <= testbench_inst.data_out[39:0];
			@(posedge clk320) 
			$fdisplay(testbench_inst.logfile,"%t Info: Channel %0d, leading edge 0x%h (coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,pair_data[31:27],pair_data[24:8],pair_data[24:10],pair_data[9:8],pair_data[7:0],pair_data[26:25]);
			$display("%t Info: Channel %0d, leading edge 0x%h (coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,pair_data[31:27],pair_data[24:8],pair_data[24:10],pair_data[9:8],pair_data[7:0],pair_data[26:25]);
			if(leading_edge_reference == pair_data[24:8])begin end
			else begin
				$fdisplay(testbench_inst.logfile,"%t Error: the leading_edge_reference 0x%h not equal to leading_edge 0x%h",$time,leading_edge_reference,pair_data[24:8]);
				$display("%t Error: the leading_edge_reference 0x%h not equal to leading_edge 0x%h",$time,leading_edge_reference,pair_data[24:8]);
				$stop;
			end
			if(width_reference[7:0] == pair_data[7:0] )begin end
			else	$fdisplay(testbench_inst.logfile,"%t Warning: the width_reference 0x%h not equal to width 0x%h",$time,width_reference,pair_data[7:0]);
		end
	end
endtask


task single_hit_edge_analysis();
	begin
		@(posedge testbench_inst.output_enable);
		if (enable_hit_edge_analysis) begin
			@(negedge clk320) edge_data <= testbench_inst.data_out[23:0];
			@(posedge clk320) 
			$fdisplay(testbench_inst.logfile,"%t Info: Channel %0d, edge 0x%h (coarse time 0x%h, fine time 0x%h), indicate b%b",$time,edge_data[23:19],edge_data[16:0],edge_data[16:2],edge_data[1:0],edge_data[18:17]);
			$display("%t Info: Channel %0d, edge 0x%h (coarse time 0x%h, fine time 0x%h), indicate b%b",$time,edge_data[23:19],edge_data[16:0],edge_data[16:2],edge_data[1:0],edge_data[18:17]);
			if(edge_data[17])begin
				if(leading_edge_reference ==edge_data[16:0])begin end
				else begin
					$fdisplay(testbench_inst.logfile,"%t Error: the leading_edge_reference 0x%h not equal to leading_edge 0x%h",$time,leading_edge_reference,edge_data[16:0]);
					$display("%t Error: the leading_edge_reference 0x%h not equal to leading_edge 0x%h",$time,leading_edge_reference,edge_data[16:0]);
					$stop;
				end
			end else begin
				if(trailing_edge_reference ==edge_data[16:0])begin end
				else begin
					$fdisplay(testbench_inst.logfile,"%t Error: the trailing_edge_reference 0x%h not equal to trailing_edge 0x%h",$time,trailing_edge_reference,edge_data[16:0]);
					$display("%t Error: the trailing_edge_reference 0x%h not equal to trailing_edge 0x%h",$time,trailing_edge_reference,edge_data[16:0]);
					$stop;
				end
			end
		end
	end
endtask

task single_hit_pair_test();
integer i,j;
begin
	$fdisplay(testbench_inst.logfile,"%t Single hit pair mode test start========================",$time);
	$display("%t Single hit pair mode test start========================",$time);
	testbench_inst.data_decoder_inst.length=3'd4;
	testbench_inst.data_decoder_inst.trigger=1'b0;
	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200
  	jtag_test_interface_inst.TRST_1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b0;
  	#500
	enable_hit_pair_analysis = 1'b1;
	for(i=0;i<24;i=i+1)begin
		for(j=0;j<8;j=j+1)begin
			triggless_mode_sim_inst.single_hit_gen(j,i,40+ {$random} % 100);
			#300;
		end
		#3000;
	end
	enable_hit_pair_analysis = 1'b0;
	$fdisplay(testbench_inst.logfile,"%t Single hit pair mode test finish========================",$time);
	$display("%t Single hit pair mode test finish========================",$time);
end
endtask

task multi_hit_pair_test();
integer i,j;
begin
	$fdisplay(testbench_inst.logfile,"%t Multi hit pair mode test start========================",$time);
	$display("%t Multi hit pair mode test start========================",$time);
	testbench_inst.data_decoder_inst.length=3'd4;
	testbench_inst.data_decoder_inst.trigger=1'b0;
	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200
  	jtag_test_interface_inst.TRST_1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b0;
  	#500
	enable_hit_pair_analysis = 1'b1;
	for(j=0;j<8;j=j+1)begin
		triggless_mode_sim_inst.multi_hit_gen(j,24'hffffff,40+ {$random} % 100);
		#3000;
	end
	enable_hit_pair_analysis = 1'b0;
	$fdisplay(testbench_inst.logfile,"%t Multi hit pair mode test finish========================",$time);
	$display("%t Multi hit pair mode test finish========================",$time);
end
endtask

task single_hit_leading_test();
integer i,j;
begin
	$fdisplay(testbench_inst.logfile,"%t Single hit leading mode test start========================",$time);
	$display("%t Single hit leading mode test start========================",$time);
	
	testbench_inst.data_decoder_inst.length=3'd3;
	testbench_inst.data_decoder_inst.trigger=1'b0;
	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200
  	jtag_test_interface_inst.TRST_1;
  	#200

	testbench_inst.jtag_test_interface_inst.JTAG_config(
		115'b0000010001001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
		9'd114,
		5'h12
		);
	@(negedge clk160) testbench_inst.reset = 1'b1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b0;
  	#500
	enable_hit_edge_analysis = 1'b1;
	for(i=0;i<24;i=i+1)begin
		for(j=0;j<8;j=j+1)begin
			triggless_mode_sim_inst.single_hit_gen(j,i,40+ {$random} % 100);
			#300;
		end
		#3000;
	end
	enable_hit_edge_analysis = 1'b0;
	$fdisplay(testbench_inst.logfile,"%t Single hit leading mode test finish========================",$time);
	$display("%t Single hit leading mode test finish========================",$time);
end
endtask

task multi_hit_leading_test();
integer i,j;
begin
	$fdisplay(testbench_inst.logfile,"%t Multi hit leading mode test start========================",$time);
	$display("%t Multi hit leading mode test start========================",$time);
	testbench_inst.data_decoder_inst.length=3'd3;
	testbench_inst.data_decoder_inst.trigger=1'b0;
	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200
  	jtag_test_interface_inst.TRST_1;
  	#200
	testbench_inst.jtag_test_interface_inst.JTAG_config(
		115'b0000010001001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
		9'd114,
		5'h12
		);
	@(negedge clk160) testbench_inst.reset = 1'b1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b0;
  	#500
	enable_hit_edge_analysis = 1'b1;
	for(j=0;j<8;j=j+1)begin
		triggless_mode_sim_inst.multi_hit_gen(j,24'hffffff,40+ {$random} % 100);
		#3000;
	end
	enable_hit_edge_analysis = 1'b0;
	$fdisplay(testbench_inst.logfile,"%t Multi hit leading mode test finish========================",$time);
	$display("%t Multi hit leading mode test finish========================",$time);
end
endtask


task fine_time_decode_test();
integer i,j;
begin	
	$fdisplay(testbench_inst.logfile,"%t fine time decode test start========================",$time);
	$display("%t fine time decode test start========================",$time);
	testbench_inst.data_decoder_inst.length=3'd3;
	testbench_inst.data_decoder_inst.trigger=1'b0;
	jtag_test_interface_inst.TRST_1;
  	jtag_test_interface_inst.TRST_0;
  	#200
  	jtag_test_interface_inst.TRST_1;
  	#200
	testbench_inst.jtag_test_interface_inst.JTAG_config(
		115'b0000010001001111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
		9'd114,
		5'h12
		);
	@(negedge clk160) testbench_inst.reset = 1'b1;
  	#200
  	@(negedge clk160) testbench_inst.reset = 1'b0;
  	#500
  	for(i=0;i<16;i=i+1)begin
  		#200;	
  		q_test = i;
  		force tdc_dut_inst.q_r[0] = q_test;
  		force tdc_dut_inst.cnt_r[0] = $random;
  		force tdc_dut_inst.cnt_inv_r[0] = $random;
  		triggless_mode_sim_inst.single_hit_gen(0,0,100);
  		fine_time_decode_check;
  		#200;
  	end
  	release tdc_dut_inst.q_r[0];
  	release tdc_dut_inst.cnt_r[0];
  	release tdc_dut_inst.cnt_inv_r[0];
  	$fdisplay(testbench_inst.logfile,"%t fine time decode test finish========================",$time);
	$display("%t fine time decode test finish========================",$time);
end
endtask






endmodule
