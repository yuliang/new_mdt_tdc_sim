/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_generator_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-23 09:42:07
//  Note       : 
//    
`timescale 1 ns / 1 fs 
module ttc_generator_sim(
	input clk320,
	input clk160,
	input clk40,
	output reg encoded_control,
	output reg bunch_reset_direct,
	input bcr
	);

reg [17:0] refrence_counter=18'b0;
always @(posedge clk320) begin
	refrence_counter <= refrence_counter + 18'b1;
end 



reg [11:0] roll_over=12'hfff;
reg [11:0] counter_bcr=12'b0;
reg enable_bcr=1'b0;
reg reset_bcr=1'b0;
always @(posedge clk40 ) begin
	counter_bcr <= (counter_bcr== roll_over)|reset_bcr ? 12'b0 : (counter_bcr +12'b1);
end
always @(negedge clk160) begin
	 bunch_reset_direct <= enable_bcr&(counter_bcr == roll_over);
end

//assign bunch_reset_direct= enable_bcr&(counter_bcr == roll_over);

reg [3:0] ttc_r = 4'b0000;
reg [3:0] new_ttc_code = 4'b0000;
wire encode_ttc_new;
reg new_ttc_start = 1'b0;
always @(posedge clk160 ) begin
	if(new_ttc_start) begin
		ttc_r <= new_ttc_code;		
	end else begin
		ttc_r <= {ttc_r[2:0],1'b0};
	end
end
assign encode_ttc_new = ttc_r[3];


reg [2:0] legacy_ttc = 3'b000;
reg [2:0] legacy_ttc_code = 3'b000;
wire encode_ttc_legacy;
reg legacy_ttc_start = 1'b0;
always @(posedge clk40) begin
		if(legacy_ttc_start) begin
			legacy_ttc <= legacy_ttc_code; 		
		end else begin
			legacy_ttc <= {legacy_ttc[1:0],1'b0};
		end
end
assign encode_ttc_legacy = legacy_ttc[2];

reg new_ttc_mode = 1'b0;
always @(negedge clk160) begin
	encoded_control <= new_ttc_mode ? encode_ttc_new : encode_ttc_legacy;
end
reg [17:0] refrence_counter_0;
reg [17:0] refrence_counter_1;
reg [17:0] bcr_interval[5:0];
task direct_bcr_test();
	begin
	  	jtag_test_interface_inst.TRST_1;
  		jtag_test_interface_inst.TRST_0;
  		#200
  		jtag_test_interface_inst.TRST_1;
  		#200
		$fdisplay(testbench_inst.logfile,"%t Direct bcr test start",$time);
		$display("%t Direct bcr test start",$time);
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			115'b0010000000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
			9'd114,
			5'h12
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200  		
  		enable_bcr=12'b0;
		roll_over = 12'h005;
		reset_bcr = 1'b1;
		enable_bcr = 1'b1;
		@(posedge clk40 )
		@(posedge clk40 )
		reset_bcr = 1'b0;
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[0] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		$display("%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[1] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		$display("%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[2] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Third bcr delay for zero is %d",$time,bcr_interval[2]);
		$display("%t Info: Third bcr delay for zero is %d",$time,bcr_interval[2]);
		if ((bcr_interval[2]==bcr_interval[1])&(bcr_interval[2]==bcr_interval[0])) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end
		enable_bcr = 1'b0;
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			94'b0000101000000100000000111111111111111111111111000000000001111110011100000000000000000000011111,
			9'd93,
			5'h03
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200
  		$fdisplay(testbench_inst.logfile,"%t change bcr delay to 1",$time);
  		$display("%t change bcr delay to 1",$time);
  		enable_bcr=12'b0;
  		roll_over = 12'h005;
  		reset_bcr = 1'b1;
		enable_bcr = 1'b1;
		@(posedge clk40 )
		@(posedge clk40 )
		reset_bcr = 1'b0;
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[3]= refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		$display("%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[4]  = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		$display("%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		@(posedge bunch_reset_direct) refrence_counter_0 =refrence_counter ;
		@(posedge bcr) bcr_interval[5]  = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Third bcr delay for one is %d",$time,bcr_interval[5]);
		$display("%t Info: Third bcr delay for one is %d",$time,bcr_interval[5]);
		if ((bcr_interval[5]==bcr_interval[4])&(bcr_interval[5]==bcr_interval[3])) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end
		if((bcr_interval[5]-bcr_interval[0])=='b1000)begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr offset fail",$time);
			$display("%t Error: the bcr offset fail",$time);
		end
		enable_bcr = 1'b0;
		$fdisplay(testbench_inst.logfile,"%t Direct bcr test finish",$time);
		$display("%t Direct bcr test finish",$time);
	end
endtask

task legacy_encode_bcr_test();
	begin
		jtag_test_interface_inst.TRST_1;
  		jtag_test_interface_inst.TRST_0;
  		#200
  		jtag_test_interface_inst.TRST_1;
  		#200
		$fdisplay(testbench_inst.logfile,"%t Legacy Encode bcr test start",$time);
		$display("%t Legacy Encode bcr test start",$time);
		new_ttc_mode = 1'b0;
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			115'b0000000000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
			9'd114,
			5'h12
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200  
		legacy_ttc_code = 3'b110;
		@(negedge clk40) legacy_ttc_start = 1'b1;
		@(posedge clk40) refrence_counter_0 =refrence_counter;
		@(negedge clk40) legacy_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[0] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		$display("%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		#200
		@(negedge clk40) legacy_ttc_start = 1'b1;
		@(posedge clk40) refrence_counter_0 =refrence_counter;
		@(negedge clk40) legacy_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[1] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		$display("%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		if (bcr_interval[1]==bcr_interval[0]) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end
		#200
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			94'b0000101000000100000000111111111111111111111111000000000001111110011100000000000000000000011111,
			9'd93,
			5'h03
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200 
  		legacy_ttc_code = 3'b110;
		@(negedge clk40) legacy_ttc_start = 1'b1;
		@(posedge clk40) refrence_counter_0 =refrence_counter;
		@(negedge clk40) legacy_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[3] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		$display("%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		#200
		@(negedge clk40) legacy_ttc_start = 1'b1;
		@(posedge clk40) refrence_counter_0 =refrence_counter;
		@(negedge clk40) legacy_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[4] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		$display("%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		if (bcr_interval[4]==bcr_interval[3]) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end 
		if((bcr_interval[4]-bcr_interval[0])=='b1000)begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr offset fail",$time);
			$display("%t Error: the bcr offset fail",$time);
		end
		$fdisplay(testbench_inst.logfile,"%t Legacy Encode bcr test finish",$time);
		$display("%t Legacy Encode bcr test finish",$time);
	end
endtask

task new_encode_bcr_test();
	begin
		jtag_test_interface_inst.TRST_1;
  		jtag_test_interface_inst.TRST_0;
  		#200
  		jtag_test_interface_inst.TRST_1;
  		#200
		$fdisplay(testbench_inst.logfile,"%t new Encode bcr test start",$time);
		$display("%t new Encode bcr test start",$time);
		new_ttc_mode = 1'b1;
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			115'b1000000000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
			9'd114,
			5'h12
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200  
		new_ttc_code = 4'b1110;
		@(negedge clk160) new_ttc_start = 1'b1;
		@(posedge clk160) refrence_counter_0 =refrence_counter;
		@(negedge clk160) new_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[0] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		$display("%t Info: First bcr delay for zero is %d",$time,bcr_interval[0]);
		#200
		@(negedge clk160) new_ttc_start = 1'b1;
		@(posedge clk160) refrence_counter_0 =refrence_counter;
		@(negedge clk160) new_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[1] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		$display("%t Info: Second bcr delay for zero is %d",$time,bcr_interval[1]);
		if (bcr_interval[1]==bcr_interval[0]) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end
		#200
		testbench_inst.jtag_test_interface_inst.JTAG_config(
			94'b0000101000000100000000111111111111111111111111000000000001111110011100000000000000000000011111,
			9'd93,
			5'h03
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#200 
  		new_ttc_code = 4'b1110;
		@(negedge clk160) new_ttc_start = 1'b1;
		@(posedge clk160) refrence_counter_0 =refrence_counter;
		@(negedge clk160) new_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[3] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		$display("%t Info: First bcr delay for one is %d",$time,bcr_interval[3]);
		#200
		@(negedge clk160) new_ttc_start = 1'b1;
		@(posedge clk160) refrence_counter_0 =refrence_counter;
		@(negedge clk160) new_ttc_start = 1'b0;
		@(posedge bcr) bcr_interval[4] = refrence_counter - refrence_counter_0;
		$fdisplay(testbench_inst.logfile,"%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		$display("%t Info: Second bcr delay for one is %d",$time,bcr_interval[4]);
		if (bcr_interval[4]==bcr_interval[3]) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr generate fail",$time);
			$display("%t Error: the bcr generate fail",$time);
		end 
		if((bcr_interval[4]-bcr_interval[0])=='b1000)begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error: the bcr offset fail",$time);
			$display("%t Error: the bcr offset fail",$time);
		end
		$fdisplay(testbench_inst.logfile,"%t New Encode bcr test finish",$time);
		$display("%t New Encode bcr test finish",$time);
	end
endtask

task encode_bcr_test();
	begin
		legacy_encode_bcr_test;
		new_encode_bcr_test;
	end
endtask

endmodule